
Overview
========

All communication between the DC library and the app is realised by exchanging
packets of Unicode strings between the two. This way, the app can be implemented in any language
and execute in a container such as a web component. 

A stream of string "packets" shall be established in both directions between the app and DC library as defined
in this document.


|   |
| - |
| *In the API lists below, **bold** items marks handles which are currently relevant* |


Communication Concepts
======================

Two overall message types can be used: _events_ and _functions_. An event is a single message sent without any
kind of confirmation. A function comprises a pair of messages going in opposite directions: a _request_ and a
_response_ message. A function request always result in a function response, which may contain either a return
message or an error message.

Functions and events can be used in both directions - app to DC library, or DC library to app.

Envelope Format
---------------

A single packet is a Unicode string containing exactly one JSON object having the format outlined below. This
is called the "DC Message envelope".

```json
{
  "messageType": ( "REQUEST" | "RESPONSE" | "EVENT" ),
  "name": "...",        // The name of the thing. Mandatory. The name of a REQUEST and matching (by requestId) RESPONSE must be identical.
  "args": {},           // A JSON Object containing the arguments - may be empty/void/absent (must be for error responses).
  "requestId": "1",     // Only for requests and responses. Mandatory. A unique ID linking the two together
  "errorCode": 0,       // Only for error responses - an optional non-zero error code, or 0 if none. Mandatory.
  "errorMessage": "..." // Only for error responses - the error message. Mandatory.
}
```

For functions returning only a single unnamed value, the `args` object will comprise a single member called
`"value"`. Functions returning more than one values must explicitly name them.

DC-2-Host and Host-2-DC
-----------------------

The functions and events sent from the app to the DC library are defined by the Host-2-DC API. In all of these
cases, the request and event messages are sent from the app to the library while responses go from the library
to the app. Conversely, the DC-2-Host API comprises all messages flowing in the opposite direction.


Error Codes
===========

The following error codes are defined for function error responses


| `errorCode` | Name                               | Description                                             |
|:-----------:|:---------------------------------- |:------------------------------------------------------- |
|           0 | `SUCCESS`                          | Not used - reserved placeholder for "no error"          |
|           1 | `UNKNOWN`                          | Unknown / unspecified error. Check logs for any details |
|           2 | `INVALID_ARGUMENT`                 | An invalid argument was supplied to the function        |
|           3 | `UNSUPPORTED_OPERATION`            | The requested operation is (currently) unsupported      |
|           4 | `BLUETOOTH_PERMISSION_NOT_GRANTED` | Permissions to access Bluetooth devices has not been granted to his app |
|           5 | `BLUETOOTH_TURNED_OFF`             | The Bluetooth transceiver is off                        |


Datatypes
=========

Two common datatypes are defined: `Device` and `Metric`.


Device
------

A `Device` object represents one particular personal health device identified by the unique identifier
`persistentDeviceid`, which may be used by the application to store information about the device persistently.

Notice that during device discovery, not all properties will be available.

- **persistentDeviceId (string)**
- **(friendly)name (string)**
- connected (bool)
- linked (bool)
- connectable (enum: NEVER, WHEN_LINKED, ALWAYS)
- disconnectable (bool)
- **specializations (array<uint32\>)**
- transport (object)
    - link_capable (bool)
    - unlink_capable (bool)
    - subclass (fixed string: 'Bluetooth') - the following:
        - bd_addr (uint64/string)
        - name (string)
        - classic (bool)
        - le (bool)
        - bonded (bool)
        - profile[]:
            - type (enum: HDP, GATT; SPP)
            - service_name
            - service_description
            - provider_name
- **serialNumber (string)**
- hwRevision (string)
- swRevision (string)
- fwRevision (string)
- **manufacturer (string)**
- **modelNumber (string)**
- systemId (string)


Metric
------

A `Metric` object captures a single aspect of a measurement, such as a value, a quality attribute, state of
the device or the user, or some other observational meta information (time, battery level or such). A single
"measurement" typically comprises a set of `Metrics`.

The concept of metrics are typically mapped to `Observation` resources in FHIR, but this is not always the
case, and since the mapping is not always 1:1, we shall avoid the confusion and refrain from using the term
"observation".

- **metricId**
- personId (int)
- **measurementTypeCode (uint32)**
- **supplementalTypeCodes (array<uint32\>)**
- measurementStatus (uint16)  // <-- null/empty when unsupported by the metric.
- sourceHandleRefs (array<metricId\>)
- hasTimeFault (bool)
- usesAbsoluteTime (bool)
- usesBaseOffsetTime (bool)
- usesRelativeTime (bool)
- usesHiResRelativeTime (bool)
- hasDateTimeAdjustment (bool)
- phgSetTime (bool)
- isAMdsMeasurement (bool)
- **agentTimeStamp (datetime)     // <-- null/empty if the metric does not report BO/absolute time**
- agentRelativeTimeStamp (uint64) // <-- null/empty if the metric does not report (high res) relative time
- **timeOfMeasurementReception (datetime)**
- **measurementStatus (uint16)    // <-- null/empty if not supported by this metric**
- **subclass (fixed string: 'Numeric')** - the following:
    - **value (string)    // <-- Note: String type to preserve precision - also remember to parse special values!**
    - **unitCode (uint32)**
- **subclass (fixed string: 'CompoundNumeric')** - the following:
    - **array<object\> components:**
        - **measurementTypeCode (uint32)**
        - **value (string)    // <-- Note: String type to preserve precision - also remember to parse special values!**
        - **unitCode (uint32)**
- subclass (fixed string: 'EnumCode') - the following:
    - value (uint32)
- **subclass (fixed string: 'EnumBits') - the following:**
    - **value (uint32)**
    - **is32Bits (bool)  // <-- false: 16 bit flags, true: 32 bit flags**
- subclass (fixed string: 'EnumString') - the following:
    - value (string)
- subclass (fixed string: 'RTSA') - the following:
    - scaleFactor (float)
    - offset (float)
    - samplePeriod (uint32) // <-- sample period in units of 1/8000 second
    - value (array<int\>)
    - unitCode (uint32)



Initialization API
==================

The `initialize()` function must be called before any other Host-2-DC event or function. Functions will throw
an exception and events will be ignored if this is not the case.

**FIXME:** PHGFields importeres pt fra CODE, hvilket skal oversættes til JSON.

Host-2-DC
---------

### Functions ###

initialize(PHGFields)



Device Management API
=====================

**FIXME:** Mangler test for BT support, rettigheder, om BT er tændt mv. (se Enlito)
Evt. signalér til app om Bluetooth tændt/slukket


Host-2-DC
---------

### Functions ###

- searchSupportingDevices(prototypeDevice) -\> searchId
- stopSearch(searchId)
- **getAvailableDevices(prototypeDevice : Device, linked : bool, connected : bool, connectable : bool) -\> array<Device\>**
- link(persistentDeviceId)
- unlink(persistentDeviceId, unlink_transport:bool)
- setName(persistentDeviceId, name:string)
- connect(persistentDeviceId)
- disconnect(persistentDeviceId)
- bond(persistentDeviceId)
- unbond(persistentDeviceId)
- setStandardPasskey(persistentDeviceId, passkey:string)

**FIXME:** Der mangler noget, der kan håndtere whitelisting og slå det til/fra.


DC-2-Host
---------

### Functions ###

- bluetoothPairingLegacy(Device) -\> string
- bluetoothPairingJustWorks(Device) -\> bool
- bluetoothPairingNumericComparison(Device,comparisonValue:uint64) -\> bool
- bluetoothPairingPasskey(Device) -\> uint64

### Events ###

- searchResult(searchId, Device)
- connected(Device)                       // <-- Device object may still be missing properties
- mdsReceived(Device)                     // <-- Device object is now fully populated
- disconnected(persistentDeviceId)
- error...

#### Event flow regex: ####

"connected" ("mdsReceived" ("metricReceived" | "mdsReceived")* )? "disconnected"



Measurement API
===============

Host-2-DC
---------

### Functions ###

- **subscribeDeviceType(specialization : uint32) -\> void**
- **unsubscribeDeviceType(specialization : uint32) -\> void**
- **subscribeDevice(persistentDeviceId : string) -\> void**
- **unsubscribeDevice(persistentDeviceId : string) -\> void**
- deleteMetric(metricId) -\> void
- **enablePulseOxModalities(normal : bool, fast : bool, slow : bool) -\> void**


DC-2-Host
---------

### Events ###

- **metricsReceived(persistentDeviceId : string, metrics : array<Metric\>)**


Generate Bundle
===============

**FIXME:** Mangler at definere datatype for Patient


Host-2-DC
---------

### Functions ###

- generateFhirBundle(persistentDeviceId, patient:Patient) -\> string
- generateFhirBundle(persistentDeviceId) -\> array<string\>    // <-- One bundle returned per patient.


DC-2-Host
---------

### Functions ###

- getPatientFor(Device, personNumber:int) -\> Patient



Auditlog API
============

DC-2-Host
---------

### Events ###

- auditEvent(type:string, measurementIDs:set<string\>, attributes:map<string,string\>)
