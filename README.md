# PHG Device Communication #

Android and iOS Device Communication (DC) library for communicating with _personal health devices_
(PHDs) and _health & fitness servers_ (HFSs). Wrapping the [CODE
PHG](https://lnijira.atlassian.net/wiki/spaces/CP/overview) implementation.

## Getting Started ##

### As a Consumer (using this service) on Android ###

To use the DC library from an Android app, add the following dependencies to your app's module-level
`build.gradle` file in the `dependencies` section:

```groovy
implementation 'dk.alexandra.phg:device_communication:vX.Y.Z'
```

where `X.Y.Z` shall be substituted by the concrete revision of the library.

The library is currently available from the Trifork Nexus Repository at 
https://nexus3.trifork.com/repository/fut-kol-maven-group/

All communication between the DC library and the app is realised by exchanging
packets of Unicode strings between the two. This way, the app can be implemented in any language
and execute in a container such as a web component. The API for the packet exchange interface is
defined in [api.md](api.md).

With the dependency in place, the next step is to import
`dk.alexandra.phg.device_communication.DeviceCommunicationController` and hook up communication
endpoints. This is accomplished using the `initialize()` method of the singleton instance of the
class, which is accessed using the `shared` field:

```java
public static final DeviceCommunicationController shared;

public Consumer<String> initialize(@NonNull Context context,
                                   @NonNull Consumer<String> dc2hostHandler)
```

The first `android.content.Context`-type argument shall be the Android Application Context (it can
be any `android.content.Context`-type object as the application context will be embedded in all of
them).

The second `java.util.function.Consumer<String>`-type `dc2hostHandler` and the returned
`host2dcHandler` connects the API for packet exchange.

The DC library requires the `BLUETOOTH_CONNECT` permission (declared in its own manifest), and is
capable of automatically asking the user for permissions to use Bluetooth or permission to turn the
Bluetooth transceiver on. To do this, the library needs to keep track of the current `Activity`,
which can be done by hooking up the `android.app.Application.ActivityLifecycleCallbacks` available
from `DeviceCommunicationController.shared.getActivityLifecycleCallbacks()` to the app's
`android.app.application.registerActivityLifecycleCallbacks()`. This is an optional step, and if
it is not done, the DC library cannot do anything about missing Bluetooth access.

The DC library depends on a configured whitelist of personal health devices. The functionality to
create and edit this whitelist resides in the `manager.DeviceManager` class which represents them
'model' role of a model-view-controller pattern for a component managing this whitelist. See the
javadoc of the `manager.DeviceManager` class for further instructions.


### As a Consumer (using this service) on iOS ###

**TBD**

### As a Developer on Android ###

The Device Communication library for Android was developed using the Android SDK Build-Tools 32.0.0. Android
Studio is recommended but not required to work with this project.

From the commandline, the library can be built and tested by running:

``` shell
cd android/
./gradlew build
```

### Building Locally with an App ###

When developing the library, it is necessary to be able to test it locally in use by an app. In
order to do this, the following steps can be used, leveraging the local Maven cache - a.k.a.
`mavenLocal`.

The following changes must be made **in the app** Gradle scripts:

 - In dependency resolution `repositories` (typically found in `settings.gradle`) add `mavenLocal()`
 - In the module `build.gradle` file's `dependencies` section, replace
   `implementation 'dk.alexandra.phg:device_communication:vX.Y.Z'` by
   `implementation 'dk.alexandra.phg:device_communication:vX.Y.Z-C-gHHHHHHH-dirty'`

The `-dirty` part is crucial here. The full name of the version can be found by executing:

```shell
git describe --tags --dirty
```

in the PHG Device Communication folder.

When this is in place, executing `gradle publishToMavenLocal` in the `android/` folder followed by
rebuilding and running the app will be working.


### As a Developer on iOS ###

**TBD** Keywords to cover in this section:

* Required tools (and versions)
* How to open and build the XCode project
* How to build the iOS Framework

## CI Pipeline and Generated Artifacts ##

The DC library is published for Android as an AAR file, which is more or less the same thing
as a JAR file, except it contains extra resources relevant to Android along with an Android
Manifest file. This format allows the library to be imported into other Android projects the
same way JAR libraries are imported into Java projects.

The library will automatically be built by Bitbucket Pipelines on every commit. Publishing the
built artifact happens manually by pushing the "Run" button next to "Publish to Nexus". The
artifact will then be made available from the Trifork Nexus Repository as explained earlier.

**TBD** Describe the iOS artifact and pipeline

### Making a Release ###

**TBD** At the moment this section only covers instructions for Android

Releases (having proper version numbers) *should* be made on the `master` branch. Furthermore,
*most* commits on the `master` branch *should* probably be released.

In order to make a proper versioned release, define the next version number according to the
[Semantic Versioning](https://semver.org/) rules and add a tag on the form `vX.Y.Z` to the commit
(on the master branch after merging a pull request). Here, X, Y, and Z are replaced by the version
number components. When the tag has been created (either on Bitbucket or locally and then pushed
with the `--tags` flag), push the Bitbucket Pipelines "Run"-button next to "Publish to Nexus".

### Making an Experimental Release ###

Experimental releases through the Trifork Nexus Repository can be created at any time from commits
on any branch. Proceed as outlined in the previous paragraph, but rather than adding a tag on the
`vX.Y.Z` form, *either* add a tag starting with any other symbol than `v`, *or* don't add a tag at
all. If you do not add a tag, the version number will be auto-generated by `git` on the form
`vX.Y.Z-C-gHHHHHHH` where `vX.Y.Z` is the most recent previous tag, `C` is the number of commits
since this tag, and `HHHHHHH` is the commit hash. See the documentation for `git describe --tags`
for further details.

## Architecture ##

**TBD** Keywords to cover in this section:

* Overall architecture
* Main CODE components and reference to LNI Confluence site for more info
* The Android Service setup
* Background execution

## Contact ##

Jacob Andersen, Alexandra Instituttet. jacob.andersen@alexandra.dk
