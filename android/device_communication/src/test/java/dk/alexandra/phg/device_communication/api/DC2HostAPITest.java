package dk.alexandra.phg.device_communication.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashSet;

@RunWith(MockitoJUnitRunner.class)
public class DC2HostAPITest {

    @Mock
    private DCMessageDispatcher dispatcher;

    private DC2HostAPI dc2HostAPI;

    @Before
    public void setUp() {
        dc2HostAPI = new DC2HostAPI(dispatcher);
        reset(dispatcher);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dispatcher);
    }


    /* metricsReceived() */

    @Test
    public void metricsReceivedEmptySet() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        dc2HostAPI.metricsReceived("qwerty", new HashSet<>());
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(0, args.getValue().getJSONArray("metrics").length());
    }

    @Test
    public void metricsReceivedNumeric() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        HashSet<Metric> metrics = new HashSet<>();
        metrics.add(new Metric.Numeric("abc", 42, null, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", -1, "123.4", 1000));
        dc2HostAPI.metricsReceived("qwerty", metrics);
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(1, args.getValue().getJSONArray("metrics").length());
        JSONObject result = args.getValue().getJSONArray("metrics").getJSONObject(0);
        assertEquals("abc", result.getString("metricId"));
        assertEquals(42, result.getInt("measurementTypeCode"));
        assertFalse(result.has("supplementalTypeCodes"));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("Numeric", result.getString("subclass"));
        assertEquals("123.4", result.getString("value"));
        assertEquals(1000, result.getInt("unitCode"));
        assertFalse(result.has("measurementStatus"));
    }

    @Test
    public void metricsReceivedNumericWithStatus() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        HashSet<Metric> metrics = new HashSet<>();
        metrics.add(new Metric.Numeric("abc", 42, null, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", 0x12fe, "123.4", 1000));
        dc2HostAPI.metricsReceived("qwerty", metrics);
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(1, args.getValue().getJSONArray("metrics").length());
        JSONObject result = args.getValue().getJSONArray("metrics").getJSONObject(0);
        assertEquals("abc", result.getString("metricId"));
        assertEquals(42, result.getInt("measurementTypeCode"));
        assertFalse(result.has("supplementalTypeCodes"));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("Numeric", result.getString("subclass"));
        assertEquals("123.4", result.getString("value"));
        assertEquals(1000, result.getInt("unitCode"));
        assertEquals(0x12fe, result.getInt("measurementStatus"));
    }

    @Test
    public void metricsReceivedCompound() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        HashSet<Metric> metrics = new HashSet<>();
        HashSet<Metric.Component> components = new HashSet<>();
        components.add(new Metric.Component(9000, "Foo", 888));
        components.add(new Metric.Component(9001, "Bar", 999));
        metrics.add(new Metric.CompoundNumeric("xyz", 87, new int[] {1,2}, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", -1, components));
        dc2HostAPI.metricsReceived("qwerty", metrics);
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(1, args.getValue().getJSONArray("metrics").length());
        JSONObject result = args.getValue().getJSONArray("metrics").getJSONObject(0);
        assertEquals("xyz", result.getString("metricId"));
        assertEquals(87, result.getInt("measurementTypeCode"));
        assertEquals(2, result.getJSONArray("supplementalTypeCodes").length());
        assertEquals(1, result.getJSONArray("supplementalTypeCodes").getInt(0));
        assertEquals(2, result.getJSONArray("supplementalTypeCodes").getInt(1));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("CompoundNumeric", result.getString("subclass"));
        assertFalse(result.has("measurementStatus"));
        assertEquals(2, result.getJSONArray("components").length());
        int foo, bar;
        if (result.getJSONArray("components").getJSONObject(0).getInt("measurementTypeCode") == 9000) {
            foo = 0; bar = 1;
        } else {
            foo = 1; bar = 0;
        }
        JSONObject component = result.getJSONArray("components").getJSONObject(foo);
        assertEquals(9000, component.getInt("measurementTypeCode"));
        assertEquals("Foo", component.getString("value"));
        assertEquals(888, component.getInt("unitCode"));
        component = result.getJSONArray("components").getJSONObject(bar);
        assertEquals(9001, component.getInt("measurementTypeCode"));
        assertEquals("Bar", component.getString("value"));
        assertEquals(999, component.getInt("unitCode"));
    }

    @Test
    public void metricsReceivedEmptyCompoundAndNumeric() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        HashSet<Metric> metrics = new HashSet<>();
        HashSet<Metric.Component> components = new HashSet<>();
        metrics.add(new Metric.CompoundNumeric("xyz", 87, new int[] {1,2}, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", 1, components));
        metrics.add(new Metric.Numeric("abc", 42, null, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", 0x10000, "123.4", 1000));
        dc2HostAPI.metricsReceived("qwerty", metrics);
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(2, args.getValue().getJSONArray("metrics").length());
        int xyz, abc;
        if ("xyz".equals(args.getValue().getJSONArray("metrics").getJSONObject(0).getString("metricId"))) {
            xyz = 0; abc = 1;
        } else {
            xyz = 1; abc = 0;
        }
        JSONObject result = args.getValue().getJSONArray("metrics").getJSONObject(xyz);
        assertEquals("xyz", result.getString("metricId"));
        assertEquals(87, result.getInt("measurementTypeCode"));
        assertEquals(2, result.getJSONArray("supplementalTypeCodes").length());
        assertEquals(1, result.getJSONArray("supplementalTypeCodes").getInt(0));
        assertEquals(2, result.getJSONArray("supplementalTypeCodes").getInt(1));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("CompoundNumeric", result.getString("subclass"));
        assertEquals(1, result.getInt("measurementStatus"));
        assertEquals(0, result.getJSONArray("components").length());
        result = args.getValue().getJSONArray("metrics").getJSONObject(abc);
        assertEquals("abc", result.getString("metricId"));
        assertEquals(42, result.getInt("measurementTypeCode"));
        assertFalse(result.has("supplementalTypeCodes"));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("Numeric", result.getString("subclass"));
        assertEquals("123.4", result.getString("value"));
        assertEquals(1000, result.getInt("unitCode"));
        assertFalse(result.has("measurementStatus"));
    }

    @Test
    public void metricsReceivedEnumBits32() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        HashSet<Metric> metrics = new HashSet<>();
        metrics.add(new Metric.EnumBits("abc", 42, null, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", -1, 0xdeadbeef, true));
        dc2HostAPI.metricsReceived("qwerty", metrics);
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(1, args.getValue().getJSONArray("metrics").length());
        JSONObject result = args.getValue().getJSONArray("metrics").getJSONObject(0);
        assertEquals("abc", result.getString("metricId"));
        assertEquals(42, result.getInt("measurementTypeCode"));
        assertFalse(result.has("supplementalTypeCodes"));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("EnumBits", result.getString("subclass"));
        assertEquals(0xdeadbeef, result.getInt("value"));
        assertTrue(result.getBoolean("is32Bits"));
        assertFalse(result.has("measurementStatus"));
    }

    @Test
    public void metricsReceivedEnumBits16() throws JSONException {
        ArgumentCaptor<JSONObject> args = ArgumentCaptor.forClass(JSONObject.class);
        HashSet<Metric> metrics = new HashSet<>();
        metrics.add(new Metric.EnumBits("abc", 42, null, "2020-01-01T10:10:10Z", "2010-10-10T01:01:01Z", -1, 0xdeadbeef, false));
        dc2HostAPI.metricsReceived("qwerty", metrics);
        verify(dispatcher).sendEventToHost(eq("metricsReceived"), args.capture());
        assertEquals("qwerty", args.getValue().getString("persistentDeviceId"));
        assertEquals(1, args.getValue().getJSONArray("metrics").length());
        JSONObject result = args.getValue().getJSONArray("metrics").getJSONObject(0);
        assertEquals("abc", result.getString("metricId"));
        assertEquals(42, result.getInt("measurementTypeCode"));
        assertFalse(result.has("supplementalTypeCodes"));
        assertEquals("2020-01-01T10:10:10Z", result.getString("agentTimeStamp"));
        assertEquals("2010-10-10T01:01:01Z", result.getString("timeOfMeasurementReception"));
        assertEquals("EnumBits", result.getString("subclass"));
        assertEquals(0x0000beef, result.getInt("value"));
        assertFalse(result.getBoolean("is32Bits"));
        assertFalse(result.has("measurementStatus"));
    }
}