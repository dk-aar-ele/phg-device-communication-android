package dk.alexandra.phg.device_communication.api;

import static org.junit.Assert.*;

import static dk.alexandra.phg.device_communication.api.RequestIdCounter.requestIdCounter;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Test;

public class DCMessageTest {

    @Test
    public void serializeRequestWithNullArgumentAndAutoIncrementRequestId() throws JSONException {
        DCMessage.Request request = new DCMessage.Request("11",null);
        assertEquals("Failed to serialize a request with null arguments",
                "{\"messageType\":\"REQUEST\",\"requestId\":\"" + ++requestIdCounter + "\",\"name\":\"11\"}",
                request.serialize());
        assertEquals("RequestId mismatch", String.valueOf(requestIdCounter), request.getRequestId());
        assertEquals("Name mismatch", "11", request.getName());
        assertNull("Args mismatch", request.getArgs());
    }

    @Test
    public void serializeRequestWithNullArgumentAndFixedRequestId() throws JSONException {
        DCMessage.Request request = new DCMessage.Request("222", "12345", null);
        assertEquals("Failed to serialize a request with preset requestId",
                "{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"222\"}",
                request.serialize());
        assertEquals("RequestId mismatch", "12345", request.getRequestId());
        assertEquals("Name mismatch", "222", request.getName());
    }

    @Test
    public void serializeRequestWithEmptyArguments() throws JSONException {
        DCMessage.Request request = new DCMessage.Request("3333",new JSONObject());
        assertEquals("Failed to serialize a request with empty-object {} arguments",
                "{\"args\":{},\"messageType\":\"REQUEST\",\"requestId\":\"" + ++requestIdCounter + "\",\"name\":\"3333\"}",
                request.serialize());
        assertEquals("RequestId mismatch", String.valueOf(requestIdCounter), request.getRequestId());
        assertEquals("Name mismatch", "3333", request.getName());
        assertNotNull("Args mismatch", request.getArgs());
        assertEquals("Args mismatch", "{}", request.getArgs().toString());
    }

    @Test
    public void serializeResponse() throws JSONException {
        DCMessage.Response response = new DCMessage.Response("FooBar", "22", new JSONObject().put("testInteger",42).put("testString","foo"));
        assertEquals("Failed to serialize a response with two arguments",
                "{\"args\":{\"testInteger\":42,\"testString\":\"foo\"},\"messageType\":\"RESPONSE\",\"requestId\":\"22\",\"name\":\"FooBar\"}",
                response.serialize());
        assertEquals("RequestId mismatch", "22", response.getRequestId());
    }

    @Test
    public void serializeErrorResponse() throws JSONException {
        DCMessage.ErrorResponse errorResponse = new DCMessage.ErrorResponse("Foo", "1", -56, "SOO WRONG!");
        assertEquals("Failed to serialize an ErrorResponse",
                "{\"messageType\":\"RESPONSE\",\"requestId\":\"1\",\"name\":\"Foo\",\"errorMessage\":\"SOO WRONG!\",\"errorCode\":-56}",
                errorResponse.serialize());
        assertEquals("RequestId mismatch", "1", errorResponse.getRequestId());
        assertEquals("errorCode mismatch", -56, errorResponse.getErrorCode());
        assertEquals("errorMessage mismatch", "SOO WRONG!", errorResponse.getErrorMessage());
    }

    @Test
    public void serializeEvent() throws JSONException {
        DCMessage.Event event = new DCMessage.Event("44444",new JSONObject());
        assertEquals("Failed to serialize an event with empty-object {} arguments",
                "{\"args\":{},\"messageType\":\"EVENT\",\"name\":\"44444\"}",
                event.serialize());
        assertEquals("Name mismatch", "44444", event.getName());
    }

    @Test
    public void parseRequest() throws JSONException {
        DCMessage message = DCMessage.parse("{\"messageType\":\"REQUEST\",\"requestId\":\"123\",\"name\":\"55x\"}");
        assertTrue(message instanceof DCMessage.Request);
        assertEquals("Name mismatch", "55x", message.getName());
        assertEquals("RequestId mismatch", "123",((DCMessage.Request) message).getRequestId());
        assertNull("Args mismatch", message.getArgs());
    }

    @Test
    public void parseResponse() throws JSONException {
        DCMessage message = DCMessage.parse("{\"args\":{\"testInteger\":42,\"testString\":\"bar\"},\"messageType\":\"RESPONSE\",\"name\":\"Foo\",\"requestId\":\"25148\"}");
        assertTrue(message instanceof DCMessage.Response);
        assertEquals("Name mismatch", "Foo", message.getName());
        assertEquals("RequestId mismatch", "25148",((DCMessage.Response) message).getRequestId());
        assertNotNull("Args mismatch", message.getArgs());
        assertEquals("Args mismatch", "{\"testInteger\":42,\"testString\":\"bar\"}", message.getArgs().toString());
    }

    @Test
    public void parseErrorResponse() throws JSONException {
        DCMessage message = DCMessage.parse("{\"messageType\":\"RESPONSE\",\"name\":\"Foo\",\"requestId\":\"10000\",\"errorMessage\":\"Ups\",\"errorCode\":856}");
        assertTrue(message instanceof DCMessage.ErrorResponse);
        assertEquals("Name mismatch", "Foo", message.getName());
        assertEquals("RequestId mismatch", "10000",((DCMessage.Response) message).getRequestId());
        assertEquals("ErrorCode mismatch", 856,((DCMessage.ErrorResponse) message).getErrorCode());
        assertEquals("ErrorMessage mismatch", "Ups",((DCMessage.ErrorResponse) message).getErrorMessage());
        assertNull("Args mismatch", message.getArgs());
    }

    @Test
    public void parseEvents() throws JSONException {
        DCMessage message = DCMessage.parse("{\"args\":{},\"messageType\":\"EVENT\",\"name\":\"NAME2\"}");
        assertTrue(message instanceof DCMessage.Event);
        assertEquals("Name mismatch", "NAME2", message.getName());
        assertNotNull("Args mismatch", message.getArgs());
        assertEquals("Args mismatch", "{}", message.getArgs().toString());
    }

    @Test
    public void parseErrors() {
        assertThrows(JSONException.class, ()-> DCMessage.parse("{\"args\":{},"));
        assertThrows(JSONException.class, ()-> DCMessage.parse("{\"args\":{}}"));
        assertThrows(JSONException.class, ()-> DCMessage.parse("{\"args\":{},\"messageType\":\"EVEN\",\"name\":\"NAME2\"}"));
        assertThrows(JSONException.class, ()-> DCMessage.parse("{\"args\":{},\"messageType\":\"REQUEST\",\"name\":\"NAME2\"}"));
        assertThrows(JSONException.class, ()-> DCMessage.parse("{\"args\":{},\"messageType\":\"EVENT\",\"requestId\":25148}"));
    }
}