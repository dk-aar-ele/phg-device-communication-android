package dk.alexandra.phg.device_communication.api;

import static dk.alexandra.phg.device_communication.api.RequestIdCounter.requestIdCounter;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.skyscreamer.jsonassert.JSONAssert;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

@RunWith(MockitoJUnitRunner.class)
public class DCMessageDispatcherTest {

    @Mock
    private Consumer<String> dc2hostHandler;

    @Mock
    private Host2DCAPI host2dcHandler;

    private final Object dispatcherLock = new Object();
    private DCMessageDispatcher dispatcher;

    @Before
    public void setUp() {
        dispatcher = new DCMessageDispatcher(dc2hostHandler);
        reset(dc2hostHandler, host2dcHandler);
    }

    @After
    public void tearDown() {
        verifyNoMoreInteractions(dc2hostHandler);
        verifyNoMoreInteractions(host2dcHandler);
    }

    private void verifyDc2Host(String json) throws JSONException {
        verifyDc2Host(new String[] {json});
    }

    private void verifyDc2Host(String[] jsons) throws JSONException {
        ArgumentCaptor<String> callback = ArgumentCaptor.forClass(String.class);
        verify(dc2hostHandler, times(jsons.length)).accept(callback.capture());
        List<String> actualStrings = callback.getAllValues();
        assertEquals("Different number of messages dc2host", jsons.length, actualStrings.size());
        for (int i = 0; i < jsons.length; i++) {
            JSONAssert.assertEquals(jsons[i], actualStrings.get(i), true);
        }
    }
    
    
    /* receiveFromHost(): Garbage input handling */

    @Test
    public void receiveFromHostNull() {
        //noinspection ConstantConditions
        assertThrows(RuntimeException.class, ()->dispatcher.receiveFromHost(null, host2dcHandler));
    }

    @Test
    public void receiveFromHostInvalidJson() {
        assertThrows(RuntimeException.class, ()->dispatcher.receiveFromHost("-", host2dcHandler));
    }

    @Test
    public void receiveFromHostInvalidEnvelope() {
        assertThrows(RuntimeException.class, ()->dispatcher.receiveFromHost("{}", host2dcHandler));
    }


    /* receiveFromHost(): Request */

    @Test
    public void receiveFromHostSubscribeDeviceTypeSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"args\":{\"specialization\":4000000000}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).subscribeDeviceType(eq((int)4000000000L), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\"}");
    }

    @Test
    public void receiveFromHostSubscribeDeviceTypeReturnsNumberedException() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"args\":{\"specialization\":4000000000}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).subscribeDeviceType(eq((int)4000000000L), callback.capture());
        callback.getValue().error(42, "qwerty");
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"errorMessage\":\"qwerty\",\"errorCode\":42}");
    }

    @Test
    public void receiveFromHostSubscribeDeviceTypeThrowsNumberedException() throws NumberedException, JSONException {
        doThrow(new NumberedException(42,"qwerty")).when(host2dcHandler).subscribeDeviceType(eq((int)4000000000L), any());
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"args\":{\"specialization\":4000000000}}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"errorMessage\":\"qwerty\",\"errorCode\":42}");
        verify(host2dcHandler).subscribeDeviceType(eq(-294967296), any());
    }

    @Test
    public void receiveFromHostSubscribeDeviceTypeThrowsRuntimeException() throws NumberedException, JSONException {
        doThrow(new RuntimeException("abcde")).when(host2dcHandler).subscribeDeviceType(eq(0), any());
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"args\":{\"specialization\":0}}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"errorMessage\":\"abcde\",\"errorCode\":1}");
        verify(host2dcHandler).subscribeDeviceType(eq(0), any());
    }

    @Test
    public void receiveFromHostSubscribeDeviceTypeInvalidArgument() throws JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"args\":{\"specialization\":5e9}}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDeviceType\",\"errorMessage\":\"Argument missing, wrong type, or out of range: specialization\",\"errorCode\":2}");
    }

    @Test
    public void receiveFromHostUnsubscribeDeviceTypeSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"unsubscribeDeviceType\",\"args\":{\"specialization\":4}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).unsubscribeDeviceType(eq(4), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"unsubscribeDeviceType\"}");
    }

    @Test
    public void receiveFromHostSubscribeDeviceSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDevice\",\"args\":{\"persistentDeviceId\":\"qwertyuiop\"}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).subscribeDevice(eq("qwertyuiop"), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDevice\"}");
    }

    @Test
    public void receiveFromHostSubscribeDeviceInvalidArgument() throws JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"subscribeDevice\",\"args\":{\"persistentDeviceId\":1000}}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"subscribeDevice\",\"errorMessage\":\"Missing argument: persistentDeviceId\",\"errorCode\":2}");
    }

    @Test
    public void receiveFromHostUnsubscribeDeviceSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"unsubscribeDevice\",\"args\":{\"persistentDeviceId\":\"qwertyuiop\"}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).unsubscribeDevice(eq("qwertyuiop"), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"unsubscribeDevice\"}");
    }

    @Test
    public void receiveFromHostDeleteMetricSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"deleteMetric\"}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).deleteMetric(callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"deleteMetric\"}");
    }

    @Test
    public void receiveFromHostEnablePulseOxModalitySpotSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"args\":{\"normal\":false,\"fast\":false,\"slow\":false}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).enablePulseOxModalities(eq(false), eq(false), eq(false), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\"}");
    }

    @Test
    public void receiveFromHostEnablePulseOxModalitiesMissingArgument() throws JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\"}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"errorMessage\":\"Missing argument: normal\",\"errorCode\":2}");
    }

    @Test
    public void receiveFromHostEnablePulseOxModalitiesInvalidArgument() throws JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"args\":{\"normal\":false,\"fast\":42,\"slow\":false}}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"errorMessage\":\"Argument missing or wrong type: fast\",\"errorCode\":2}");
    }

    @Test
    public void receiveFromHostEnablePulseOxModalityNormalSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"args\":{\"normal\":true,\"fast\":false,\"slow\":false}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).enablePulseOxModalities(eq(true), eq(false), eq(false), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\"}");
    }

    @Test
    public void receiveFromHostEnablePulseOxModalityFastSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"args\":{\"normal\":false,\"fast\":true,\"slow\":false}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).enablePulseOxModalities(eq(false), eq(true), eq(false), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\"}");
    }

    @Test
    public void receiveFromHostEnablePulseOxModalitySlowSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\",\"args\":{\"normal\":false,\"fast\":false,\"slow\":true}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).enablePulseOxModalities(eq(false), eq(false), eq(true), callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"enablePulseOxModalities\"}");
    }

    @Test
    public void receiveFromHostGenerateFhirBundleSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"generateFhirBundle\"}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Void>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).generateFhirBundle(callback.capture());
        callback.getValue().success(null);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"generateFhirBundle\"}");
    }

    @Test
    public void receiveFromHostGetAvailableDevicesLinkedSuccess() throws NumberedException, JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"getAvailableDevices\",\"args\":{\"linked\":true,\"connected\":false,\"connectable\":false}}", host2dcHandler);
        @SuppressWarnings("unchecked")
        ArgumentCaptor<Callback<Set<Device>>> callback = ArgumentCaptor.forClass(Callback.class);
        verify(host2dcHandler).getAvailableDevices(eq(null), eq(true), eq(false), eq(false), callback.capture());
        HashSet<Device> set = new HashSet<>();
        set.add(Device.fromJson(new JSONObject("{\"serialNumber\":\"\",\"specializations\":[],\"name\":\"BLAbla\",\"persistentDeviceId\":\"abcdef\",\"modelNumber\":\"\",\"manufacturer\":\"\"}")));
        callback.getValue().success(set);
        verifyDc2Host("{\"args\":{\"value\":[{\"serialNumber\":\"\",\"specializations\":[],\"name\":\"BLAbla\",\"persistentDeviceId\":\"abcdef\",\"modelNumber\":\"\",\"manufacturer\":\"\"}]},\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"getAvailableDevices\"}");
    }

    @Test
    public void receiveFromHostInvalidRequest() throws JSONException {
        dispatcher.receiveFromHost("{\"messageType\":\"REQUEST\",\"requestId\":\"12345\",\"name\":\"foobar\"}", host2dcHandler);
        verifyDc2Host("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\",\"name\":\"foobar\",\"errorMessage\":\"Unexpected request name: foobar\",\"errorCode\":1}");
    }


    /* receiveFromHost(): Response */

    @Test
    public void receiveFromHostInvalidResponse() {
        assertThrows(RuntimeException.class, ()-> dispatcher.receiveFromHost("{\"messageType\":\"RESPONSE\",\"requestId\":\"12345\"}", host2dcHandler));
    }


    /* receiveFromHost(): Events */



    @Test
    public void receiveFromHostInvalidEvent() {
        assertThrows(IllegalArgumentException.class, ()-> dispatcher.receiveFromHost("{\"messageType\":\"EVENT\",\"name\":\"foobar\"}", host2dcHandler));
    }


    /* sendRequestToHost() and receiveFromHost(): Response */

    @Test
    @SuppressWarnings("unchecked")
    public void sendRequestToHostSingleRequestSuccessResponse() throws JSONException {
        int requestId;
        Callback<JSONObject> cb = mock(Callback.class);
        synchronized (dispatcherLock) {
            requestId = ++requestIdCounter;
            dispatcher.sendRequestToHost("Test", null, cb, x -> x);
        }
        verifyDc2Host("{\"messageType\":\"REQUEST\",\"requestId\":\"" + requestId + "\",\"name\":\"Test\"}");
        dispatcher.receiveFromHost("{\"messageType\":\"RESPONSE\",\"requestId\":\"" + requestId + "\",\"name\":\"Test\"}", host2dcHandler);
        verify(cb).success(null);
        verifyNoMoreInteractions(cb);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void sendRequestToHostSingleRequestExceptionResponse() throws JSONException {
        int requestId;
        Callback<JSONObject> cb = mock(Callback.class);
        synchronized (dispatcherLock) {
            requestId = ++requestIdCounter;
            dispatcher.sendRequestToHost("Test", null, cb, x -> x);
        }
        verifyDc2Host("{\"messageType\":\"REQUEST\",\"requestId\":\"" + requestId + "\",\"name\":\"Test\"}");
        dispatcher.receiveFromHost("{\"messageType\":\"RESPONSE\",\"requestId\":\"" + requestId +
                "\",\"errorMessage\":\"UPS\",\"name\":\"Test\"}", host2dcHandler);
        verify(cb).error(0, "UPS");
        verifyNoMoreInteractions(cb);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void sendRequestToHostThreeRequestsMixedResponses() throws JSONException {
        int requestId1;
        int requestId2;
        int requestId3;

        Callback<JSONObject> cb1 = mock(Callback.class);
        Callback<JSONObject> cb2 = mock(Callback.class);
        Callback<JSONObject> cb3 = mock(Callback.class);

        /*
         * First request   name: "First",  args: {val: 1}, response: {hello: 1}
         * Second request  name: "Second", args: null,     response: null
         * Third request   name: "Third",  args: {val: 3}, response: error 42, "FAILED"
         */
        synchronized (dispatcherLock) {
            dispatcher.sendRequestToHost("First", (new JSONObject()).put("val", 1), cb1, x -> x);
            requestId1 = ++requestIdCounter;
        }
        synchronized (dispatcherLock) {
            dispatcher.sendRequestToHost("Second", null, cb2, x -> x);
            requestId2 = ++requestIdCounter;
        }
        synchronized (dispatcherLock) {
            dispatcher.sendRequestToHost("Third", (new JSONObject()).put("val", 3), cb3, x -> x);
            requestId3 = ++requestIdCounter;
        }

        verifyDc2Host(new String[] {
                "{\"args\":{\"val\":1},\"messageType\":\"REQUEST\",\"requestId\":\"" + requestId1 + "\",\"name\":\"First\"}",
                "{\"messageType\":\"REQUEST\",\"requestId\":\"" + requestId2 + "\",\"name\":\"Second\"}",
                "{\"args\":{\"val\":3},\"messageType\":\"REQUEST\",\"requestId\":\"" + requestId3 + "\",\"name\":\"Third\"}"
        });

        dispatcher.receiveFromHost("{\"messageType\":\"RESPONSE\",\"requestId\":\"" + requestId3 +
                "\",\"errorCode\":42,\"errorMessage\":\"FAILED\",\"name\":\"Third\"}", host2dcHandler);
        dispatcher.receiveFromHost("{\"messageType\":\"RESPONSE\",\"requestId\":\"" + requestId2 +
                "\",\"name\":\"Second\"}", host2dcHandler);
        dispatcher.receiveFromHost("{\"args\":{\"hello\":1},\"messageType\":\"RESPONSE\",\"requestId\":\"" +
                requestId1 + "\",\"name\":\"First\"}", host2dcHandler);

        verify(cb1).success(any(JSONObject.class));
        verify(cb2).success(null);
        verify(cb3).error(42, "FAILED");

        verifyNoMoreInteractions(cb1);
        verifyNoMoreInteractions(cb2);
        verifyNoMoreInteractions(cb3);
    }


    /* sendEventToHost() */

    @Test
    public void sendEventToHost() throws JSONException {
        dispatcher.sendEventToHost("Test", null);
        verifyDc2Host("{\"messageType\":\"EVENT\",\"name\":\"Test\"}");
    }
}