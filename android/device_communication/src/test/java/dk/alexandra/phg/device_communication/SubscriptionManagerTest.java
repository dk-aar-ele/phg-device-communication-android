package dk.alexandra.phg.device_communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.atMostOnce;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.reset;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import android.app.ServiceStartNotAllowedException;
import android.content.Context;
import android.util.Log;

import androidx.annotation.Nullable;

import com.pcha.btmanager.PhdInformation;
import com.pcha.codephg.inter.SpecializationStruct;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedStatic;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import dk.alexandra.phg.device_communication.api.Callback;
import dk.alexandra.phg.device_communication.api.Device;
import dk.alexandra.phg.device_communication.api.ErrorCodes;
import dk.alexandra.phg.device_communication.api.NumberedException;
import dk.alexandra.phg.device_communication.manager.DeviceManager;

@RunWith(MockitoJUnitRunner.class)
public class SubscriptionManagerTest {

    private MockedStatic<Log> logMockedStatic;

    private MockedStatic<DeviceManager> deviceManagerMockedStatic;

    private MockedStatic<DeviceCommunicationService> deviceCommunicationServiceMockedStatic;

    private Context context;

    @Mock
    private BluetoothGuard bluetoothGuard;

    @Mock
    private Callback<Void> voidCallback;

    @Mock
    private Callback<Set<Device>> devicesCallback;

    private final Set<PhdInformation> whitelist = new HashSet<>();

    @Captor
    private ArgumentCaptor<Set<Device>> devices;

    @Captor
    private ArgumentCaptor<Set<PhdInformation>> phdInfos;

    private SubscriptionManager subscriptionManager;

    private enum GuardResult {NO_PERMS, NOT_ON, ON}

    @SuppressWarnings("unchecked")
    private void setupBluetoothGuard(GuardResult guardResult) {
        reset(bluetoothGuard);
        doAnswer(invocation -> {
            ((Consumer<Boolean>) invocation.getArguments()[1]).accept(
                    ((Boolean) invocation.getArguments()[0]) ?
                            (guardResult == GuardResult.ON) :
                            (guardResult != GuardResult.NO_PERMS)
            );
            return null;
        }).when(bluetoothGuard).bluetoothLEGuard(anyBoolean(), any());
    }

    private void setup(GuardResult guardResult) {
        MockitoAnnotations.openMocks(this);
        logMockedStatic = mockStatic(Log.class);
        context = mock(Context.class);
        deviceManagerMockedStatic = mockStatic(DeviceManager.class);
        whitelist.clear();
        deviceManagerMockedStatic.when(() -> DeviceManager.getWhitelist(any())).thenReturn(whitelist);
        deviceCommunicationServiceMockedStatic = mockStatic(DeviceCommunicationService.class);
        Supplier<Context> contextSupplier = () -> context;
        Supplier<BluetoothGuard> bluetoothGuardSupplier = () -> bluetoothGuard;
        setupBluetoothGuard(guardResult);
        subscriptionManager = new SubscriptionManager(contextSupplier, bluetoothGuardSupplier);
    }

    @After
    public void tearDown() {
        try {
            deviceManagerMockedStatic.verify(() -> DeviceManager.getWhitelist(any()), atMostOnce());
            deviceManagerMockedStatic.verifyNoMoreInteractions();
            deviceCommunicationServiceMockedStatic.verify(() -> DeviceCommunicationService.ensurePhdInfoFileIsLoaded(any()), atMostOnce());
            deviceCommunicationServiceMockedStatic.verifyNoMoreInteractions();
        } finally {
            if (deviceCommunicationServiceMockedStatic != null) {
                deviceCommunicationServiceMockedStatic.close();
                deviceCommunicationServiceMockedStatic = null;
            }
            if (deviceManagerMockedStatic != null) {
                deviceManagerMockedStatic.close();
                deviceManagerMockedStatic = null;
            }
            if (logMockedStatic != null) {
                logMockedStatic.close();
                logMockedStatic = null;
            }
        }
        verifyNoMoreInteractions(voidCallback);
        verifyNoMoreInteractions(devicesCallback);
        whitelist.clear();
    }

    /* Constructor functional */

    @Test
    public void canConstructObject() {
        setup(GuardResult.NO_PERMS);
        assertNotNull(subscriptionManager);
    }

    /* getAvailableDevices() */

    @Test
    public void getAvailableDevicesNoPermission() {
        setup(GuardResult.NO_PERMS);
        subscriptionManager.getAvailableDevices(null, true, false, false, devicesCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        ArgumentCaptor<NumberedException> exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(devicesCallback).error(exception.capture());
        assertEquals(ErrorCodes.BLUETOOTH_PERMISSION_NOT_GRANTED.value, exception.getValue().errorNumber);
    }

    private void getAvailableDevicesUnsupportedOperation(@Nullable Device prototypeDevice,
                                                         boolean linked, boolean connected,
                                                         boolean connectable) {
        setup(GuardResult.NOT_ON);
        subscriptionManager.getAvailableDevices(prototypeDevice, linked, connected,
                connectable, devicesCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        ArgumentCaptor<NumberedException> exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(devicesCallback).error(exception.capture());
        assertEquals(ErrorCodes.UNSUPPORTED_OPERATION.value, exception.getValue().errorNumber);
    }

    @Test
    public void getAvailableDevicesUnsupportedArg1() {
        getAvailableDevicesUnsupportedOperation(mock(Device.class), true, false,
                false);
    }

    @Test
    public void getAvailableDevicesUnsupportedArg2() {
        getAvailableDevicesUnsupportedOperation(null, false, false,
                false);
    }

    @Test
    public void getAvailableDevicesUnsupportedArg3() {
        getAvailableDevicesUnsupportedOperation(null, true, true,
                false);
    }

    @Test
    public void getAvailableDevicesUnsupportedArg4() {
        getAvailableDevicesUnsupportedOperation(null, true, false,
                true);
    }

    @Test
    public void getAvailableDevicesEmptyWhitelist() {
        setup(GuardResult.NOT_ON);
        subscriptionManager.getAvailableDevices(null, true, false,
                false, devicesCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        verify(devicesCallback).success(devices.capture());
        assertEquals(0, devices.getValue().size());
    }

    @Test
    public void getAvailableDevicesNonEmptyWhitelist() {
        setup(GuardResult.NOT_ON);
        whitelist.add(mock(PhdInformation.class));
        subscriptionManager.getAvailableDevices(null, true, false,
                false, devicesCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        verify(devicesCallback).success(devices.capture());
        assertEquals(1, devices.getValue().size());
    }

    private PhdInformation createPhdInfoMock(String sysId, int[] specs) {
        PhdInformation result = mock(PhdInformation.class);
        when(result.getSystemId()).thenReturn(sysId);
        when(result.getSpecializations()).thenReturn(new ArrayList<>(Arrays.stream(specs).boxed().map(spec -> new SpecializationStruct(Integer.toUnsignedLong(spec))).collect(Collectors.toList())));
        return result;
    }


    @Test
    @SuppressWarnings("unchecked")
    public void getAvailableDevicesWhitelistIsCached() {
        setup(GuardResult.NOT_ON);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.getAvailableDevices(null, true, false,
                false, devicesCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        verify(devicesCallback).success(devices.capture());
        assertEquals(1, devices.getValue().size());

        reset(devicesCallback);
        subscriptionManager.getAvailableDevices(null, true, false,
                false, devicesCallback);
        verify(devicesCallback).success(devices.capture());
        assertEquals(1, devices.getValue().size());
        // In tearDown() it is verified that DeviceManager.getWhitelist(any()) is called atMostOnce
    }


    /* subscribeDeviceType() */

    @Test
    public void subscribeDeviceTypeNoPermission() {
        setup(GuardResult.NO_PERMS);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.subscribeDeviceType(1, voidCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        ArgumentCaptor<NumberedException> exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(voidCallback).error(exception.capture());
        assertEquals(ErrorCodes.BLUETOOTH_PERMISSION_NOT_GRANTED.value, exception.getValue().errorNumber);
    }

    @Test
    public void subscribeDeviceTypeNoBluetoothTransceiver() {
        setup(GuardResult.NOT_ON);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.subscribeDeviceType(420000, voidCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(true), any());
        ArgumentCaptor<NumberedException> exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(voidCallback).error(exception.capture());
        assertEquals(ErrorCodes.BLUETOOTH_TURNED_OFF.value, exception.getValue().errorNumber);
    }

    @Test
    public void subscribeDeviceTypeSingleMatch() {
        setup(GuardResult.ON);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.subscribeDeviceType(420000, voidCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(true), any());
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()));
        assertEquals(1, phdInfos.getValue().size());
        assertEquals("FEEDADADDEADBEEF",
                ((PhdInformation) phdInfos.getValue().toArray()[0]).getSystemId());
    }


    /* unsubscribeDeviceType() */

    @Test
    public void unsubscribeDeviceTypeNoPermission() {
        setup(GuardResult.NO_PERMS);
        subscriptionManager.unsubscribeDeviceType(1, voidCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        ArgumentCaptor<NumberedException> exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(voidCallback).error(exception.capture());
        assertEquals(ErrorCodes.BLUETOOTH_PERMISSION_NOT_GRANTED.value, exception.getValue().errorNumber);
    }

    @Test
    public void unsubscribeDeviceTypeNoBluetoothTransceiver() {
        setup(GuardResult.NOT_ON);
        subscriptionManager.unsubscribeDeviceType(420000, voidCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(false), any());
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()));
        assertEquals(0, phdInfos.getValue().size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void unsubscribeDeviceTypeSingleMatch() {
        setup(GuardResult.ON);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.subscribeDeviceType(420000, voidCallback);
        verify(bluetoothGuard).bluetoothLEGuard(eq(true), any());
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()));
        assertEquals(1, phdInfos.getValue().size());
        assertEquals("FEEDADADDEADBEEF",
                ((PhdInformation) phdInfos.getValue().toArray()[0]).getSystemId());

        reset(voidCallback);
        subscriptionManager.unsubscribeDeviceType(420000, voidCallback);
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(2));
        assertEquals(0, phdInfos.getValue().size());
    }


    /* subscribeDevice() / unsubscribeDevice() */

    @Test
    public void subscribeDeviceSingleMatch() {
        setup(GuardResult.ON);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.subscribeDevice("FEEDADADDEADBEEF", voidCallback);
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()));
        assertEquals(1, phdInfos.getValue().size());
        assertEquals("FEEDADADDEADBEEF",
                ((PhdInformation) phdInfos.getValue().toArray()[0]).getSystemId());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void unsubscribeDeviceSingleMatch() {
        setup(GuardResult.ON);
        whitelist.add(createPhdInfoMock("FEEDADADDEADBEEF", new int[]{420000}));
        subscriptionManager.subscribeDevice("FEEDADADDEADBEEF", voidCallback);
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()));
        assertEquals(1, phdInfos.getValue().size());
        assertEquals("FEEDADADDEADBEEF",
                ((PhdInformation) phdInfos.getValue().toArray()[0]).getSystemId());

        reset(voidCallback);
        subscriptionManager.unsubscribeDevice("FEEDADADDEADBEEF", voidCallback);
        verify(voidCallback).success(null);
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(2));
        assertEquals(0, phdInfos.getValue().size());

    }


    /* Complex subscription */

    private ArrayList<PhdInformation> bigWhitelist() {
        ArrayList<PhdInformation> result = new ArrayList<>();
        result.add(createPhdInfoMock("0000000000000001", new int[]{0xffffffff, 0xdeafbeef})); //x
        result.add(createPhdInfoMock("0000000000000002", new int[]{0x80000001, 0x70000000})); //x
        result.add(createPhdInfoMock("0000000000000003", new int[]{0x00010001, 0x00020002}));
        result.add(createPhdInfoMock("0000000000000004", new int[]{0x12345678, 0x87654321}));
        result.add(createPhdInfoMock("0000000000000005", new int[]{0x11111111, 0x22222222})); //x
        return result;
    }

    private Set<String> getIds(Set<PhdInformation> phdInformations) {
        return phdInformations.stream().map(PhdInformation::getSystemId).collect(Collectors.toSet());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void subscribeDeviceComplex() {
        setup(GuardResult.ON);
        whitelist.addAll(bigWhitelist());
        subscriptionManager.subscribeDeviceType(-1, voidCallback);
        subscriptionManager.subscribeDeviceType(0x00020002, voidCallback);
        subscriptionManager.subscribeDevice("FEEDADADDEADBEEF", voidCallback);
        subscriptionManager.subscribeDeviceType(420000, voidCallback);
        subscriptionManager.subscribeDeviceType(0x11111111, voidCallback);
        subscriptionManager.subscribeDevice("0000000000000004", voidCallback);
        subscriptionManager.subscribeDeviceType(0x22222222, voidCallback);
        verify(voidCallback, times(7)).success(null);
        verify(voidCallback, never()).error(any());
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(7));
        Set<String> ids = getIds(phdInfos.getValue());
        assertEquals(4, ids.size());
        assertTrue(ids.contains("0000000000000001"));
        assertTrue(ids.contains("0000000000000003"));
        assertTrue(ids.contains("0000000000000004"));
        assertTrue(ids.contains("0000000000000005"));

        reset(voidCallback);
        subscriptionManager.unsubscribeDeviceType(0x00020002, voidCallback);
        subscriptionManager.unsubscribeDevice("FEEDADADDEADBEEF", voidCallback);
        subscriptionManager.unsubscribeDevice("0000000000000004", voidCallback);
        subscriptionManager.unsubscribeDeviceType(0x22222222, voidCallback);
        subscriptionManager.subscribeDeviceType(0x80000001, voidCallback);
        verify(voidCallback, times(5)).success(null);
        verify(voidCallback, never()).error(any());
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(12));
        ids = getIds(phdInfos.getValue());
        assertEquals(3, ids.size());
        assertTrue(ids.contains("0000000000000001"));
        assertTrue(ids.contains("0000000000000002"));
        assertTrue(ids.contains("0000000000000005"));
    }

    /* Don't subscribe / unsubscribe if Bluetooth is off */

    @Test
    @SuppressWarnings("unchecked")
    public void subscribeDeviceBluetoothTemporaryOff() {
        setup(GuardResult.ON);
        whitelist.addAll(bigWhitelist());
        subscriptionManager.subscribeDeviceType(0xdeafbeef, voidCallback);
        subscriptionManager.subscribeDevice("0000000000000004", voidCallback);
        verify(voidCallback, times(2)).success(null);
        verify(voidCallback, never()).error(any());
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(2));
        Set<String> ids = getIds(phdInfos.getValue());
        assertEquals(2, ids.size());
        assertTrue(ids.contains("0000000000000001"));
        assertTrue(ids.contains("0000000000000004"));

        // Turning Bluetooth off: New subscriptions shall not be accepted.
        setupBluetoothGuard(GuardResult.NOT_ON);
        reset(voidCallback);
        subscriptionManager.subscribeDeviceType(0x80000001, voidCallback);
        subscriptionManager.subscribeDevice("0000000000000003", voidCallback);
        verify(voidCallback, never()).success(null);
        ArgumentCaptor<NumberedException> exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(voidCallback, times(2)).error(exception.capture());
        assertEquals(ErrorCodes.BLUETOOTH_TURNED_OFF.value, exception.getValue().errorNumber);
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(2)); // No change since last

        // Bluetooth still off:  Existing subscriptions cannot be cleared
        reset(voidCallback);
        subscriptionManager.unsubscribeDeviceType(0xdeafbeef, voidCallback);
        subscriptionManager.unsubscribeDevice("0000000000000004", voidCallback);
        exception = ArgumentCaptor.forClass(NumberedException.class);
        verify(voidCallback, times(2)).error(exception.capture());
        assertEquals(ErrorCodes.BLUETOOTH_TURNED_OFF.value, exception.getValue().errorNumber);
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(2)); // Still no change...

        // Turning Bluetooth back on: New subscriptions can be added
        setupBluetoothGuard(GuardResult.ON);
        reset(voidCallback);
        subscriptionManager.subscribeDevice("0000000000000005", voidCallback);
        verify(voidCallback).success(null);
        verify(voidCallback, never()).error(any());
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(3)); // This time it was invoked
        ids = getIds(phdInfos.getValue());
        assertEquals(3, ids.size());
        assertTrue(ids.contains("0000000000000001"));
        assertTrue(ids.contains("0000000000000004"));
        assertTrue(ids.contains("0000000000000005"));

        // Bluetooth still on: Subscriptions can be removed
        reset(voidCallback);
        subscriptionManager.unsubscribeDeviceType(0xdeafbeef, voidCallback);
        subscriptionManager.unsubscribeDevice("0000000000000004", voidCallback);
        subscriptionManager.unsubscribeDevice("0000000000000005", voidCallback);
        verify(voidCallback, times(3)).success(null);
        verify(voidCallback, never()).error(any());
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), phdInfos.capture()),
                times(6));
        assertEquals(0, phdInfos.getValue().size());
    }


    /* Don't subscribe if service fails to start */

    @Test
    public void subscribeDeviceServiceFails() {
        setup(GuardResult.ON);
        whitelist.addAll(bigWhitelist());
        deviceCommunicationServiceMockedStatic.when(() -> DeviceCommunicationService.updateSubscriptions(any(), any()))
                .thenThrow(mock(ServiceStartNotAllowedException.class));
        subscriptionManager.subscribeDeviceType(0xdeafbeef, voidCallback);
        subscriptionManager.subscribeDevice("0000000000000004", voidCallback);
        verify(voidCallback, never()).success(null);
        verify(voidCallback, times(2)).error(eq(1), any());
        deviceCommunicationServiceMockedStatic.verify(() ->
                        DeviceCommunicationService.updateSubscriptions(any(), any()),
                times(2));
    }
}