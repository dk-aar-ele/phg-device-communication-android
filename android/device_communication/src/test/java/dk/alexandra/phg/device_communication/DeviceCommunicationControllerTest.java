package dk.alexandra.phg.device_communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.activity.ComponentActivity;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Constructor;
import java.util.Set;
import java.util.function.Consumer;
import java.util.function.Supplier;

import dk.alexandra.phg.device_communication.api.Callback;
import dk.alexandra.phg.device_communication.api.DCMessageDispatcher;
import dk.alexandra.phg.device_communication.api.Device;
import dk.alexandra.phg.device_communication.api.Host2DCAPI;
import dk.alexandra.phg.device_communication.manager.DeviceManager;

@RunWith(MockitoJUnitRunner.class)
public class DeviceCommunicationControllerTest {

    private MockedStatic<DeviceManager> deviceManagerMockedStatic;
    private MockedStatic<Looper> looperMockedStatic;

    private MockedConstruction<Handler> handlerMockedConstruction;
    private MockedConstruction<SubscriptionManager> subscriptionManagerMockedConstruction;
    private MockedConstruction<BluetoothGuard> bluetoothGuardMockedConstruction;
    private MockedConstruction<DeviceManager> deviceManagerMockedConstruction;
    private MockedConstruction<DCMessageDispatcher> dcMessageDispatcherMockedConstruction;

    @Mock
    private Context context;

    @Mock
    private Context applicationContext;

    @Mock
    private Consumer<String> dc2hostHandler;

    @Mock
    private Consumer<Boolean> booleanResult;

    @Mock
    private Callback<Void> voidCallback;

    @Mock
    private Callback<Set<Device>> setCallback;


    @Captor
    private ArgumentCaptor<Runnable> runnable;


    private Supplier<Context> subscriptionManagerConstructorFirstArgument_applicationContextSupplier;
    private Supplier<BluetoothGuard> subscriptionManagerConstructorSecondArgument_bluetoothGuardSupplier;

    private Context bluetoothGuardConstructorFirstArgument_context;

    private ComponentActivity deviceManagerConstructorFirstArgument_activity;
    private BluetoothGuard deviceManagerConstructorSecondArgument_bluetoothGuard;

    private Consumer<String> dcMessageDispatcherConstructorFirstArgument_dc2hostHandler;

    private DeviceCommunicationController deviceCommunicationController;
    private BluetoothGuard bluetoothGuard;
    private DCMessageDispatcher messageDispatcher;
    private SubscriptionManager subscriptionManager;

    @SuppressWarnings("unchecked")
    private void setup() throws Exception {
        MockitoAnnotations.openMocks(this);
        deviceManagerMockedStatic = mockStatic(DeviceManager.class);
        looperMockedStatic = mockStatic(Looper.class);
        assertNotNull(DeviceCommunicationController.shared);
        handlerMockedConstruction = mockConstruction(Handler.class);
        subscriptionManagerMockedConstruction = mockConstruction(SubscriptionManager.class,
                (mock, context) -> {
                    assertEquals(2, context.arguments().size());
                    subscriptionManagerConstructorFirstArgument_applicationContextSupplier
                            = (Supplier<Context>) context.arguments().get(0);
                    subscriptionManagerConstructorSecondArgument_bluetoothGuardSupplier
                            = (Supplier<BluetoothGuard>) context.arguments().get(1);
                    subscriptionManager = mock;
                });
        bluetoothGuardMockedConstruction = mockConstruction(BluetoothGuard.class,
                (mock, context) -> {
                    assertEquals(1, context.arguments().size());
                    bluetoothGuardConstructorFirstArgument_context
                            = (Context) context.arguments().get(0);
                    bluetoothGuard = mock;
                });
        deviceManagerMockedConstruction = mockConstruction(DeviceManager.class,
                (mock, context) -> {
                    assertEquals(2, context.arguments().size());
                    deviceManagerConstructorFirstArgument_activity
                            = (ComponentActivity) context.arguments().get(0);
                    deviceManagerConstructorSecondArgument_bluetoothGuard
                            = (BluetoothGuard) context.arguments().get(1);
                });
        dcMessageDispatcherMockedConstruction = mockConstruction(DCMessageDispatcher.class,
                (mock, context) -> {
                    assertEquals(1, context.arguments().size());
                    dcMessageDispatcherConstructorFirstArgument_dc2hostHandler
                            = (Consumer<String>) context.arguments().get(0);
                    messageDispatcher = mock;
                });

        Constructor<DeviceCommunicationController> constructor
                = DeviceCommunicationController.class.getDeclaredConstructor();
        constructor.setAccessible(true);
        deviceCommunicationController = constructor.newInstance();
        assertEquals(1, handlerMockedConstruction.constructed().size());
        when(handlerMockedConstruction.constructed().get(0)
                .post(runnable.capture())).thenReturn(true);
        when(context.getApplicationContext()).thenReturn(applicationContext);
    }

    @After
    public void tearDown() {
        if (dcMessageDispatcherMockedConstruction != null) {
            dcMessageDispatcherMockedConstruction.close();
            dcMessageDispatcherMockedConstruction = null;
        }
        if (deviceManagerMockedConstruction != null) {
            deviceManagerMockedConstruction.close();
            deviceManagerMockedConstruction = null;
        }
        if (bluetoothGuardMockedConstruction != null) {
            bluetoothGuardMockedConstruction.close();
            bluetoothGuardMockedConstruction = null;
        }
        if (subscriptionManagerMockedConstruction != null) {
            subscriptionManagerMockedConstruction.close();
            subscriptionManagerMockedConstruction = null;
        }
        if (handlerMockedConstruction != null) {
            handlerMockedConstruction.close();
            handlerMockedConstruction = null;
        }
        if (looperMockedStatic != null) {
            looperMockedStatic.close();
            looperMockedStatic = null;
        }
        if (deviceManagerMockedStatic != null) {
            deviceManagerMockedStatic.close();
            deviceManagerMockedStatic = null;
        }
    }

    /* Constructor functional */

    @Test
    public void canConstructObject() throws Exception {
        setup();
        assertNotNull(deviceCommunicationController);
    }

    /* Initialize functional */

    @Test
    public void canInitialize() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        assertNotNull(host2dcHandler);
    }

    /* Check BluetoothGuard and SubscriptionManager arguments */

    @Test
    public void bluetoothManagerContextArgument() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        assertEquals(applicationContext, bluetoothGuardConstructorFirstArgument_context);
    }

    @Test
    public void subscriptionManagerContextArgument() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        assertEquals(applicationContext,
                subscriptionManagerConstructorFirstArgument_applicationContextSupplier.get());
    }

    @Test
    public void subscriptionManagerBluetoothGuardArgument() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        assertEquals(bluetoothGuard,
                subscriptionManagerConstructorSecondArgument_bluetoothGuardSupplier.get());
    }

    /* getActivityLifecycleCallbacks() */

    @Test
    public void getActivityLifecycleCallbacksNonInitialized() throws Exception {
        setup();
        assertThrows(IllegalStateException.class,
                () -> deviceCommunicationController.getActivityLifecycleCallbacks());
    }

    @Test
    public void getActivityLifecycleCallbacks() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        Application.ActivityLifecycleCallbacks alcMock
                = mock(Application.ActivityLifecycleCallbacks.class);
        when(bluetoothGuard.getActivityLifecycleCallbacks()).thenReturn(alcMock);
        Application.ActivityLifecycleCallbacks alc
                = deviceCommunicationController.getActivityLifecycleCallbacks();
        assertEquals(alcMock, alc);
        verify(bluetoothGuard).getActivityLifecycleCallbacks();
    }

    /* getDeviceManager() */

    @Test
    public void getDeviceManager() throws Exception {
        setup();
        ComponentActivity activity = mock(ComponentActivity.class);
        DeviceManager dm = deviceCommunicationController.getDeviceManager(activity);
        assertNotNull(dm);
        assertEquals(activity, deviceManagerConstructorFirstArgument_activity);
        assertEquals(bluetoothGuard, deviceManagerConstructorSecondArgument_bluetoothGuard);
    }

    /* start/end device manager session */

    @Test
    public void startDeviceManagerSessionNonInitialized() throws Exception {
        setup();
        assertThrows(IllegalStateException.class,
                () -> deviceCommunicationController.startDeviceManagerSession(booleanResult));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void startDeviceManagerSessionNoPermsImmediately() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        doAnswer(invocation -> {
            ((Consumer<Boolean>) invocation.getArguments()[1]).accept(false);
            return null;
        }).when(bluetoothGuard).bluetoothLEGuard(anyBoolean(), any());
        deviceCommunicationController.startDeviceManagerSession(booleanResult);
        verify(booleanResult).accept(false);
        deviceManagerMockedStatic.verify(
                () -> DeviceManager.beginSessionIfNotStarted(any()), never());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void startDeviceManagerSessionNoPermsDelayed() throws Exception {
        setup();
        final Boolean[] invocationHasReturned = new Boolean[1];
        invocationHasReturned[0] = false;
        deviceCommunicationController.initialize(context, dc2hostHandler);
        doAnswer(invocation -> {
            (new Thread(() -> {
                try {
                    synchronized (invocationHasReturned) {
                        while (!invocationHasReturned[0]) invocationHasReturned.wait();
                    }
                } catch (InterruptedException ignored) {}
                ((Consumer<Boolean>) invocation.getArguments()[1]).accept(false);
            })).start();
            return null;
        }).when(bluetoothGuard).bluetoothLEGuard(anyBoolean(), any());
        deviceCommunicationController.startDeviceManagerSession(booleanResult);
        synchronized (invocationHasReturned) {
            invocationHasReturned[0] = true;
            invocationHasReturned.notifyAll();
        }
        verify(booleanResult, timeout(500)).accept(false);
        deviceManagerMockedStatic.verify(
                () -> DeviceManager.beginSessionIfNotStarted(any()),
                timeout(500).times(0));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void startDeviceManagerSessionPermsOkImmediately() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        doAnswer(invocation -> {
            ((Consumer<Boolean>) invocation.getArguments()[1]).accept(true);
            return null;
        }).when(bluetoothGuard).bluetoothLEGuard(anyBoolean(), any());
        deviceCommunicationController.startDeviceManagerSession(booleanResult);
        verify(booleanResult).accept(true);
        deviceManagerMockedStatic.verify(
                () -> DeviceManager.beginSessionIfNotStarted(eq(applicationContext)));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void startDeviceManagerSessionPermsOkDelayed() throws Exception {
        setup();
        final Boolean[] invocationHasReturned = new Boolean[1];
        deviceCommunicationController.initialize(context, dc2hostHandler);
        doAnswer(invocation -> {
            (new Thread(() -> {
                try (MockedStatic<DeviceManager> devMgr = mockStatic(DeviceManager.class)) {
                    synchronized (invocationHasReturned) {
                        while (!invocationHasReturned[0]) invocationHasReturned.wait();
                    }
                    ((Consumer<Boolean>) invocation.getArguments()[1]).accept(true);
                    devMgr.verify(
                            () -> DeviceManager.beginSessionIfNotStarted(eq(applicationContext)));
                } catch (InterruptedException ignored) {
                }
            })).start();
            return null;
        }).when(bluetoothGuard).bluetoothLEGuard(anyBoolean(), any());
        deviceCommunicationController.startDeviceManagerSession(booleanResult);
        synchronized (invocationHasReturned) {
            invocationHasReturned[0] = true;
            invocationHasReturned.notifyAll();
        }
        verify(booleanResult, Mockito.timeout(500)).accept(true);
    }

    @Test
    public void endDeviceManagerSession() throws Exception {
        setup();
        deviceCommunicationController.endDeviceManagerSession();
        deviceManagerMockedStatic.verify(DeviceManager::endSession);
    }


    /* Message dispatching */

    @Test
    public void dc2hostHandlerDelivered() throws Exception {
        setup();
        deviceCommunicationController.initialize(context, dc2hostHandler);
        assertNotNull(messageDispatcher);
        assertEquals(dc2hostHandler, dcMessageDispatcherConstructorFirstArgument_dc2hostHandler);
    }

    @Test
    public void host2dcMessageSubscribeDeviceType() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        host2dcHandler.accept("FOOBAR");
        ArgumentCaptor<Host2DCAPI> dispatcherCaptor = ArgumentCaptor.forClass(Host2DCAPI.class);
        verify(messageDispatcher).receiveFromHost(eq("FOOBAR"), dispatcherCaptor.capture());
        Host2DCAPI dispatcher = dispatcherCaptor.getValue();

        dispatcher.subscribeDeviceType(42, voidCallback);
        runnable.getValue().run();
        verify(subscriptionManager).subscribeDeviceType(42, voidCallback);
    }

    @Test
    public void host2dcMessageSubscribeDevice() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        host2dcHandler.accept("FOOBAR");
        ArgumentCaptor<Host2DCAPI> dispatcherCaptor = ArgumentCaptor.forClass(Host2DCAPI.class);
        verify(messageDispatcher).receiveFromHost(eq("FOOBAR"), dispatcherCaptor.capture());
        Host2DCAPI dispatcher = dispatcherCaptor.getValue();

        dispatcher.subscribeDevice("BAZ", voidCallback);
        runnable.getValue().run();
        verify(subscriptionManager).subscribeDevice("BAZ", voidCallback);
    }

    @Test
    public void host2dcMessageUnsubscribeDeviceType() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        host2dcHandler.accept("FOOBAR");
        ArgumentCaptor<Host2DCAPI> dispatcherCaptor = ArgumentCaptor.forClass(Host2DCAPI.class);
        verify(messageDispatcher).receiveFromHost(eq("FOOBAR"), dispatcherCaptor.capture());
        Host2DCAPI dispatcher = dispatcherCaptor.getValue();

        dispatcher.unsubscribeDeviceType(24, voidCallback);
        runnable.getValue().run();
        verify(subscriptionManager).unsubscribeDeviceType(24, voidCallback);
    }

    @Test
    public void host2dcMessageUnsubscribeDevice() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        host2dcHandler.accept("FOOBAR");
        ArgumentCaptor<Host2DCAPI> dispatcherCaptor = ArgumentCaptor.forClass(Host2DCAPI.class);
        verify(messageDispatcher).receiveFromHost(eq("FOOBAR"), dispatcherCaptor.capture());
        Host2DCAPI dispatcher = dispatcherCaptor.getValue();

        dispatcher.unsubscribeDevice("ZAB", voidCallback);
        runnable.getValue().run();
        verify(subscriptionManager).unsubscribeDevice("ZAB", voidCallback);
    }

    @Test
    public void host2dcMessageGetAvailableDevices() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        host2dcHandler.accept("FOOBAR");
        ArgumentCaptor<Host2DCAPI> dispatcherCaptor = ArgumentCaptor.forClass(Host2DCAPI.class);
        verify(messageDispatcher).receiveFromHost(eq("FOOBAR"), dispatcherCaptor.capture());
        Host2DCAPI dispatcher = dispatcherCaptor.getValue();

        Device d = mock(Device.class);

        dispatcher.getAvailableDevices(d, true, false, false, setCallback);
        runnable.getValue().run();
        verify(subscriptionManager).getAvailableDevices(d, true, false, false, setCallback);

        dispatcher.getAvailableDevices(null, false, true, false, setCallback);
        runnable.getValue().run();
        verify(subscriptionManager).getAvailableDevices(null, false, true, false, setCallback);

        dispatcher.getAvailableDevices(null, false, false, true, setCallback);
        runnable.getValue().run();
        verify(subscriptionManager).getAvailableDevices(null, false, false, true, setCallback);
    }

    @Test
    public void host2dcMessageEnablePulseOxModalities() throws Exception {
        setup();
        Consumer<String> host2dcHandler =
                deviceCommunicationController.initialize(context, dc2hostHandler);
        host2dcHandler.accept("FOOBAR");
        ArgumentCaptor<Host2DCAPI> dispatcherCaptor = ArgumentCaptor.forClass(Host2DCAPI.class);
        verify(messageDispatcher).receiveFromHost(eq("FOOBAR"), dispatcherCaptor.capture());
        Host2DCAPI dispatcher = dispatcherCaptor.getValue();

        try (MockedStatic<DeviceCommunicationService> deviceCommunicationServiceMockedStatic
                     = mockStatic(DeviceCommunicationService.class)) {

            dispatcher.enablePulseOxModalities(true, false, false, voidCallback);
            runnable.getValue().run();
            deviceCommunicationServiceMockedStatic.verify(
                    () -> DeviceCommunicationService.enablePulseOxModalities(true, false, false));
            verify(voidCallback).success(null);

            dispatcher.enablePulseOxModalities(false, true, false, voidCallback);
            runnable.getValue().run();
            deviceCommunicationServiceMockedStatic.verify(
                    () -> DeviceCommunicationService.enablePulseOxModalities(false, true, false));
            verify(voidCallback, times(2)).success(null);

            dispatcher.enablePulseOxModalities(false, false, true, voidCallback);
            runnable.getValue().run();
            deviceCommunicationServiceMockedStatic.verify(
                    () -> DeviceCommunicationService.enablePulseOxModalities(false, false, true));
            verify(voidCallback, times(3)).success(null);
        }
    }
}