package dk.alexandra.phg.device_communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.lenient;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContract;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.core.content.ContextCompat;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockedStatic;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.util.LinkedList;
import java.util.function.Consumer;

@RunWith(MockitoJUnitRunner.class)
public class BluetoothGuardTest {

    private MockedStatic<Log> log;

    private MockedStatic<ContextCompat> contextCompat;

    private Context context;

    private Consumer<Boolean> callback;

    private BluetoothGuard bluetoothGuard;

    private BluetoothManager bluetoothManager;

    private final String permission = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ?
            Manifest.permission.BLUETOOTH_CONNECT : Manifest.permission.BLUETOOTH_ADMIN;


    private void setup(boolean hasFeatureLE, boolean hasBtAdapter, boolean hasBtConnectPerms,
                       boolean transceiverEnabled, Activity activity) {
        log = mockStatic(Log.class);
        contextCompat = mockStatic(ContextCompat.class);
        context = mock(Context.class);
        @SuppressWarnings("unchecked")
        Consumer<Boolean> callback = mock(Consumer.class);
        this.callback = callback;
        PackageManager pm = mock(PackageManager.class);
        bluetoothManager = mock(BluetoothManager.class);
        BluetoothAdapter ba = null;
        if (hasBtAdapter) {
            ba = mock(BluetoothAdapter.class);
            lenient().when(ba.isEnabled()).thenReturn(transceiverEnabled);
        }
        when(context.getPackageManager()).thenReturn(pm);
        lenient().when(context.getSystemService(BluetoothManager.class)).thenReturn(bluetoothManager);
        lenient().when(pm.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)).thenReturn(hasFeatureLE);
        lenient().when(bluetoothManager.getAdapter()).thenReturn(ba);
        contextCompat.when(() -> ContextCompat.checkSelfPermission(context, permission))
                .thenReturn(hasBtConnectPerms ? PackageManager.PERMISSION_GRANTED : PackageManager.PERMISSION_DENIED);
        bluetoothGuard = new BluetoothGuard(context);
        if (activity != null) {
            Application.ActivityLifecycleCallbacks alc = bluetoothGuard.getActivityLifecycleCallbacks();
            alc.onActivityCreated(activity, null);
            alc.onActivityStarted(activity);
            alc.onActivityResumed(activity);
        }
    }

    private ComponentActivity activityMock(LinkedList<Boolean> userResponses) {
        ComponentActivity ca = mock(ComponentActivity.class);

        ActivityResultContract<Intent,ActivityResult> enableContract = any(ActivityResultContracts.StartActivityForResult.class);
        @SuppressWarnings("unchecked")
        ActivityResultCallback<ActivityResult> arc = any(ActivityResultCallback.class);
        lenient().when(ca.registerForActivityResult(enableContract, arc))
                .thenAnswer((Answer<ActivityResultLauncher<Intent>>) invocation -> {
                    ActivityResultCallback<ActivityResult> callback = invocation.getArgument(1);
                    @SuppressWarnings("unchecked")
                    ActivityResultLauncher<Intent> result = mock(ActivityResultLauncher.class);
                    lenient().doAnswer((Answer<Void>) unused -> {
                        ActivityResult activityResult = mock(ActivityResult.class);
                        when(activityResult.getResultCode()).thenReturn(userResponses.remove(0)? Activity.RESULT_OK : Activity.RESULT_CANCELED);
                        callback.onActivityResult(activityResult);
                        return null;
                    }).when(result).launch(any(Intent.class));
                    return result;
                });

        ActivityResultContract<String,Boolean> permissionContract = any(ActivityResultContracts.RequestPermission.class);
        @SuppressWarnings("unchecked")
        ActivityResultCallback<Boolean> bc = any(ActivityResultCallback.class);
        lenient().when(ca.registerForActivityResult(permissionContract, bc))
                .thenAnswer((Answer<ActivityResultLauncher<String>>) invocation -> {
                    ActivityResultCallback<Boolean> callback = invocation.getArgument(1);
                    @SuppressWarnings("unchecked")
                    ActivityResultLauncher<String> result = mock(ActivityResultLauncher.class);
                    lenient().doAnswer((Answer<Void>) unused -> {
                        callback.onActivityResult(userResponses.remove(0));
                        return null;
                    }).when(result).launch(any(String.class));
                    return result;
                });
        return ca;
    }

    @After
    public void tearDown() {
        if (log != null) {
            log.close();
            log = null;
        }
        if (contextCompat != null) {
            contextCompat.close();
            contextCompat = null;
        }
        verifyNoMoreInteractions(callback);
    }

    /* Constructor and getActivityLifecycleCallbacks() functional */

    @Test
    public void canConstructObject() {
        setup(true, true, false, false, null);
        assertNotNull(bluetoothGuard.getActivityLifecycleCallbacks());
    }

    /* No system / hardware support for Bluetooth LE */

    @Test
    public void noBleSupport() {
        setup(false, true, false, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback).accept(false);
    }

    @Test
    public void noBluetoothAdapter() {
        setup(true, false, false, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback).accept(false);
    }

    /* No permissions for BLUETOOTH_CONNECT */

    @Test
    public void noPermissionsAndNoActivityRegistered() {
        setup(true, true, false, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback).accept(false);
    }

    /* Permissions for BLUETOOTH_CONNECT and no transceiver test */

    @Test
    public void allOkWithNoTransceiverTest() {
        setup(true, true, true, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback).accept(true);
    }

    /* Permissions for BLUETOOTH_CONNECT but transceiver is off */

    @Test
    public void transceiverOffAndNoActivityRegistered() {
        setup(true, true, true, false, null);
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(false);
    }

    /* Permissions for BLUETOOTH_CONNECT and transceiver is on */

    @Test
    public void allClearAndNoActivityRegistered() {
        setup(true, true, true, true, null);
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(true);
    }

    /* No adapter results in repeated checks */

    @Test
    public void failingAdapterChecksWillBeRepeated() {
        setup(true, false, true, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback, times(2)).accept(false);
        verify(bluetoothManager, times(2)).getAdapter();
    }

    /* No permissions for BLUETOOTH_CONNECT results in repeated checks */

    @Test
    public void failingPermsChecksWillBeRepeated() {
        setup(true, true, false, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback, times(2)).accept(false);
        contextCompat.verify(() -> ContextCompat.checkSelfPermission(context,
                permission), times(2));
    }

    /* All OK will not result in repeated checks */

    @Test
    public void allOkWillNotResultInRepeatedChecks() {
        setup(true, true, true, false, null);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback, times(2)).accept(true);
        verify(bluetoothManager, times(1)).getAdapter();
        contextCompat.verify(() -> ContextCompat.checkSelfPermission(context,
                permission), times(1));
    }

    /* No permissions for BLUETOOTH_CONNECT with activity registered */

    @Test
    public void noPermissionsWithActivityRegisteredAndUserReject() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(false);
        setup(true, true, false, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback).accept(false);
        assertEquals(0, userAnswers.size());
    }

    @Test
    public void noPermissionsWithActivityRegisteredAndUserAccept() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(true);
        setup(true, true, false, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verify(callback).accept(true);
        assertEquals(0, userAnswers.size());
    }

    /* Bluetooth transceiver is off with activity registered */

    @Test
    public void transceiverOffWithActivityRegisteredAndUserReject() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(false);
        setup(true, true, true, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(false);
        assertEquals(0, userAnswers.size());
    }

    @Test
    public void transceiverOffWithActivityRegisteredAndUserAccept() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(true);
        setup(true, true, true, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(true);
        assertEquals(0, userAnswers.size());
    }

    /* Combinations */

    @Test
    public void noPermissionsAndCheckTransceiverWithActivityRegisteredAndUserAccept() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(true);
        setup(true, true, false, true, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(true);
        assertEquals(0, userAnswers.size());
    }

    @Test
    public void noPermissionsAndTransceiverOffWithActivityRegisteredAndUserRejectFirst() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(false);
        setup(true, true, false, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(false);
        assertEquals(0, userAnswers.size());
    }

    @Test
    public void noPermissionsAndTransceiverOffWithActivityRegisteredAndUserRejectSecond() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(true);
        userAnswers.add(false);
        setup(true, true, false, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(false);
        assertEquals(0, userAnswers.size());
    }

    @Test
    public void noPermissionsAndTransceiverOffWithActivityRegisteredAndUserAccept() {
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        userAnswers.add(true);
        userAnswers.add(true);
        setup(true, true, false, false, activityMock(userAnswers));
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verify(callback).accept(true);
        assertEquals(0, userAnswers.size());
    }

    /* A regular Activity has focus when the permission request is made */

    @Test
    public void noPermissionsWithSimpleActivityRegistered() {
        // First setup with ordinary ComponentActivity
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        Activity firstActivity = activityMock(userAnswers);
        setup(true, true, false, false, firstActivity);
        // Then navigate to a simple Activity
        Activity secondActivity = mock(Activity.class);
        Application.ActivityLifecycleCallbacks alc = bluetoothGuard.getActivityLifecycleCallbacks();
        alc.onActivityPaused(firstActivity);
        alc.onActivityCreated(secondActivity, null);
        alc.onActivityStarted(secondActivity);
        alc.onActivityResumed(secondActivity);
        alc.onActivityStopped(firstActivity);
        bluetoothGuard.bluetoothLEGuard(false, callback);
        verifyNoInteractions(callback); // Not touched yet
        // Now, navigate back to the original activity and interact with the user
        userAnswers.add(false);
        alc.onActivityPaused(secondActivity);
        alc.onActivityStarted(firstActivity);
        alc.onActivityResumed(firstActivity);
        alc.onActivityStopped(secondActivity);
        alc.onActivityDestroyed(secondActivity);
        verify(callback).accept(false);
        assertEquals(0, userAnswers.size());
    }

    @Test
    public void noPermissionsAndTransceiverOnWithSimpleActivityRegistered() {
        // First setup with ordinary ComponentActivity
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        Activity firstActivity = activityMock(userAnswers);
        setup(true, true, false, true, firstActivity);
        // Then navigate to a simple Activity
        Activity secondActivity = mock(Activity.class);
        Application.ActivityLifecycleCallbacks alc = bluetoothGuard.getActivityLifecycleCallbacks();
        alc.onActivityPaused(firstActivity);
        alc.onActivityCreated(secondActivity, null);
        alc.onActivityStarted(secondActivity);
        alc.onActivityResumed(secondActivity);
        alc.onActivityStopped(firstActivity);
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verifyNoInteractions(callback); // Not touched yet
        // Now, navigate back to the original activity and interact with the user
        userAnswers.add(true);
        alc.onActivityPaused(secondActivity);
        alc.onActivityStarted(firstActivity);
        alc.onActivityResumed(firstActivity);
        alc.onActivityStopped(secondActivity);
        alc.onActivityDestroyed(secondActivity);
        verify(callback).accept(true);
        assertEquals(0, userAnswers.size());
    }

    /* No Activity (in this app) is visible when the request to turn on the transceiver is made */

    @Test
    public void transceiverOffWithNoActivityRegistered() {
        // First setup with ordinary ComponentActivity
        LinkedList<Boolean> userAnswers = new LinkedList<>();
        Activity activity = activityMock(userAnswers);
        setup(true, true, true, false, activity);
        // Then loose visibility
        Application.ActivityLifecycleCallbacks alc = bluetoothGuard.getActivityLifecycleCallbacks();
        alc.onActivityPaused(activity);
        alc.onActivityStopped(activity);
        bluetoothGuard.bluetoothLEGuard(true, callback);
        verifyNoInteractions(callback); // Not touched yet
        // Now, navigate back to the original activity and interact with the user
        userAnswers.add(true);
        alc.onActivityStarted(activity);
        alc.onActivityResumed(activity);
        verify(callback).accept(true);
        assertEquals(0, userAnswers.size());
    }

}
