package dk.alexandra.phg.device_communication;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.lenient;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.mockConstruction;
import static org.mockito.Mockito.mockStatic;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import static dk.alexandra.phg.device_communication.api.Metric.MEASUREMENT_STATUS_NOT_USED;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.util.Log;

import com.pcha.btmanager.BtManager;
import com.pcha.btmanager.IntermediaryCallback;
import com.pcha.btmanager.PhdInformation;
import com.pcha.codephg.inter.CompoundEntry;
import com.pcha.codephg.inter.EnumEntry;
import com.pcha.codephg.inter.MderFloat;
import com.pcha.codephg.inter.MdsIntermediary;
import com.pcha.codephg.inter.MetricIntermediary;
import com.pcha.codephg.inter.NumericEntry;
import com.pcha.codephg.inter.PhgFields;
import com.pcha.codephg.inter.TimeStruct;

import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.MockedConstruction;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Supplier;

import dk.alexandra.phg.device_communication.api.DC2HostAPI;
import dk.alexandra.phg.device_communication.api.Metric;

@RunWith(MockitoJUnitRunner.class)
public class DeviceCommunicationServiceTest {

    private MockedStatic<Log> logMockedStatic;
    private MockedStatic<BtManager> btManagerMockedStatic;
    private MockedStatic<Looper> looperMockedStatic;

    private MockedConstruction<BtManager> btManagerMockedConstruction;

    @Mock
    private Context context;

    @Mock
    private BluetoothDevice device1, device2, device3, device4;

    @Mock
    private PhdInformation phdInfo1, phdInfo2, phdInfo3;

    @Captor
    private ArgumentCaptor<ArrayList<BluetoothDevice>> bluetoothDevicesCaptor;

    @Captor
    private ArgumentCaptor<PhgFields> phgFieldsCaptor;

    @Captor
    private ArgumentCaptor<IntermediaryCallback> intermediaryCallbackCaptor;

    @Captor
    private ArgumentCaptor<List<String>> whitelistCaptor;

    @Captor
    private ArgumentCaptor<Runnable> runnable;

    @Captor
    ArgumentCaptor<Collection<Metric>> metricsCaptor;

    private String BLUETOOTH_DEVICES_EXTRA;

    private BtManager btManager;

    private static boolean phdInfoFileLoaded = false;
    private static final Object phdInfoFileLoadedMonitor = new Object();

    private Set<PhdInformation> currentSubscriptions;

    private DeviceCommunicationService deviceCommunicationService;
    private DeviceCommunicationService spy;

    private final int startId = 42;

    private void setupStatic() throws Exception {
        MockitoAnnotations.openMocks(this);

        logMockedStatic = mockStatic(Log.class);

        btManagerMockedStatic = mockStatic(BtManager.class);
        btManagerMockedStatic.when(
                () -> BtManager.setIntermediaryCallback(intermediaryCallbackCaptor.capture()))
                .thenAnswer(invocation -> null);
        btManagerMockedStatic.when(() -> BtManager.setPhgFields(phgFieldsCaptor.capture()))
                .thenAnswer(invocation -> null);

        btManagerMockedConstruction = mockConstruction(BtManager.class,
                (mock, context) -> {
                    assertEquals(4, context.arguments().size());
                    btManager = mock;
                    synchronized (phdInfoFileLoadedMonitor) {
                        phdInfoFileLoaded = true;
                    }
                });

        Field btDevExtraField = DeviceCommunicationService.class.getDeclaredField("BLUETOOTH_DEVICES_EXTRA");
        btDevExtraField.setAccessible(true);
        BLUETOOTH_DEVICES_EXTRA = (String) btDevExtraField.get(null);
    }

    @SuppressWarnings("unchecked")
    private void setupWithServiceObject() throws Exception {
        setupStatic();
        looperMockedStatic = mockStatic(Looper.class);
        deviceCommunicationService = new DeviceCommunicationService();
        Field csField = DeviceCommunicationService.class.getDeclaredField("currentSubscriptions");
        csField.setAccessible(true);
        currentSubscriptions = (Set<PhdInformation>) csField.get(deviceCommunicationService);

    }

    private void invokeOnCreate() throws Exception {
        Method method = DeviceCommunicationService.class.getDeclaredMethod("onCreateImpl");
        method.setAccessible(true);
        method.invoke(deviceCommunicationService);
    }

    private void invokeOnDestroy() throws Exception {
        Method method = DeviceCommunicationService.class.getDeclaredMethod("onDestroyImpl");
        method.setAccessible(true);
        method.invoke(deviceCommunicationService);
    }

    @After
    public void tearDown() {
        if (looperMockedStatic != null) {
            looperMockedStatic.close();
            looperMockedStatic = null;
        }
        if (btManagerMockedConstruction != null) {
            btManagerMockedConstruction.close();
            btManagerMockedConstruction = null;
        }
        if (btManagerMockedStatic != null) {
            btManagerMockedStatic.close();
            btManagerMockedStatic = null;
        }
        if (logMockedStatic != null) {
            logMockedStatic.close();
            logMockedStatic = null;
        }
    }

    //
    // Static method tests
    //

    /* ensurePhdInfoFileIsLoaded */

    @Test
    public void ensurePhdInfoFileIsLoaded() throws Exception {
        setupStatic();
        synchronized (phdInfoFileLoadedMonitor) {
            DeviceCommunicationService.ensurePhdInfoFileIsLoaded(context);
        }
        assertTrue(phdInfoFileLoaded);
    }

    /* updateSubscriptions */

    @Test
    public void updateSubscriptionsEmptySet() throws Exception {
        setupStatic();
        try (MockedConstruction<Intent> intentMockedConstruction = mockConstruction(Intent.class)) {
            DeviceCommunicationService.updateSubscriptions(context, new HashSet<>());
            assertEquals(1, intentMockedConstruction.constructed().size());
            Intent i = intentMockedConstruction.constructed().get(0);
            verify(context).startService(i);
            verify(i).putParcelableArrayListExtra(eq(BLUETOOTH_DEVICES_EXTRA),
                    bluetoothDevicesCaptor.capture());
            assertEquals(0, bluetoothDevicesCaptor.getValue().size());
        }
    }

    @Test
    public void updateSubscriptionsThreeDevices() throws Exception {
        setupStatic();
        BluetoothDevice device1 = mock(BluetoothDevice.class);
        BluetoothDevice device2 = mock(BluetoothDevice.class);
        BluetoothDevice device3 = mock(BluetoothDevice.class);
        Set<PhdInformation> inputSet = new HashSet<>();
        PhdInformation phdInfo = mock(PhdInformation.class);
        when(phdInfo.getBluetoothDevice()).thenReturn(device1);
        inputSet.add(phdInfo);
        phdInfo = mock(PhdInformation.class);
        when(phdInfo.getBluetoothDevice()).thenReturn(device2);
        inputSet.add(phdInfo);
        phdInfo = mock(PhdInformation.class);
        when(phdInfo.getBluetoothDevice()).thenReturn(device3);
        inputSet.add(phdInfo);
        try (MockedConstruction<Intent> intentMockedConstruction = mockConstruction(Intent.class)) {
            DeviceCommunicationService.updateSubscriptions(context, inputSet);
            assertEquals(1, intentMockedConstruction.constructed().size());
            Intent i = intentMockedConstruction.constructed().get(0);
            verify(context).startService(i);
            verify(i).putParcelableArrayListExtra(eq(BLUETOOTH_DEVICES_EXTRA),
                    bluetoothDevicesCaptor.capture());
            ArrayList<BluetoothDevice> devices = bluetoothDevicesCaptor.getValue();
            assertEquals(3, devices.size());
            assertTrue(devices.contains(device1));
            assertTrue(devices.contains(device2));
            assertTrue(devices.contains(device3));
        }
    }

    /* enablePulseOxModalities */

    @Test
    public void enablePulseOxModalitiesNormal() throws Exception {
        setupStatic();
        DeviceCommunicationService.enablePulseOxModalities(true, false, false);
        btManagerMockedStatic.verify(() -> BtManager.setEnableInstantPulseOxDataFiltering(false));
        btManagerMockedStatic.verify(() -> BtManager.setEnableFastModalityFiltering(true));
        btManagerMockedStatic.verify(() -> BtManager.setEnableSlowModalityFiltering(true));
    }

    @Test
    public void enablePulseOxModalitiesFast() throws Exception {
        setupStatic();
        DeviceCommunicationService.enablePulseOxModalities(false, true, false);
        btManagerMockedStatic.verify(() -> BtManager.setEnableInstantPulseOxDataFiltering(true));
        btManagerMockedStatic.verify(() -> BtManager.setEnableFastModalityFiltering(false));
        btManagerMockedStatic.verify(() -> BtManager.setEnableSlowModalityFiltering(true));
    }

    @Test
    public void enablePulseOxModalitiesSlow() throws Exception {
        setupStatic();
        DeviceCommunicationService.enablePulseOxModalities(false, false, true);
        btManagerMockedStatic.verify(() -> BtManager.setEnableInstantPulseOxDataFiltering(true));
        btManagerMockedStatic.verify(() -> BtManager.setEnableFastModalityFiltering(true));
        btManagerMockedStatic.verify(() -> BtManager.setEnableSlowModalityFiltering(false));
    }



    //
    // Service object method tests
    //

    // This will prepare the whitelist with device1+2+3 from above
    private void setupWhitelist() {
        lenient().when(device1.getAddress()).thenReturn("1");
        lenient().when(device2.getAddress()).thenReturn("2");
        lenient().when(device3.getAddress()).thenReturn("3");
        lenient().when(device4.getAddress()).thenReturn("4");
        lenient().when(phdInfo1.getBluetoothDevice()).thenReturn(device1);
        lenient().when(phdInfo2.getBluetoothDevice()).thenReturn(device2);
        lenient().when(phdInfo3.getBluetoothDevice()).thenReturn(device3);
        ArrayList<PhdInformation> phdInfoList = new ArrayList<>(3);
        phdInfoList.add(phdInfo1);
        phdInfoList.add(phdInfo2);
        phdInfoList.add(phdInfo3);
        btManagerMockedStatic.when(BtManager::getKnownDevices).thenReturn(phdInfoList);
    }

    private Intent buildIntent(ArrayList<Parcelable> devices) {
        Intent intent = mock(Intent.class);
        when(intent.hasExtra(BLUETOOTH_DEVICES_EXTRA)).thenReturn(true);
        when(intent.getParcelableArrayListExtra(BLUETOOTH_DEVICES_EXTRA)).thenReturn(devices);
        return intent;
    }

    private int invokeOnStartCommand(Intent intent) {
        spy = Mockito.spy(deviceCommunicationService);
        lenient().doNothing().when(spy).stopSelf(anyInt());
        return spy.onStartCommand(intent, 0, startId);
    }

    private int timesChecked = 0;
    private void checkSubscriptions(String[] expected, ArrayList<Parcelable> devices) {
        int serviceState = invokeOnStartCommand(buildIntent(devices));
        assertEquals(expected.length, currentSubscriptions.size());
        btManagerMockedStatic.verify(() -> BtManager.setWhiteList(whitelistCaptor.capture()),
                times(++timesChecked));
        List<String> wlList = whitelistCaptor.getValue();
        assertEquals(expected.length, wlList.size());
        Set<String> wlSet = new HashSet<>(wlList);
        assertEquals(expected.length, wlSet.size());
        for (String element : expected) {
            assertTrue(wlSet.contains(element));
        }
        if (expected.length == 0) {
            verify(spy).stopSelf(startId);
            verify(btManager).close();
            assertEquals(Service.START_NOT_STICKY, serviceState);
        } else {
            verify(spy, never()).stopSelf(startId);
            assertEquals(Service.START_REDELIVER_INTENT, serviceState);
        }
    }

    private void checkNewSubscriptions(int[] expected) {
        for (int element : expected) {
            switch (element) {
                case 1:
                    verify(btManager).connectToDeviceFromDiscovery(device1, phdInfo1);
                    break;
                case 2:
                    verify(btManager).connectToDeviceFromDiscovery(device2, phdInfo2);
                    break;
                case 3:
                    verify(btManager).connectToDeviceFromDiscovery(device3, phdInfo3);
                    break;
                default:
            }
        }
        verifyNoMoreInteractions(btManager);
    }


    /* Constructor functional */

    @Test
    public void canConstructObject() throws Exception {
        setupWithServiceObject();
        assertNotNull(deviceCommunicationService);
    }

    /* Initialization / de-initialization functional */

    @Test
    public void canInitializeAndDeInitialize() throws Exception {
        setupWithServiceObject();
        invokeOnCreate();
        assertNotNull(phgFieldsCaptor.getValue());
        assertNotNull(intermediaryCallbackCaptor.getValue());
        invokeOnDestroy();
        assertNull(phgFieldsCaptor.getValue());
        assertNull(intermediaryCallbackCaptor.getValue());
    }

    /* Start service with no subscriptions */

    private void startServiceWithNoSubscription(Intent intent) {
        int serviceState = invokeOnStartCommand(intent);
        verify(spy).stopSelf(startId);
        assertEquals(Service.START_NOT_STICKY, serviceState);
        assertEquals(0, currentSubscriptions.size());
    }

    @Test
    public void startServiceWithNullIntent() throws Exception {
        setupWithServiceObject();
        startServiceWithNoSubscription(null);
    }

    @Test
    public void startServiceWithNoExtra() throws Exception {
        setupWithServiceObject();
        Intent intent = mock(Intent.class);
        ArgumentCaptor<String> extraNameCaptor = ArgumentCaptor.forClass(String.class);
        when(intent.hasExtra(extraNameCaptor.capture())).thenReturn(false);
        startServiceWithNoSubscription(intent);
        assertEquals(BLUETOOTH_DEVICES_EXTRA, extraNameCaptor.getValue());
    }

    @Test
    public void startServiceWithEmptyList() throws Exception {
        setupWithServiceObject();
        startServiceWithNoSubscription(buildIntent(new ArrayList<>(0)));
    }

    @Test
    public void startServiceWithThreeDevicesAndNoWhitelist() throws Exception {
        setupWithServiceObject();

        ArrayList<Parcelable> devices = new ArrayList<>(3);
        devices.add(device1);
        devices.add(device2);
        devices.add(device3);

        startServiceWithNoSubscription(buildIntent(devices));
    }

    //
    // Subscriptions
    //

    /* Start service with subscriptions partially overlapping with the whitelist */

    @Test
    public void startServiceWithThreeDevicesAndOverlappingWhitelist() throws Exception {
        setupWithServiceObject();

        setupWhitelist();

        ArrayList<Parcelable> devices = new ArrayList<>(3);
        devices.add(device2);
        devices.add(device3);
        devices.add(device4);

        // Whitelist is device 1+2+3 and we ask to subscribe to 2+3+4, so we expect
        // to get only device 2+3 in the end (the set intersection)!

        checkSubscriptions(new String[]{"2", "3"}, devices);
        checkNewSubscriptions(new int[] {2, 3});
    }



    /* Series of additions and removal of subscriptions */

    @Test
    public void subscriptionAdditionAndRemoval() throws Exception {
        setupWithServiceObject();

        setupWhitelist();

        // Subscribe 2 + 3 -> result {2, 3}

        ArrayList<Parcelable> devices = new ArrayList<>(2);
        devices.add(device2);
        devices.add(device3);

        checkSubscriptions(new String[]{"2", "3"}, devices);
        checkNewSubscriptions(new int[]{2, 3});

        // Unsubscribe 2 -> result {3}

        devices = new ArrayList<>(1);
        devices.add(device3);

        checkSubscriptions(new String[]{"3"}, devices);
        checkNewSubscriptions(new int[0]);
        btManagerMockedStatic.verify(() -> BtManager.removeDeviceFromSession(device2));

        // Subscribe 1 -> result {1, 3}

        devices = new ArrayList<>(2);
        devices.add(device1);
        devices.add(device3);

        checkSubscriptions(new String[]{"1", "3"}, devices);
        checkNewSubscriptions(new int[]{1});

        // Unsubscribe 1 -> result {3}

        devices = new ArrayList<>(1);
        devices.add(device3);

        checkSubscriptions(new String[]{"3"}, devices);
        checkNewSubscriptions(new int[0]);
        btManagerMockedStatic.verify(() -> BtManager.removeDeviceFromSession(device1));

        // Unsubscribe 3 -> result {}

        devices = new ArrayList<>(0);

        checkSubscriptions(new String[0], devices);
        checkNewSubscriptions(new int[0]);
        btManagerMockedStatic.verify(() -> BtManager.removeDeviceFromSession(device3));

        // Subscribe 1 -> result {1}

        devices = new ArrayList<>(1);
        devices.add(device1);

        checkSubscriptions(new String[]{"1"}, devices);
        checkNewSubscriptions(new int[]{1});
    }

    //
    // Metrics received
    //

    private void testMetricContent(Metric metric, int[] supplementalTypeCodes, int measurementStatus,
                                   String timeOfMeasurementReception, String agentTimestamp,
                                   String subclass) {
        if (supplementalTypeCodes == null) {
            assertNull(metric.getSupplementalTypeCodes());
        } else {
            int[] stc = metric.getSupplementalTypeCodes();
            assertTrue(stc != null && stc.length == supplementalTypeCodes.length);
            for (int i=0; i<stc.length; i++) {
                assertEquals(supplementalTypeCodes[i], stc[i]);
            }
        }
        assertEquals(measurementStatus, metric.getMeasurementStatus());
        assertEquals(timeOfMeasurementReception, metric.getTimeOfMeasurementReception());
        assertEquals(agentTimestamp, metric.getAgentTimeStamp());
        assertEquals(subclass, metric.getSubclass());
    }

    /* Single numeric */

    @Test
    public void metricSingleNumeric() throws Exception {
        try (MockedConstruction<Handler> handlerMockedConstruction = mockConstruction(Handler.class)) {
            setupWithServiceObject();
            assertEquals(1, handlerMockedConstruction.constructed().size());
            when(handlerMockedConstruction.constructed().get(0)
                    .post(runnable.capture())).thenReturn(true);
            invokeOnCreate();

            DC2HostAPI dc2hostAPI = mock(DC2HostAPI.class);
            Field dc2hostApiField = DeviceCommunicationService.class.getDeclaredField("dc2hostAPI");
            dc2hostApiField.setAccessible(true);
            dc2hostApiField.set(deviceCommunicationService,(Supplier<DC2HostAPI>) () -> dc2hostAPI);

            IntermediaryCallback ic = intermediaryCallbackCaptor.getValue();

            // MDS:
            //   SystemId:                       "DEVICE_ID"
            // Metric:
            //   MeasurementTypeCode:            0xffffffff
            //   SupplementalTypesNomenclatures: Empty list
            //   MeasurementStatus:              Absent
            //   TimeOfMeasurementReception:     "timeStamp"
            //   AgentTimeStamp:                 Absent
            //   Numeric:
            //     Value:                        "123.45"
            //     Unit:                         0x12345678

            MetricIntermediary intermediaryMetric = mock(MetricIntermediary.class);

            MdsIntermediary mds = mock(MdsIntermediary.class);

            when(mds.getSystemId()).thenReturn("DEVICE_ID");

            when(intermediaryMetric.getMeasurementTypeCode()).thenReturn(0xffffffffL);

            when(intermediaryMetric.getSupplementalTypesNomenclatures()).thenReturn(new LinkedList<>());

            when(intermediaryMetric.isHasMeasurementStatus()).thenReturn(false);

            TimeStruct ts = mock(TimeStruct.class);
            when(intermediaryMetric.getTimeOfMeasurementReception()).thenReturn(ts);
            when(ts.getFhirString()).thenReturn("timeStamp");

            when(intermediaryMetric.getAgentTimeStamp()).thenReturn(null);

            when(intermediaryMetric.isNumeric()).thenReturn(true);

            NumericEntry numericEntry = mock(NumericEntry.class);

            MderFloat mderFloat = mock(MderFloat.class);
            when(mderFloat.toPcd01String()).thenReturn("123.45");

            when(numericEntry.getValue()).thenReturn(mderFloat);
            when(numericEntry.getUnitCode32()).thenReturn(0x12345678L);

            when(intermediaryMetric.getNumericEntry()).thenReturn(numericEntry);

            List<MetricIntermediary> metricList = new LinkedList<>();
            metricList.add(intermediaryMetric);

            ic.onReceiveMetricIntermediaries(metricList, mds);
            runnable.getValue().run();

            // VERIFY

            // MDS:
            //   SystemId:                       "DEVICE_ID"
            // Metric:
            //   MeasurementTypeCode:            0xffffffff
            //   SupplementalTypesNomenclatures: Empty list
            //   MeasurementStatus:              Absent
            //   TimeOfMeasurementReception:     "timeStamp"
            //   AgentTimeStamp:                 Absent
            //   Numeric:
            //     Value:                        "123.45"
            //     Unit:                         0x12345678

            verify(dc2hostAPI).metricsReceived(eq("DEVICE_ID"),  metricsCaptor.capture());

            assertEquals(1, metricsCaptor.getValue().size());
            assertTrue(metricsCaptor.getValue().stream().findFirst().isPresent());
            Metric apiMetric = metricsCaptor.getValue().stream().findFirst().get();

            assertEquals(0xffffffff, apiMetric.getMeasurementTypeCode());
            testMetricContent(apiMetric, null, MEASUREMENT_STATUS_NOT_USED,
                    "timeStamp", null, "Numeric");

            assertEquals("123.45", ((Metric.Numeric)apiMetric).getValue());
            assertEquals(0x12345678, ((Metric.Numeric)apiMetric).getUnit());
        }
    }

    /* Compound numeric and bits enum */
    @Test
    public void metricDualCompoundAndBitsEnum() throws Exception {
        try (MockedConstruction<Handler> handlerMockedConstruction = mockConstruction(Handler.class)) {
            setupWithServiceObject();
            assertEquals(1, handlerMockedConstruction.constructed().size());
            when(handlerMockedConstruction.constructed().get(0)
                    .post(runnable.capture())).thenReturn(true);
            invokeOnCreate();

            DC2HostAPI dc2hostAPI = mock(DC2HostAPI.class);
            Field dc2hostApiField = DeviceCommunicationService.class.getDeclaredField("dc2hostAPI");
            dc2hostApiField.setAccessible(true);
            dc2hostApiField.set(deviceCommunicationService,(Supplier<DC2HostAPI>) () -> dc2hostAPI);

            IntermediaryCallback ic = intermediaryCallbackCaptor.getValue();

            // MDS:
            //   SystemId:                       "DEVICE_ID"
            // Metric[0]:
            //   MeasurementTypeCode:            0xfffffffe
            //   SupplementalTypesNomenclatures: {0x00000001, 0x00020000}
            //   MeasurementStatus:              0xA55A
            //   TimeOfMeasurementReception:     "timeOfMeasurement"
            //   AgentTimeStamp:                 "timeFromAgent"
            //   CompoundNumeric:
            //     component[0]:
            //       MeasurementTypeCode:        0xffff0000
            //       Value:                      "12.345"
            //       Unit:                       0x00000000
            //     component[1]:
            //       MeasurementTypeCode:        0xffff0001
            //       Value:                      "12345"
            //       Unit:                       0x00010001

            MetricIntermediary intermediaryMetric = mock(MetricIntermediary.class);

            MdsIntermediary mds = mock(MdsIntermediary.class);

            when(mds.getSystemId()).thenReturn("DEVICE_ID");

            when(intermediaryMetric.getMeasurementTypeCode()).thenReturn(0xfffffffeL);

            LinkedList<Long> supplTypes = new LinkedList<>();
            supplTypes.add(0x00000001L);
            supplTypes.add(0x00020000L);
            when(intermediaryMetric.getSupplementalTypesNomenclatures()).thenReturn(supplTypes);

            when(intermediaryMetric.isHasMeasurementStatus()).thenReturn(true);
            when(intermediaryMetric.getMeasurementStatus()).thenReturn(0xa55a);

            TimeStruct ts = mock(TimeStruct.class);
            when(intermediaryMetric.getTimeOfMeasurementReception()).thenReturn(ts);
            when(ts.getFhirString()).thenReturn("timeOfMeasurement");

            ts = mock(TimeStruct.class);
            when(intermediaryMetric.getAgentTimeStamp()).thenReturn(ts);
            when(ts.getFhirString()).thenReturn("timeFromAgent");

            when(intermediaryMetric.isCompoundNumeric()).thenReturn(true);

            LinkedList<CompoundEntry> compoundEntries = new LinkedList<>();
            CompoundEntry compoundEntry = mock(CompoundEntry.class);
            MderFloat mderFloat = mock(MderFloat.class);
            when(mderFloat.toPcd01String()).thenReturn("12.345");
            when(compoundEntry.getType()).thenReturn(0xffff0000L);
            when(compoundEntry.getValue()).thenReturn(mderFloat);
            when(compoundEntry.getUnitCode32()).thenReturn(0x00000000L);
            compoundEntries.add(compoundEntry);
            compoundEntry = mock(CompoundEntry.class);
            mderFloat = mock(MderFloat.class);
            when(mderFloat.toPcd01String()).thenReturn("12345");
            when(compoundEntry.getType()).thenReturn(0xffff0001L);
            when(compoundEntry.getValue()).thenReturn(mderFloat);
            when(compoundEntry.getUnitCode32()).thenReturn(0x00010001L);
            compoundEntries.add(compoundEntry);
            when(intermediaryMetric.getCompoundEntries()).thenReturn(compoundEntries);

            List<MetricIntermediary> metricList = new LinkedList<>();
            metricList.add(intermediaryMetric);

            // Metric[1]:
            //   MeasurementTypeCode:            0x80008000
            //   SupplementalTypesNomenclatures: Empty list
            //   MeasurementStatus:              Absent
            //   TimeOfMeasurementReception:     "timeOfMeasurement"
            //   AgentTimeStamp:                 Absent
            //   EnumBits:
            //     value:                        0x1234
            //     is32bits:                     false

            intermediaryMetric = mock(MetricIntermediary.class);

            when(intermediaryMetric.getMeasurementTypeCode()).thenReturn(0x80008000L);

            when(intermediaryMetric.getSupplementalTypesNomenclatures()).thenReturn(new LinkedList<>());

            when(intermediaryMetric.isHasMeasurementStatus()).thenReturn(false);

            ts = mock(TimeStruct.class);
            when(intermediaryMetric.getTimeOfMeasurementReception()).thenReturn(ts);
            when(ts.getFhirString()).thenReturn("timeOfMeasurement");

            when(intermediaryMetric.getAgentTimeStamp()).thenReturn(null);

            when(intermediaryMetric.isEnumeration()).thenReturn(true);

            EnumEntry enumEntry = mock(EnumEntry.class);
            when(enumEntry.isBits16Enum()).thenReturn(true);
            when(enumEntry.getBitsEnum()).thenReturn(0x1234);

            when(intermediaryMetric.getEnumEntry()).thenReturn(enumEntry);
            metricList.add(intermediaryMetric);

            // Metric[2]:
            //   MeasurementTypeCode:            0x90009000
            //   SupplementalTypesNomenclatures: Empty list
            //   MeasurementStatus:              Absent
            //   TimeOfMeasurementReception:     "timeOfMeasurement"
            //   AgentTimeStamp:                 Absent
            //   EnumBits:
            //     value:                        0x1234fffe
            //     is32bits:                     true

            intermediaryMetric = mock(MetricIntermediary.class);

            when(intermediaryMetric.getMeasurementTypeCode()).thenReturn(0x90009000L);

            when(intermediaryMetric.getSupplementalTypesNomenclatures()).thenReturn(new LinkedList<>());

            when(intermediaryMetric.isHasMeasurementStatus()).thenReturn(false);

            ts = mock(TimeStruct.class);
            when(intermediaryMetric.getTimeOfMeasurementReception()).thenReturn(ts);
            when(ts.getFhirString()).thenReturn("timeOfMeasurement");

            when(intermediaryMetric.getAgentTimeStamp()).thenReturn(null);

            when(intermediaryMetric.isEnumeration()).thenReturn(true);

            enumEntry = mock(EnumEntry.class);
            when(enumEntry.isBits32Enum()).thenReturn(true);
            when(enumEntry.getBitsEnum()).thenReturn(0x1234FFFE);

            when(intermediaryMetric.getEnumEntry()).thenReturn(enumEntry);
            metricList.add(intermediaryMetric);


            ic.onReceiveMetricIntermediaries(metricList, mds);
            runnable.getValue().run();


            // VERIFY

            // MDS:
            //   SystemId:                       "DEVICE_ID"

            verify(dc2hostAPI).metricsReceived(eq("DEVICE_ID"),  metricsCaptor.capture());

            assertEquals(3, metricsCaptor.getValue().size());
            boolean[] verifiedMetric = new boolean[3];
            for (Metric apiMetric : metricsCaptor.getValue()) {
                switch (apiMetric.getMeasurementTypeCode()) {

                        // Metric[0]:
                        //   MeasurementTypeCode:            0xfffffffe
                        //   SupplementalTypesNomenclatures: {0x00000001, 0x00020000}
                        //   MeasurementStatus:              0xA55A
                        //   TimeOfMeasurementReception:     "timeOfMeasurement"
                        //   AgentTimeStamp:                 "timeFromAgent"
                        //   CompoundNumeric:
                        //     component[0]:
                        //       MeasurementTypeCode:        0xffff0000
                        //       Value:                      "12.345"
                        //       Unit:                       0x00000000
                        //     component[1]:
                        //       MeasurementTypeCode:        0xffff0001
                        //       Value:                      "12345"
                        //       Unit:                       0x00010001

                    case 0xfffffffe:

                        testMetricContent(apiMetric, new int[] {0x00000001, 0x00020000},
                                0xa55a, "timeOfMeasurement",
                                "timeFromAgent", "CompoundNumeric");
                        Collection<Metric.Component> components = ((Metric.CompoundNumeric)apiMetric).getComponents();
                        assertEquals(2, components.size());
                        for (Metric.Component c : components) {
                            if (c.getMeasurementTypeCode() == 0xffff0000) {
                                assertEquals("12.345", c.getValue());
                                assertEquals(0x00000000, c.getUnit());
                            } else {
                                assertEquals(0xffff0001, c.getMeasurementTypeCode());
                                assertEquals("12345", c.getValue());
                                assertEquals(0x00010001, c.getUnit());
                            }
                        }
                        verifiedMetric[0] = true;
                        break;

                        // Metric[1]:
                        //   MeasurementTypeCode:            0x80008000
                        //   SupplementalTypesNomenclatures: Empty list
                        //   MeasurementStatus:              Absent
                        //   TimeOfMeasurementReception:     "timeOfMeasurement"
                        //   AgentTimeStamp:                 Absent
                        //   EnumBits:
                        //     value:                        0x1234
                        //     is32bits:                     false

                    case 0x80008000:

                        testMetricContent(apiMetric, null, MEASUREMENT_STATUS_NOT_USED,
                                "timeOfMeasurement", null, "EnumBits");
                        assertEquals(0x1234, ((Metric.EnumBits)apiMetric).getValue());
                        assertFalse(((Metric.EnumBits)apiMetric).getIs32bits());
                        verifiedMetric[1] = true;
                        break;

                        // Metric[2]:
                        //   MeasurementTypeCode:            0x90009000
                        //   SupplementalTypesNomenclatures: Empty list
                        //   MeasurementStatus:              Absent
                        //   TimeOfMeasurementReception:     "timeOfMeasurement"
                        //   AgentTimeStamp:                 Absent
                        //   EnumBits:
                        //     value:                        0x1234fffe
                        //     is32bits:                     true

                    case 0x90009000:

                        testMetricContent(apiMetric, null, MEASUREMENT_STATUS_NOT_USED,
                                "timeOfMeasurement", null, "EnumBits");
                        assertEquals(0x1234fffe, ((Metric.EnumBits)apiMetric).getValue());
                        assertTrue(((Metric.EnumBits)apiMetric).getIs32bits());
                        verifiedMetric[2] = true;
                        break;

                    default:
                }
            }
            assertTrue(verifiedMetric[0]);
            assertTrue(verifiedMetric[1]);
            assertTrue(verifiedMetric[2]);
        }
    }
}