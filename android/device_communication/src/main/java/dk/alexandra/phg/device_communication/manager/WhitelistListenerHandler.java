package dk.alexandra.phg.device_communication.manager;

import androidx.annotation.NonNull;

import com.pcha.btmanager.PhdInformation;

import java.lang.ref.WeakReference;
import java.util.HashSet;
import java.util.Iterator;
import java.util.function.Consumer;

public class WhitelistListenerHandler {

    private final HashSet<WeakReference<WhitelistListener>> whitelistListeners = new HashSet<>();


    void addWhitelistListener(@NonNull WhitelistListener listener) {
        whitelistListeners.add(new WeakReference<>(listener));
    }

    void removeWhitelistListener(@NonNull WhitelistListener listener) {
        Iterator<WeakReference<WhitelistListener>> iterator = whitelistListeners.iterator();
        while (iterator.hasNext()) {
            WhitelistListener l = iterator.next().get();
            if (l == null || l == listener) iterator.remove();
        }
    }


    // Signal all WhitelistListeners

    void signalPhdAdded(@NonNull PhdInformation phd, boolean currentlyConnected) {
        signalWhitelistListeners(wll -> wll.phdAdded(phd, currentlyConnected));
    }

    void signalPhdDeleted(@NonNull PhdInformation phd) {
        signalWhitelistListeners(wll -> wll.phdDeleted(phd));
    }

    void signalPhdUpdated(@NonNull PhdInformation phd, boolean currentlyConnected) {
        signalWhitelistListeners(wll -> wll.phdUpdated(phd, currentlyConnected));
    }

    private void signalWhitelistListeners(@NonNull Consumer<WhitelistListener> wll) {
        Iterator<WeakReference<WhitelistListener>> iterator = whitelistListeners.iterator();
        while (iterator.hasNext()) {
            WhitelistListener l = iterator.next().get();
            if (l == null) {
                iterator.remove();
            } else {
                wll.accept(l);
            }
        }
    }
}
