package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Device {

    private final @NonNull String persistentDeviceId;
    private @NonNull String name;
    private @NonNull String manufacturer;
    private @NonNull String modelNumber;
    private @NonNull String serialNumber;

    private @NonNull int[] specializations;

    public Device(@NonNull String persistentDeviceId, @NonNull String name,
                  @NonNull String manufacturer, @NonNull String modelNumber,
                  @NonNull String serialNumber, @NonNull int[] specializations) {
        this.persistentDeviceId = persistentDeviceId;
        this.name = name;
        this.manufacturer = manufacturer;
        this.modelNumber = modelNumber;
        this.serialNumber = serialNumber;
        this.specializations = specializations;
    }


    JSONObject toJson() throws JSONException {
        JSONObject device = new JSONObject();
        device.put("persistentDeviceId", persistentDeviceId);
        device.put("name", name);
        device.put("manufacturer", manufacturer);
        device.put("modelNumber", modelNumber);
        device.put("serialNumber", serialNumber);
        JSONArray specs = new JSONArray();
        // Treat specializations as unsigned integers (cast to long)
        for (int s : specializations) specs.put(Integer.toUnsignedLong(s));
        device.put("specializations", specs);
        return device;
    }

    public static Device fromJson(@NonNull JSONObject deviceJson) throws JSONException {
        String persistentDeviceId = deviceJson.getString("persistentDeviceId");
        String name = deviceJson.optString("name");
        String manufacturer = deviceJson.optString("manufacturer");
        String modelNumber = deviceJson.optString("modelNumber");
        String serialNumber = deviceJson.optString("serialNumber");
        JSONArray specializationsJson = deviceJson.optJSONArray("specializations");
        int[] specializations;
        if (specializationsJson != null) {
            specializations = new int[specializationsJson.length()];
            for (int i=0; i<specializationsJson.length(); i++) {
                specializations[i] = (int)specializationsJson.getLong(i);
            }
        } else {
            specializations = new int[0];
        }
        return new Device(persistentDeviceId, name, manufacturer, modelNumber,
                serialNumber, specializations);
    }
}
