package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;

public abstract class Metric {

    public static final int MEASUREMENT_STATUS_NOT_USED = -1;

    private final @NonNull String metricId;
    private int measurementTypeCode;
    private @Nullable int[] supplementalTypeCodes;
    private @Nullable String agentTimeStamp;
    private @NonNull String timeOfMeasurementReception;
    private @NonNull String subclass;
    private int measurementStatus;


    protected Metric(@NonNull String metricId,
                  int measurementTypeCode,
                  @Nullable int[] supplementalTypeCodes,
                  @Nullable String agentTimestamp,
                  @NonNull String timeOfMeasurementReception,
                  int measurementStatus,
                  @NonNull String subclass) {
        this.metricId = metricId;
        this.measurementTypeCode = measurementTypeCode;
        this.supplementalTypeCodes = supplementalTypeCodes;
        this.agentTimeStamp = agentTimestamp;
        this.timeOfMeasurementReception = timeOfMeasurementReception;
        this.measurementStatus = measurementStatus;
        this.subclass = subclass;
    }

    JSONObject toJson() throws JSONException {
        JSONObject metric = new JSONObject();
        metric.put("metricId", metricId);
        metric.put("measurementTypeCode", Integer.toUnsignedLong(measurementTypeCode));
        if (agentTimeStamp != null) metric.put("agentTimeStamp", agentTimeStamp);
        metric.put("timeOfMeasurementReception", timeOfMeasurementReception);
        metric.put("subclass", subclass);
        if (supplementalTypeCodes != null && supplementalTypeCodes.length > 0) {
            JSONArray stc = new JSONArray();
            for (int supplementalTypeCode : supplementalTypeCodes) {
                stc.put(Integer.toUnsignedLong(supplementalTypeCode));
            }
            metric.put("supplementalTypeCodes", stc);
        }
        if (measurementStatus >= 0 && measurementStatus <= 0xFFFF) {
            // This value is optional. Use MEASUREMENT_STATUS_NOT_USED to skip
            metric.put("measurementStatus", measurementStatus);
        }
        return metric;
    }

    public @NonNull String getMetricId() { return metricId; }
    public int getMeasurementTypeCode() { return measurementTypeCode; }
    public @Nullable int[] getSupplementalTypeCodes() { return supplementalTypeCodes; }
    public @Nullable String getAgentTimeStamp() { return agentTimeStamp; }
    public @NonNull String getTimeOfMeasurementReception() { return timeOfMeasurementReception; }
    public @NonNull String getSubclass() { return subclass; }
    public int getMeasurementStatus() { return measurementStatus; }


    public static class Numeric extends Metric {

        private @NonNull String value;
        private int unit;

        public Numeric(@NonNull String metricId,
                       int measurementTypeCode,
                       @Nullable int[] supplementalTypeCodes,
                       @Nullable String agentTimestamp,
                       @NonNull String timeOfMeasurementReception,
                       int measurementStatus,
                       @NonNull String value, int unit) {
            super(metricId, measurementTypeCode, supplementalTypeCodes, agentTimestamp,
                    timeOfMeasurementReception, measurementStatus, "Numeric");
            this.value = value;
            this.unit = unit;
        }

        @Override
        JSONObject toJson() throws JSONException {
            JSONObject numeric = super.toJson();
            numeric.put("value", value);
            numeric.put("unitCode", Integer.toUnsignedLong(unit));
            return numeric;
        }

        public @NonNull String getValue() { return value; }
        public int getUnit() { return unit; }
    }


    public static class Component {
        private int measurementTypeCode;
        private @NonNull String value;
        private int unit;

        public Component(int measurementTypeCode, @NonNull String value, int unit) {
            this.measurementTypeCode = measurementTypeCode;
            this.value = value;
            this.unit = unit;
        }

        public int getMeasurementTypeCode() { return measurementTypeCode; }
        public @NonNull String getValue() { return value; }
        public int getUnit() { return unit; }
    }

    public static class CompoundNumeric extends Metric {

        private @NonNull Collection<Component> components;

        public CompoundNumeric(@NonNull String metricId,
                               int measurementTypeCode,
                               @Nullable int[] supplementalTypeCodes,
                               @Nullable String agentTimestamp,
                               @NonNull String timeOfMeasurementReception,
                               int measurementStatus,
                               @NonNull Collection<Component> components) {
            super(metricId, measurementTypeCode, supplementalTypeCodes, agentTimestamp,
                    timeOfMeasurementReception, measurementStatus, "CompoundNumeric");
            this.components = components;
        }

        @Override
        JSONObject toJson() throws JSONException {
            JSONObject compoundNumeric = super.toJson();
            JSONArray componentArray = new JSONArray();
            for (Component comp : components) {
                JSONObject compJson = new JSONObject();
                compJson.put("measurementTypeCode",
                        Integer.toUnsignedLong(comp.measurementTypeCode));
                compJson.put("value", comp.value);
                compJson.put("unitCode", Integer.toUnsignedLong(comp.unit));
                componentArray.put(compJson);
            }
            compoundNumeric.put("components", componentArray);
            return compoundNumeric;
        }

        public @NonNull Collection<Component> getComponents() { return components; }
    }

    public static class EnumBits extends Metric {
        private int value;
        private boolean is32bits;

        public EnumBits(@NonNull String metricId,
                        int measurementTypeCode,
                        @Nullable int[] supplementalTypeCodes,
                        @Nullable String agentTimestamp,
                        @NonNull String timeOfMeasurementReception,
                        int measurementStatus,
                        int value, boolean is32bits) {
            super(metricId, measurementTypeCode, supplementalTypeCodes, agentTimestamp,
                    timeOfMeasurementReception, measurementStatus, "EnumBits");
            this.value = value;
            this.is32bits = is32bits;
        }

        @Override
        JSONObject toJson() throws JSONException {
            JSONObject enumBits = super.toJson();
            enumBits.put("value", is32bits ? Integer.toUnsignedLong(value) : (value & 0xFFFF));
            enumBits.put("is32Bits", is32bits);
            return enumBits;
        }

        public int getValue() { return value; }
        public boolean getIs32bits() { return is32bits; }
    }
}
