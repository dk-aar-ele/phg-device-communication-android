package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;

/**
 * Handles the communication in both directions between host and device communication library.
 * <p>
 * Raw strings with messages from the host is received using the receiveFromHost() method and
 * dispatched to an implementation of the Host2DCAPI interface provided to the constructor.
 * <p>
 * Invocations of methods on a DC2HostAPI proxy object will be serialized and sent to the host
 * using the dc2hostHandler string consumer provided to the constructor.
 */
public class DCMessageDispatcher {

    private static class CBHandler<T> {
        @NonNull final Callback<T> callback;
        @NonNull final Function<JSONObject, T> parser;
        CBHandler(@NonNull Callback<T> callback, @NonNull Function<JSONObject, T> parser) {
            this.callback = callback;
            this.parser = parser;
        }
        void respond(DCMessage.Response response) {
            if (response instanceof DCMessage.ErrorResponse) {
                DCMessage.ErrorResponse exception = (DCMessage.ErrorResponse) response;
                callback.error(exception.getErrorCode(), exception.getErrorMessage());
            } else {
                callback.success(parser.apply(response.getArgs()));
            }
        }
    }
    private final HashMap<String, CBHandler<?>> callbackMap = new HashMap<>();
    private final Consumer<String> dc2hostHandler;

    public DCMessageDispatcher(@NonNull Consumer<String> dc2hostHandler) {
        this.dc2hostHandler = dc2hostHandler;
    }

    /**
     * Receive and dispatch a message from the host.
     * <p>
     * The message will be unpacked and forwarded to the host2dcHandler
     * @param host2dcMessage The raw message received from the host
     * @param host2dcHandler The handler of the parsed message
     * @throws IllegalArgumentException If the message format/contents is illegal or an event
     *                       throws an unchecked exception. If an exception is caused by the
     *                       execution of a request, no exception is thrown here - instead it
     *                       is returned as a response to the host.
     */
    public void receiveFromHost(@NonNull String host2dcMessage, @NonNull Host2DCAPI host2dcHandler) {
        try {
            DCMessage msg = DCMessage.parse(host2dcMessage);
            if (msg instanceof DCMessage.Request) {
                dispatchRequestFromHost((DCMessage.Request) msg, host2dcHandler);
            } else if (msg instanceof DCMessage.Response) {
                DCMessage.Response response = (DCMessage.Response) msg;
                Objects.requireNonNull(callbackMap.remove(response.getRequestId()),
                        "No matching callback for requestId").respond(response);
            } else { // msg instanceof DCMessage.Event
                dispatchEventFromHost((DCMessage.Event) msg, host2dcHandler);
            }
        } catch (JSONException e) {
            // Convert to IllegalArgumentException. The caller will have to deal with it.
            throw new IllegalArgumentException(e);
        }
    }

    private void sendMessageToHost(@NonNull DCMessage message) throws JSONException {
        dc2hostHandler.accept(message.serialize());
    }

    @FunctionalInterface
    private interface ResponseEncoder<T> {
        @Nullable JSONObject encode(@Nullable  T what) throws JSONException;
    }

    @NonNull private <T> Callback<T> makeCallback(@NonNull DCMessage.Request request,
                                                  @NonNull ResponseEncoder<T> responseEncoder) {
        return new Callback<>() {
            @Override
            public void success(@Nullable T responseArgs) {
                try {
                    sendMessageToHost(new DCMessage.Response(request.getName(), request.getRequestId(),
                            responseEncoder.encode(responseArgs)));
                } catch (JSONException e) {
                    // Convert to IllegalArgumentException. The caller will have to deal with it.
                    throw new IllegalArgumentException(e);
                }
            }

            @Override
            public void error(int errorCode, @NonNull String errorMessage) {
                try {
                    sendMessageToHost(new DCMessage.ErrorResponse(request.getName(),
                            request.getRequestId(), errorCode, errorMessage));
                } catch (JSONException e) {
                    // Convert to IllegalArgumentException. The caller will have to deal with it.
                    throw new IllegalArgumentException(e);
                }
            }
        };
    }

    /** Receive and dispatch a request from the host. */
    private void dispatchRequestFromHost(@NonNull DCMessage.Request request,
                                         @NonNull Host2DCAPI host2dcHandler) throws JSONException {
        JSONObject args = request.getArgs();
        try {
            String name = request.getName();
            switch (Host2DCAPI.Functions.get(name).orElseThrow(() ->
                    new IllegalArgumentException("Unexpected request name: " + name))) {
                case SUBSCRIBE_DEVICE_TYPE:
                    host2dcHandler.subscribeDeviceType(
                            getMandatoryUInt32Argument("specialization", args),
                            makeCallback(request, v -> null ));
                    break;
                case UNSUBSCRIBE_DEVICE_TYPE:
                    host2dcHandler.unsubscribeDeviceType(
                            getMandatoryUInt32Argument("specialization", args),
                            makeCallback(request, v -> null));
                    break;
                case SUBSCRIBE_DEVICE:
                    host2dcHandler.subscribeDevice(
                            getMandatoryStringArgument("persistentDeviceId", args),
                            makeCallback(request, v -> null));
                    break;
                case UNSUBSCRIBE_DEVICE:
                    host2dcHandler.unsubscribeDevice(
                            getMandatoryStringArgument("persistentDeviceId", args),
                            makeCallback(request, v -> null));
                    break;
                case DELETE_METRIC:
                    host2dcHandler.deleteMetric(makeCallback(request, v -> null));
                    break;
                case ENABLE_PULSE_OX_MODALITIES:
                    host2dcHandler.enablePulseOxModalities(
                            getMandatoryBooleanArgument("normal", args),
                            getMandatoryBooleanArgument("fast", args),
                            getMandatoryBooleanArgument("slow", args),
                            makeCallback(request, v -> null));
                    break;
                case GENERATE_FHIR_BUNDLE:
                    host2dcHandler.generateFhirBundle(makeCallback(request, v -> null));
                    break;
                case GET_AVAILABLE_DEVICES:
                    host2dcHandler.getAvailableDevices(
                            getOptionalDeviceArgument("prototypeDevice", args),
                            getMandatoryBooleanArgument("linked", args),
                            getMandatoryBooleanArgument("connected", args),
                            getMandatoryBooleanArgument("connectable", args),
                            makeCallback(request, devices -> {
                                JSONArray value = new JSONArray();
                                if (devices != null) {
                                    for (Device device : devices) {
                                        value.put(device.toJson());
                                    }
                                }
                                return (new JSONObject()).put("value",value);
                            }));
                    break;
                default:
                    throw new IllegalArgumentException("Unexpected request name: " + name);
            }
        } catch (NumberedException e) {
            sendMessageToHost(new DCMessage.ErrorResponse(request.getName(), request.getRequestId(),
                    e.errorNumber,
                    Objects.toString(e.getMessage(),"Unknown Error")));
        } catch (Exception e) {
            sendMessageToHost(new DCMessage.ErrorResponse(request.getName(), request.getRequestId(),
                    ErrorCodes.UNKNOWN.value,
                    Objects.toString(e.getMessage(),"Unknown Error")));
        }
    }


    /* Argument helper functions */

    private @NonNull String getMandatoryStringArgument(@NonNull String argName,
                                                       @Nullable JSONObject args)
            throws NumberedException.InvalidArgument {
        String retVal = getOptionalStringArgument(argName, args);
        if (retVal != null) return retVal;
        throw new NumberedException.InvalidArgument("Missing argument: " + argName);
    }

    private @Nullable String getOptionalStringArgument(@NonNull String argName,
                                                       @Nullable JSONObject args) {
        if (args == null) return null;
        try {
            return args.getString(argName);
        } catch (JSONException e) {
            return null;
        }
    }

    private int getMandatoryUInt32Argument(@NonNull String argName,
                                           @Nullable JSONObject args)
            throws NumberedException.InvalidArgument {

        if (args != null) {
            long retVal = args.optLong(argName, -1);
            if (retVal >= 0 && retVal <= 0xFFFFFFFFL) return (int) retVal;
        }
        throw new NumberedException.InvalidArgument("Argument missing, wrong type, or out of range: " + argName);
    }

    private boolean getMandatoryBooleanArgument(@NonNull String argName,
                                                @Nullable JSONObject args)
            throws NumberedException.InvalidArgument {

        if (args == null) {
            throw new NumberedException.InvalidArgument("Missing argument: " + argName);
        }
        try {
            return args.getBoolean(argName);
        } catch (JSONException e) {
            throw new NumberedException.InvalidArgument("Argument missing or wrong type: " + argName);
        }
    }

    private @Nullable Device getOptionalDeviceArgument(@NonNull String argName,
                                                       @Nullable JSONObject args) {
        if (args == null) return null;
        try {
            JSONObject device = args.optJSONObject(argName);
            return device == null ? null : Device.fromJson(device);
        } catch (JSONException e) {
            return null;
        }
    }

    /** Receive and dispatch an event from the host. */
    private void dispatchEventFromHost(@NonNull DCMessage.Event event,
                                       @NonNull Host2DCAPI host2dcHandler) throws JSONException {
        // This method creates a lot of linter warnings because it is actually never used.
        // It is merely a placeholder for events to be defined in the future
        @NonNull String name = event.getName();
        @Nullable JSONObject args = event.getArgs();
        switch (Host2DCAPI.Events.get(name).orElseThrow(() ->
                new IllegalArgumentException("Unexpected event name: " + name))) {
            case FOOBAR: // Dummy value because we need at least one to have a valid enum...
                break;
        }
    }

    /** Send a request to the host. This method is used by the DC2HostAPI */
    <T> void sendRequestToHost(@NonNull String name, @Nullable JSONObject args,
                               @NonNull Callback<T> callback, @NonNull Function<JSONObject, T> parser) {
        DCMessage.Request request = new DCMessage.Request(name, args);
        callbackMap.put(request.getRequestId(), new CBHandler<>(callback, parser));
        try {
            sendMessageToHost(request);
        } catch (JSONException e) {
            // Re-throw as IllegalArgumentException - no known reasonable scenario where this could go wrong
            throw new IllegalArgumentException(e);
        }
    }

    /** Send an event to the host. This method is used by the DC2HostAPI */
    void sendEventToHost(@NonNull String name, @Nullable JSONObject args) {
        try {
            sendMessageToHost(new DCMessage.Event(name, args));
        } catch (JSONException e) {
            // Re-throw as IllegalArgumentException - no known reasonable scenario where this could go wrong
            throw new IllegalArgumentException(e);
        }
    }
}
