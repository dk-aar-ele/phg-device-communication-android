package dk.alexandra.phg.device_communication.manager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Receiver of error notifications when devices are discovered
 */
public interface DeviceDiscoveryErrorCallback {
    /**
     * Error types reported by error()
     */
    enum ErrorCode {
        BLUETOOTH_UNAVAILABLE,
        ONGOING_ADDITION,
        BONDING_FAILED,
        BONDING_TIMEOUT
    }

    /**
     * An error happened during discovery that should be reported to the user.
     *
     * @param code The error code
     * @param deviceName The name reported by the device, if applicable
     */
    void error(@NonNull ErrorCode code, @Nullable String deviceName);
}
