package dk.alexandra.phg.device_communication.manager;

import android.app.Activity;
import android.bluetooth.le.ScanResult;
import android.companion.AssociatedDevice;
import android.companion.AssociationInfo;
import android.companion.AssociationRequest;
import android.companion.CompanionDeviceManager;
import android.content.IntentSender;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Manage showing the Companion pop-up window.
 * <p>
 * The Android Companion Device Manager is not well documented - in perfect harmony with the
 * rest of the Bluetooth stack... One of the "hidden features" is that the scanner cannot
 * be stopped. Instead it always terminates after 20 seconds regardless of whether you want
 * it to terminate sooner or run for longer periods. We handle this using two workarounds
 * First, we use a repeating timer (the timerTask below) to restart the scanner every 22
 * seconds as long as the device discovery is active, and secondly, we will ignore
 * onDeviceFound() callbacks from the scanner after deviceDiscovery has ended.
 */

class AssociationHandler {

    // For Android 12: 22 seconds is fixed. The scanner stops after 20 seconds
    // For Android 13+: NOT USED! (The pop-up is always shown and we'll be notified when it is closed)
    private static final int DELAY_SECONDS = 22;

    // For Android 12: Android does not keep track of whether a pop-up window is showing or not. So we need to...
    // For Android 13+: NOT USED! (The pop-up is always shown and we'll be notified when it is closed)
    private static boolean companionShowing = false;

    // There can be at most one AssociationHandler at any point in time
    private static AssociationHandler currentHandler;

    private final @NonNull AssociationRequest associationRequest;
    private final @NonNull CompanionDeviceManager companionDeviceManager;
    private final @NonNull ActivityResultLauncher<IntentSenderRequest> activityLauncher;
    private final @NonNull DiscoveryHandler discoveryHandler;
    private final @NonNull DeviceDiscoveryErrorCallback errorCallback;
    private final @NonNull Handler delayTimer;
    private final @NonNull Runnable task = this::timerTask;

    private boolean terminated = false;


    interface AssociationResult extends DeviceDiscoveryErrorCallback {
        /** The association (attempt) is completed and we wish to resume scanning */
        void resume();
    }

    @FunctionalInterface
    interface DiscoveryHandler {
        void accept(@NonNull ScanResult scanResult,
                    @Nullable AssociationInfo associationInfo,
                    @NonNull AssociationResult associationResult);
    }

    AssociationHandler(@NonNull AssociationRequest associationRequest,
                       @NonNull CompanionDeviceManager companionDeviceManager,
                       @NonNull ActivityResultLauncher<IntentSenderRequest> activityLauncher,
                       @NonNull DiscoveryHandler discoveryHandler,
                       @NonNull DeviceDiscoveryErrorCallback errorCallback) {
        this.associationRequest = associationRequest;
        this.companionDeviceManager = companionDeviceManager;
        this.activityLauncher = activityLauncher;
        this.discoveryHandler = discoveryHandler;
        this.errorCallback = errorCallback;
        if (currentHandler != null) currentHandler.terminate();
        setCurrentHandler(this);
        delayTimer = new Handler(Looper.getMainLooper());
        delayTimer.post(task);
    }

    void terminate() {
        terminated = true;
        delayTimer.removeCallbacks(task);
        if (currentHandler == this) setCurrentHandler(null);
    }

    private void resume() {
        // Small delay to prevent a race with the Intent response (activityLauncherCallback)
        delayTimer.postDelayed(task,10);
    }

    private void timerTask() {
        if (terminated) return;
        companionDeviceManager.associate(associationRequest, new CdmCallback(), null);
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            delayTimer.postDelayed(task, DELAY_SECONDS * 1000L);
        }
    }

    private class CdmCallback extends CompanionDeviceManager.Callback {
        /**  @deprecated Only used prior to API 33. Can be removed when API 32 is no longer supported */
        @Override
        @Deprecated(forRemoval = true)
        public void onDeviceFound(@NonNull IntentSender intentSender) {
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                showCompanion(intentSender);
            }
        }

        @Override
        public void onAssociationPending(@NonNull IntentSender intentSender) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
                showCompanion(intentSender);
            }
        }

        @Override
        public void onAssociationCreated(@NonNull AssociationInfo associationInfo) {
            super.onAssociationCreated(associationInfo);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                delayTimer.removeCallbacks(task);
                AssociatedDevice dev = associationInfo.getAssociatedDevice();
                ScanResult scanResult = dev == null ? null : dev.getBleDevice();
                if (scanResult != null) {
                    discoveryHandler.accept(scanResult, associationInfo, associationResult);
                } else {
                    resume();
                }
            }
        }

        @Override
        public void onFailure(CharSequence charSequence) {
            if (Build.VERSION.SDK_INT == Build.VERSION_CODES.TIRAMISU
                    && "discovery_timeout".contentEquals(charSequence)) {
                resume();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                setCompanionShowing(false);
            }
        }

        private void showCompanion(IntentSender intentSender) {
            if (terminated || companionShowing) return; // Discovery stopped or window is already up

            activityLauncher.launch(new IntentSenderRequest.Builder(intentSender).build());
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                setCompanionShowing(true);
            }
        }
    }

    private final AssociationResult associationResult = new AssociationResult() {
        @Override
        public void resume() {
            if (!terminated) AssociationHandler.this.resume();
        }

        @Override
        public void error(@NonNull ErrorCode code, @Nullable String deviceName) {
            if (!terminated) errorCallback.error(code, deviceName);
        }
    };

    private void activityCallback(ActivityResult result) {
        if (result == null) return;
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            if (result.getResultCode() == Activity.RESULT_OK) {
                delayTimer.removeCallbacks(task);
                if (result.getData() != null) {
                    // Ignore the deprecations for now. In API34 (UpsideDownCake) a better
                    // solution will appear where CompanionDeviceManager.Callback.onAssociationCreated()
                    // above will receive the ScanResult object we need.
                    @SuppressWarnings({"deprecation", "RedundantSuppression"})
                    ScanResult scanResult = result.getData().getParcelableExtra(
                            CompanionDeviceManager.EXTRA_DEVICE);

                    if (scanResult != null) {
                        discoveryHandler.accept(scanResult, null, associationResult);
                        // Skip resuming searching for now - wait until the selected device has
                        // been processed and AssociationResult.* is invoked.
                        return;
                    }
                }
                resume();
            } else if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                // user cancelled device searching - we will not resume the association task
                terminate();
            }
        } else { // API >=34
            switch (result.getResultCode()) {
                case CompanionDeviceManager.RESULT_OK:
                    // Do nothing - A device was selected and will be reported to on
                    // AssociationCreated(). When this has been processed, scanning will be resumed.
                    break;
                case CompanionDeviceManager.RESULT_CANCELED:
                    // Take a break for 5 seconds, allowing the user to temporarily interact with
                    // UI behind the modal pop up window - e.g. remove a device from the list.
                    // (The modal window will re-appear after this short break.)
                    delayTimer.postDelayed(task, 5 * 1000L);
                    break;
                case CompanionDeviceManager.RESULT_DISCOVERY_TIMEOUT:
                    resume();
                    break;
                default: // RESULT_USER_REJECTED, RESULT_INTERNAL_ERROR
                    terminate();
                    break;
            }
        }
    }

    static void activityLauncherCallback(ActivityResult result) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
            companionShowing = false;
        }
        if (currentHandler == null) return; // Ignore, since we are stopped
        currentHandler.activityCallback(result);
    }

    private static void setCurrentHandler(AssociationHandler handler) { currentHandler = handler; }
    private static void setCompanionShowing(boolean state) { companionShowing = state; }
}
