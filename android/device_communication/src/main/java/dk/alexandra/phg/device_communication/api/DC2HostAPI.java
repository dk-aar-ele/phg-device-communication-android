package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Collection;
import java.util.function.Function;

/**
 * Proxy of the interface exposed by the app to the Device Communication library.
 * <p>
 * This interface is exposed by the app. This class implements a proxy wrapping the app
 * communication.
 */
public class DC2HostAPI {

    /* Function-request names calling up from DC library to the host */

    public enum Functions {

        GET_PATIENT_FOR("getPatientFor");

        private final String name;

        Functions(String name) { this.name = name; }

        public String getName() { return name; }
    }

    /* Event names signalled from DC library to the host */

    public enum Events {
        CONNECTED("connected"),
        DISCONNECTED("disconnected"),
        MDS_RECEIVED("mdsReceived"),
        METRICS_RECEIVED("metricsReceived");

        private final String name;

        Events(String name) { this.name = name; }

        public String getName() { return name; }
    }

    private final DCMessageDispatcher dispatcher;

    public DC2HostAPI(@NonNull DCMessageDispatcher dispatcher) {
        this.dispatcher = dispatcher;
    }

    <T> void sendRequestToHost(@NonNull Functions function, @Nullable JSONObject args,
                           @NonNull Callback<T> callback, @NonNull Function<JSONObject, T> parser) {
        dispatcher.sendRequestToHost(function.getName(), args, callback, parser);
    }

    private void sendEventToHost(@NonNull Events event, @Nullable JSONObject args) {
        dispatcher.sendEventToHost(event.getName(), args);
    }


    /* Functions */

    public void getPatientFor(@NonNull Callback<GetPatientForResponse> callback) {
        sendRequestToHost(Functions.GET_PATIENT_FOR, null, callback,
                jsonObject -> null);
    }
    public interface GetPatientForResponse { /* void */ }


    /* Events */

    public void connected() {
        sendEventToHost(Events.CONNECTED, null);
    }

    public void disconnected() { sendEventToHost(Events.DISCONNECTED, null); }

    public void mdsReceived() { sendEventToHost(Events.MDS_RECEIVED, null); }

    public void metricsReceived(@NonNull String persistentDeviceId,
                                @NonNull Collection<Metric> metrics) {
        JSONObject args = new JSONObject();
        try {
            JSONArray metricsArray = new JSONArray();
            for (Metric metric : metrics) {
                metricsArray.put(metric.toJson());
            }
            args.put("persistentDeviceId", persistentDeviceId);
            args.put("metrics", metricsArray);
        } catch (JSONException e) {
            // Re-throw as RuntimeException - no known reasonable scenario where this could go wrong
            throw new RuntimeException(e);
        }
        sendEventToHost(Events.METRICS_RECEIVED, args);
    }
}
