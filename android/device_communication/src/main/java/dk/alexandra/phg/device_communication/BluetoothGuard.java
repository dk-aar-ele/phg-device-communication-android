package dk.alexandra.phg.device_communication;

import android.Manifest;
import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Consumer;

/**
 * Attempt to acquire permissions to access Bluetooth.
 * <p>
 * This singleton class exposes a single method bluetoothLEGuard() to be used whenever access to
 * Bluetooth LE is needed. If the app does not have permissions to use Bluetooth or the Bluetooth
 * transceiver is off, this method will try to ask the user to fix this. A callback will be
 * invoked when this request is granted or rejected.
 * <p>
 * In order to make contact to the UI, the BluetoothGuard object needs to be registered as an
 * android.app.Application.ActivityLifecycleCallbacks with the Application.
 */
public class BluetoothGuard {
    private static final String TAG = BluetoothGuard.class.getName();

    private static final String BLUETOOTH_PERMISSION = Build.VERSION.SDK_INT >= Build.VERSION_CODES.S ?
            Manifest.permission.BLUETOOTH_CONNECT : Manifest.permission.BLUETOOTH_ADMIN;

    // The application context
    private final Context context;
    // The callbacks object that must be registered with the Application for this class to function.
    @NonNull
    private final Application.ActivityLifecycleCallbacks activityLifecycleCallbacks;
    // If the callbacks object was never registered correctly, we will return immediately rather
    // than waiting forever.
    private boolean callbacksRegistered = false;
    // The UI Activity that is currently in focus - may be null
    @Nullable
    private Activity currentActivity;
    // Cached positive result of hardware capabilities and permissions (they can never change
    // from true->false when an app is running)
    private boolean cachedCheck = false;

    @Nullable
    private ActivityResultLauncher<Intent> activityLauncher;
    @Nullable
    private ActivityResultLauncher<String> permissionLauncher;

    private static class LauncherPair {
        final ActivityResultLauncher<Intent> enableBluetoothLauncher;
        final ActivityResultLauncher<String> requestBluetoothPermissionLauncher;
        LauncherPair(ActivityResultLauncher<Intent> enableBluetoothLauncher,
                     ActivityResultLauncher<String> requestBluetoothPermissionLauncher) {
            this.enableBluetoothLauncher = enableBluetoothLauncher;
            this.requestBluetoothPermissionLauncher = requestBluetoothPermissionLauncher;
        }
    }
    private final HashMap<Activity, LauncherPair> launchers = new HashMap<>();

    // Is asking or will ask (pendingState) for Bluetooth permission (only)
    private final Set<Consumer<Boolean>> checkPermsCallbacks = new HashSet<>();
    // Is asking or will ask (pendingState) for Bluetooth permission and after a positive response
    // check the transceiver state
    private final Set<Consumer<Boolean>> checkPermsAndTransceiverCallbacks = new HashSet<>();
    // Is asking or will ask (pendingState) to turn on the Bluetooth transceiver
    private final Set<Consumer<Boolean>> checkTransceiverCallbacks = new HashSet<>();

    // true if the state (which must not be IDLE) is pending a set of launchers before requesting an
    // action from the user.
    private boolean pendingState = false;

    BluetoothGuard(Context context) {
        this.context = context;
        activityLifecycleCallbacks = new Application.ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle bundle) {
                callbacksRegistered = true;
                createLaunchers(activity);
            }

            @Override
            public void onActivityResumed(@NonNull Activity activity) {
                callbacksRegistered = true;
                currentActivity = activity;
                setLaunchers(activity);
            }

            @Override
            public void onActivityStopped(@NonNull Activity activity) {
                callbacksRegistered = true;
                if (currentActivity == activity) {
                    currentActivity = null;
                    clearLaunchers();
                }
            }

            @Override
            public void onActivityDestroyed(@NonNull Activity activity) {
                callbacksRegistered = true;
                removeLaunchers(activity);
            }

            // All other callback types are ignored...

            @Override
            public void onActivityStarted(@NonNull Activity activity) { /* ignore */ }

            @Override
            public void onActivityPaused(@NonNull Activity activity) { /* ignore */ }

            @Override
            public void onActivitySaveInstanceState(@NonNull Activity activity,
                                                    @NonNull Bundle bundle) { /* ignore */ }
        };
    }

    /**
     * Get an ActivityLifecycleCallbacks for monitoring this application's UI.
     * <p>
     * The device communication library can automatically issue requests to the Android OS to grant
     * the app permission to access Bluetooth equipment, and to turn on the Bluetooth radio.
     * In order to do this, the device communication library needs a reference to the UI Activity
     * element currently showing on the screen. So, to enable this feature, the current app must
     * register an android.app.Application.ActivityLifecycleCallbacks handler provided by this
     * method. This is an optional feature. If the application does not register this handler,
     * the library can do nothing if Bluetooth is not available.
     *
     * @return An instance of the Application.ActivityLifecycleCallbacks interface which the
     * Application may register using its registerActivityLifecycleCallbacks() method.
     */
    @NonNull
    Application.ActivityLifecycleCallbacks getActivityLifecycleCallbacks() {
        return activityLifecycleCallbacks;
    }

    private void createLaunchers(Activity activity) {
        if (activity instanceof ComponentActivity) {
            ComponentActivity ca = (ComponentActivity) activity;
            launchers.put(activity, new LauncherPair(
                    ca.registerForActivityResult(
                            new ActivityResultContracts.StartActivityForResult(),
                            activityLauncherCallback),
                    ca.registerForActivityResult(
                            new ActivityResultContracts.RequestPermission(),
                            permissionLauncherCallback)));
        }
    }

    private void removeLaunchers(Activity activity) {
        launchers.remove(activity);
    }

    private void setLaunchers(Activity activity) {
        LauncherPair lPair = launchers.get(activity);
        if (lPair == null) {
            clearLaunchers();
        } else {
            synchronized (this) {
                activityLauncher = lPair.enableBluetoothLauncher;
                permissionLauncher = lPair.requestBluetoothPermissionLauncher;
                if (pendingState) {
                    pendingState = false;
                } else {
                    return;
                }
            }
            if (!checkPermsCallbacks.isEmpty() || !checkPermsAndTransceiverCallbacks.isEmpty()) {
                requestPermission();
            } else if (!checkTransceiverCallbacks.isEmpty()) {
                requestEnable();
            }
        }
    }

    private void clearLaunchers() {
        activityLauncher = null;
        permissionLauncher = null;
    }

    private void respond(Set<Consumer<Boolean>> callbacks, boolean result) {
        List<Consumer<Boolean>> callbacksClone = new LinkedList<>(callbacks);
        callbacks.clear();
        for (Consumer<Boolean> callback : callbacksClone) callback.accept(result);
    }

    private final ActivityResultCallback<ActivityResult> activityLauncherCallback =
            result -> {
                if (result == null || result.getResultCode() != Activity.RESULT_OK) {
                    Log.w(TAG, "Bluetooth transceiver was not enabled – " +
                            "user refused request to turn it on");
                    respond(checkPermsAndTransceiverCallbacks, false);
                    respond(checkTransceiverCallbacks, false);
                } else {
                    respond(checkTransceiverCallbacks, true);
                }
            };

    private final ActivityResultCallback<Boolean> permissionLauncherCallback =
            result -> {
                if (result == null || !result) {
                    Log.w(TAG, "Permissions to use Bluetooth was refused by user");
                    respond(checkPermsAndTransceiverCallbacks, false);
                    respond(checkPermsCallbacks, false);
                    respond(checkTransceiverCallbacks, false);
                } else {
                    cachedCheck = true;
                    if (!checkPermsAndTransceiverCallbacks.isEmpty()) {
                        checkTransceiverCallbacks.addAll(checkPermsAndTransceiverCallbacks);
                        checkPermsAndTransceiverCallbacks.clear();
                        checkTransceiver();
                    }
                    respond(checkPermsCallbacks, true);
                }
            };


    /**
     * This method is designed to wrap all access to Bluetooth LE functionality.
     * It will check:
     * 1. That the hardware has Bluetooth Low Energy capabilities
     * 2. That the app permission BLUETOOTH_CONNECT has been granted
     * 3. If (checkTransceiverState) that the Bluetooth transceiver is turned on
     * <p>
     * If the hardware capabilities are not in place, the callback will be notified (false)
     * immediately. If all tests pass, the callback will be notified (true) immediately.
     * If the tests does not pass and the ActivityLifecycleCallbacks were not registered, we cannot
     * do anything about this and the callback will be notified (false) immediately, otherwise
     * we will wait for a suitable Activity to be displayed on the screen and then ask the
     * user to fix the problem. The callback will be notified whenever the result of this process
     * is available.
     *
     * @param checkTransceiverState If false, this will only check if the hardware capabilities
     *                              and application permissions are in place. If true, this will
     *                              also check that the Bluetooth transceiver is turned on.
     * @param callback              Returns true when all the checks have passed. It may take a while and
     *                              the callback may be invoked on different threads! It may also be
     *                              invoked immediately on the same thread during the call.
     */
    public void bluetoothLEGuard(boolean checkTransceiverState, @NonNull Consumer<Boolean> callback) {

        if (!cachedCheck) {

            // We need to check capabilities and permissions
            if (!context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE) ||
                    (context.getSystemService(BluetoothManager.class).getAdapter() == null)) {
                Log.w(TAG, "Bluetooth LE not supported by hardware");
                callback.accept(false);
                return;

            } else if (ContextCompat.checkSelfPermission(context, BLUETOOTH_PERMISSION)
                    != PackageManager.PERMISSION_GRANTED) {
                Log.i(TAG, "Permission to use Bluetooth has not been granted yet");
                if (checkTransceiverState) {
                    checkPermsAndTransceiverCallbacks.add(callback);
                } else {
                    checkPermsCallbacks.add(callback);
                }
                requestPermissionIfPossible();
                return;
            }
        }

        // Capabilities and Permissions OK
        cachedCheck = true;

        if (checkTransceiverState) {
            checkTransceiverCallbacks.add(callback);
            checkTransceiver();
        } else {
            // All requirements passed
            callback.accept(true);
        }
    }

    private void requestPermissionIfPossible() {
        if (!callbacksRegistered) {
            // Nothing we can do to request permissions
            Log.w(TAG, "Permissions to use Bluetooth was not granted - " +
                    "we cannot ask the user");
            respond(checkPermsAndTransceiverCallbacks, false);
            respond(checkPermsCallbacks, false);
            respond(checkTransceiverCallbacks, false);
            return;
        }
        // Request permissions
        requestPermission();
    }

    private void requestPermission() {
        ActivityResultLauncher<String> pl;
        synchronized (this) {
            if (permissionLauncher != null) {
                pl = permissionLauncher;
            } else {
                pendingState = true;
                return;
            }
        }
        pl.launch(BLUETOOTH_PERMISSION);
    }

    private void checkTransceiver() {
        boolean enabled = context.getSystemService(BluetoothManager.class).getAdapter().isEnabled();
        if (enabled || !callbacksRegistered) {
            if (!callbacksRegistered) {
                Log.w(TAG, "Bluetooth transceiver was not enabled - " +
                        "we cannot ask the user");
            }
            if (!enabled) respond(checkPermsAndTransceiverCallbacks, false);
            respond(checkTransceiverCallbacks, enabled);
            return;
        }
        requestEnable();
    }

    private void requestEnable() {
        ActivityResultLauncher<Intent> al;
        synchronized (this) {
            if (activityLauncher != null) {
                al = activityLauncher;
            } else {
                pendingState = true;
                return;
            }
        }
        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
        al.launch(enableBtIntent);
    }
}
