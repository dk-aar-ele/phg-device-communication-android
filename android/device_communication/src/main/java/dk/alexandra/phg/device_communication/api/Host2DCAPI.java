package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;

/**
 * The interface exposed by the Device Communication library to an app.
 * <p>
 * This interface is implemented by the Device Communication library.
 */
public interface Host2DCAPI {

    /* Function-request names calling down from host to the DC library */

    enum Functions {

        SUBSCRIBE_DEVICE_TYPE("subscribeDeviceType"),
        UNSUBSCRIBE_DEVICE_TYPE("unsubscribeDeviceType"),
        SUBSCRIBE_DEVICE("subscribeDevice"),
        UNSUBSCRIBE_DEVICE("unsubscribeDevice"),
        DELETE_METRIC("deleteMetric"),
        ENABLE_PULSE_OX_MODALITIES("enablePulseOxModalities"),
        GENERATE_FHIR_BUNDLE("generateFhirBundle"),
        GET_AVAILABLE_DEVICES("getAvailableDevices");

        private final String name;

        Functions(String name) { this.name = name; }

        public String getName() { return name; }

        public static Optional<Functions> get(String name) {
            return Arrays.stream(Functions.values())
                    .filter(f -> f.name.equals(name))
                    .findFirst();
        }
    }

    /* Event names signalled from the host to the DC library */

    enum Events {

        FOOBAR("BLA"); // Dummy value because we need at least one to have a valid enum...

        private final String name;

        Events(String name) { this.name = name; }

        public String getName() { return name; }

        public static Optional<Events> get(String name) {
            return Arrays.stream(Events.values())
                    .filter(f -> f.name.equals(name))
                    .findFirst();
        }
    }


    /* Functions */

    void subscribeDeviceType(int specialization, @NonNull Callback<Void> callback) throws NumberedException;
    void unsubscribeDeviceType(int specialization, @NonNull Callback<Void> callback) throws NumberedException;
    void subscribeDevice(@NonNull String persistentDeviceId, @NonNull Callback<Void> callback) throws NumberedException;
    void unsubscribeDevice(@NonNull String persistentDeviceId, @NonNull Callback<Void> callback) throws NumberedException;
    void deleteMetric(@NonNull Callback<Void> callback) throws NumberedException;
    void enablePulseOxModalities(boolean normal, boolean fast, boolean slow,
                                 @NonNull Callback<Void> callback) throws NumberedException;
    void generateFhirBundle(@NonNull Callback<Void> callback) throws NumberedException;
    void getAvailableDevices(@Nullable Device prototypeDevice, boolean linked,
                             boolean connected, boolean connectable,
                             @NonNull Callback<Set<Device>> callback) throws NumberedException;


    /* Events */


}
