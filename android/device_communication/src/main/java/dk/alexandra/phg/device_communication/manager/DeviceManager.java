package dk.alexandra.phg.device_communication.manager;

import android.annotation.SuppressLint;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.companion.AssociationInfo;
import android.companion.AssociationRequest;
import android.companion.BluetoothLeDeviceFilter;
import android.companion.CompanionDeviceManager;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.ParcelUuid;
import android.util.Log;

import androidx.activity.ComponentActivity;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.IntentSenderRequest;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pcha.btmanager.PhdInformation;

import java.util.Map;
import java.util.Set;

import dk.alexandra.phg.device_communication.BluetoothGuard;

/**
 * Manage Personal Health Devices associated with this app (a.k.a. "whitelist").
 * <p>
 * This class represents the "model" part of the model-view-controller pattern for a UI for editing
 * the whitelist. This object is therefore closely linked to the view represented by an Android
 * ComponentActivity object. Since these objects are recreated on different occasions (such as
 * screen rotations), new DeviceManager objects can be created to take over the role of an old
 * object.
 * <p>
 * The first time a DeviceManager object is created marks the beginning of a manager session, which
 * may span the life of multiple successive DeviceManager objects. The shared state of this session
 * is contained in a ManagerSession object. When the user has finished editing the whitelist, this
 * session must be ended (see endSession()), which will save the whitelist and free the devices.
 */
public class DeviceManager {
    private static final String TAG = DeviceManager.class.getName();

    // The linter will complain about a memory leak by storing arbitrary Context objects.
    // But we only explicitly store the ApplicationContext, which is safe to do in a static field.
    @SuppressLint("StaticFieldLeak")
    private static ManagerSession managerSession = null;

    @NonNull private final ComponentActivity activity;
    @NonNull private final BluetoothGuard bluetoothGuard;
    @NonNull private final ActivityResultLauncher<IntentSenderRequest> activityLauncher;
    @NonNull private final CompanionDeviceManager companionDeviceManager;

    // != null when a Device discovery is active
    @Nullable private AssociationHandler associationHandler = null;


    /**
     * Get the current whitelist of personal health devices available to the app.
     *
     * @param context The Android application context
     * @return A Set of PhdInformation objects holding details of each PHD
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public static Set<PhdInformation> getWhitelist(@NonNull Context context) {
        return WhitelistManager.getWhitelist(context);
    }


    /**
     * Device manager for setting up personal health device associations with an app.
     * <p>
     * The device manager offers functionality to establish and maintain a whitelist of
     * personal health devices that can be used as sources of observations.
     * <p>
     * An active androidx.activity.ComponentActivity must be available to receive notifications
     * from the OS when new devices show appear. This activity must be registered here during
     * its own initialization, i.e. the DeviceManager must always be constructed before the provided
     * activity is started!
     *
     * @param activity A ComponentActivity which can receive notifications about new devices.
     * @param bluetoothGuard The guard handling Bluetooth runtime permissions
     */
    public DeviceManager(@NonNull ComponentActivity activity,
                         @NonNull BluetoothGuard bluetoothGuard) {

        beginSessionIfNotStarted(activity);

        this.activity = activity;
        this.bluetoothGuard = bluetoothGuard;
        this.activityLauncher = activity.registerForActivityResult(
                new ActivityResultContracts.StartIntentSenderForResult(),
                AssociationHandler::activityLauncherCallback);
        companionDeviceManager =
                (CompanionDeviceManager) activity.getSystemService(Context.COMPANION_DEVICE_SERVICE);

    }

    public static void beginSessionIfNotStarted(@NonNull Context context) {
        if (managerSession == null) {
            managerSession = new ManagerSession(context.getApplicationContext());
        }
    }

    private static void checkSession() {
        if (managerSession == null)
            throw new IllegalStateException("No active Device Manager Session");
    }

    @Nullable
    static ManagerSession getSession() { return managerSession; }

    /**
     * End the device manager session.
     * <p>
     * This will save the whitelist and disconnect all device connections so that the devices can
     * be used for "real" PHD communication.
     * <p>
     * No further interaction is allowed with existing DeviceManager objects after endSession() is
     * called. A new session can be started by creating a new DeviceManager.
     */
    public static void endSession() {
        if (managerSession == null) return;
        managerSession.cleanup();
        managerSession = null;
    }


    /**
     * Add a receiver of whitelist updates.
     * <p>
     * Adds a receiver of updates to the whitelist. The receiver can be removed using
     * removeWhitelistListener(), but the listener will be stored as a weak reference, so it
     * should not leak memory if it is not removed.
     * <p>
     * This method must be called while a Device Manager Session is active, i.e. after the
     * creation of a DeviceManager object, but before endSession().
     * <p>
     * The method will return the current state of the whitelist, which will be the origin of all
     * further updates.
     * @param listener The listener
     * @return The whitelist as-is: A set of PhdInformation objects each associated to a boolean
     *         that indicates if the device is currently connected.
     * @throws IllegalStateException if a Device Manager Session was not active.
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public static Map<PhdInformation, Boolean> addWhitelistListener(@NonNull WhitelistListener listener) {
        checkSession();
        managerSession.whitelistListenerHandler.addWhitelistListener(listener);
        return managerSession.whitelistManager.getWhitelistWithConnectionState();
    }


    /**
     * Remove a receiver of whitelist updates.
     * <p>
     * Removes a receiver previously added using addWhitelistListener(). It is a no-op if the
     * argument was not on the list or if a Device Manager Session was not active.
     * @param listener The listener
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public static void removeWhitelistListener(@NonNull WhitelistListener listener) {
        if (managerSession != null)
            managerSession.whitelistListenerHandler.removeWhitelistListener(listener);
    }


    /**
     * Remove a device from the whitelist.
     * <p>
     * Removes a personal health device from the whitelist. It is a no-op if the argument was not
     * on the list
     * @param phdInformation The device to remove from the list
     * @throws IllegalStateException if a Device Manager Session was not active.
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public void removeDeviceFromWhitelist(@NonNull PhdInformation phdInformation) {
        checkSession();
        managerSession.whitelistManager.removeDevice(phdInformation, companionDeviceManager);
    }


    /**
     * Start device discovery.
     * <p>
     * Only devices that meet the criteria and are not already on the whitelist will be
     * discovered.
     *
     * @param errorCallback A callback to use for user interaction on errors
     * @param specializations The types of devices that we are interested in
     * @throws IllegalStateException If the device discovery is already started
     * @throws UnsupportedOperationException If companion device setup is not supported by the OS
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public void startDeviceDiscovery(@NonNull DeviceDiscoveryErrorCallback errorCallback,
                                     @NonNull int[] specializations) {

        checkSession();
        if (associationHandler != null) throw new IllegalStateException(
                "Attempt to start device discovery while another device discovery is active");

        // Test if companion device manager is supported
        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_COMPANION_DEVICE_SETUP)) {
            Log.e(TAG, "Companion Device Setup is not supported by this OS");
            throw new UnsupportedOperationException("Companion Device Setup is not supported by this OS");
        }
        AssociationRequest.Builder asb = new AssociationRequest.Builder();
        for (int i : specializations) {
            asb.addDeviceFilter(new BluetoothLeDeviceFilter.Builder()
                    .setScanFilter(new ScanFilter.Builder()
                            .setServiceUuid(new ParcelUuid(managerSession.mdcToServiceUuid.get(i)))
                            .build())
                    .build());
        }
        asb.setSingleDevice(false);

        associationHandler = new AssociationHandler(asb.build(), companionDeviceManager,
                activityLauncher, this::handleDiscovery, errorCallback);
    }


    /**
     * Stop device discovery.
     * <p>
     * If no discovery is active, this is a no-op.
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public void stopDeviceDiscovery() {
        if (associationHandler == null) return;
        associationHandler.terminate();
        associationHandler = null;
    }


    private void handleDiscovery(@NonNull ScanResult scanResult, @Nullable AssociationInfo associationInfo,
                                 @NonNull AssociationHandler.AssociationResult associationResult) {
        bluetoothGuard.bluetoothLEGuard(true, success -> {
            if (managerSession == null) return;
            if (Boolean.TRUE.equals(success)) {
                managerSession.whitelistManager.addDevice(scanResult, associationInfo, associationResult);
            } else {
                associationResult.error(DeviceDiscoveryErrorCallback.ErrorCode.BLUETOOTH_UNAVAILABLE,
                        null);
                // Don't resume the associationTask - makes no sense
                // if we cannot connect
            }
        });
    }
}
