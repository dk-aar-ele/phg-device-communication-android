package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * Parses and serializes the messages between the host and the device communication library.
 * <p>
 * A device communication message (DCMessage) has three sub-types:
 * <p>
 *  - Request:   A request is like a function invocation, soliciting a response
 *  - Response:  A response is always sent as an answer to a request. It contains either the
 *               requested information (could be void - which will just be an acknowledgement
 *               that the request was processed), or it can be an error message and error code
 *  - Event:     An event is just a simple message - no response will be issued.
 * <p>
 *  The message is wrapped in a JSON object using the following envelope:
 * <p>
 * ```json
 * {
 *    messageType: ( "REQUEST" | "RESPONSE" | "EVENT" ),
 *    name: "...",        // Only for requests and events - the name of the thing.
 *    args: {},           // A JSON Object containing the arguments - may be empty/void/absent (must be for error responses).
 *    requestId: "1",     // Only for requests and responses. A unique string linking the two together
 *    errorCode: 0,       // Only for error responses - an optional non-zero error code, or 0 if none.
 *    errorMessage: "..." // Only for error responses - the error message. Mandatory.
 * }
 * ```
 */
abstract class DCMessage {

    /* Message types - values for the messageType field in the envelope header */

    private static final String MESSAGE_TYPE_REQUEST = "REQUEST";
    private static final String MESSAGE_TYPE_RESPONSE = "RESPONSE";
    private static final String MESSAGE_TYPE_EVENT = "EVENT";

    /* Envelope field names */

    private static final String ENVELOPE_MESSAGE_TYPE = "messageType";
    private static final String ENVELOPE_NAME = "name";
    private static final String ENVELOPE_REQUEST_ID = "requestId";
    private static final String ENVELOPE_ARGS = "args";
    private static final String ENVELOPE_ERROR_CODE = "errorCode";
    private static final String ENVELOPE_ERROR_MESSAGE = "errorMessage";

    /** Counter generating unique request IDs */
    private static final AtomicInteger lastRequestId = new AtomicInteger();

    private final @NonNull String messageType;
    private final @NonNull String name;
    private final @Nullable JSONObject args;

    private DCMessage(@NonNull String messageType, @NonNull String name, @Nullable JSONObject args) {
        this.messageType = messageType;
        this.name = name;
        this.args = args;
    }

    @Nullable JSONObject getArgs() { return args; }

    protected void fillEnvelope(@NonNull JSONObject envelope) throws JSONException {
        envelope.put(ENVELOPE_MESSAGE_TYPE, messageType);
        envelope.put(ENVELOPE_NAME, name);
        envelope.putOpt(ENVELOPE_ARGS, args);
    }

    @NonNull String getName() { return name; }

    String serialize() throws JSONException {
        JSONObject envelope = new JSONObject();
        fillEnvelope(envelope);
        return envelope.toString();
    }


    static class Request extends DCMessage {
        private final @NonNull String requestId;

        Request(@NonNull String name, @Nullable JSONObject args) {
            this(name, String.valueOf(lastRequestId.incrementAndGet()), args);
        }

        Request(@NonNull String name, @NonNull String requestId, @Nullable JSONObject args) {
            super(MESSAGE_TYPE_REQUEST, name, args);
            this.requestId = requestId;
        }

        @Override
        protected void fillEnvelope(@NonNull JSONObject envelope) throws JSONException {
            super.fillEnvelope(envelope);
            envelope.put(ENVELOPE_REQUEST_ID, requestId);
        }

        @NonNull String getRequestId() { return requestId; }
    }


    static class Response extends DCMessage {
        private final @NonNull String requestId;

        Response(@NonNull String name, @NonNull String requestId, @Nullable JSONObject args) {
            super(MESSAGE_TYPE_RESPONSE, name, args);
            this.requestId = requestId;
        }

        @Override
        protected void fillEnvelope(@NonNull JSONObject envelope) throws JSONException {
            super.fillEnvelope(envelope);
            envelope.put(ENVELOPE_REQUEST_ID, requestId);
        }

        @NonNull String getRequestId() { return requestId; }
    }


    static class ErrorResponse extends Response {

        private final int errorCode;
        private final @NonNull String errorMessage;

        ErrorResponse(@NonNull String name, @NonNull String requestId, int errorCode, @NonNull String errorMessage) {
            super(name, requestId, null);
            this.errorCode = errorCode;
            this.errorMessage = errorMessage;
        }

        @Override
        protected void fillEnvelope(@NonNull JSONObject envelope) throws JSONException {
            super.fillEnvelope(envelope);
            envelope.put(ENVELOPE_ERROR_CODE, errorCode);
            envelope.put(ENVELOPE_ERROR_MESSAGE, errorMessage);
        }

        int getErrorCode() { return errorCode; }
        @NonNull String getErrorMessage() { return errorMessage; }
    }


    static class Event extends DCMessage {
        Event(@NonNull String name, @Nullable JSONObject args) {
            super(MESSAGE_TYPE_EVENT, name, args);
        }
    }


    @NonNull
    static DCMessage parse(@NonNull String message) throws JSONException {
        JSONObject json = new JSONObject(message);
        String messageType = json.getString(ENVELOPE_MESSAGE_TYPE);
        JSONObject args = json.optJSONObject(ENVELOPE_ARGS);
        String name = json.getString(ENVELOPE_NAME);
        String errorMessage;
        String requestId;
        int errorCode;

        switch (messageType) {
            case MESSAGE_TYPE_REQUEST:
                requestId = json.getString(ENVELOPE_REQUEST_ID);
                return new Request(name, requestId, args);
            case MESSAGE_TYPE_RESPONSE:
                requestId = json.getString(ENVELOPE_REQUEST_ID);
                errorCode = json.optInt(ENVELOPE_ERROR_CODE, 0);
                errorMessage = json.optString(ENVELOPE_ERROR_MESSAGE);
                if ("".equals(errorMessage)) {
                    return new Response(name, requestId, args);
                } else {
                    return new ErrorResponse(name, requestId, errorCode, errorMessage);
                }
            case MESSAGE_TYPE_EVENT:
                return new Event(name, args);
            default:
                throw new JSONException("Invalid contents");
        }
    }
}