package dk.alexandra.phg.device_communication.api;

/**
 * Definition of the errorCode fields used in function error responses
 */
public enum ErrorCodes {

    /** Unknown / unexpected runtime exception occurred */
    UNKNOWN(1),

    /** An invalid argument was supplied to the function */
    INVALID_ARGUMENT(2),

    /** The requested operation is (currently) unsupported */
    UNSUPPORTED_OPERATION(3),

    /** Permissions to access Bluetooth devices has not been granted to his app */
    BLUETOOTH_PERMISSION_NOT_GRANTED(4),

    /** The Bluetooth transceiver is off (the user did not accept request to turn it on) */
    BLUETOOTH_TURNED_OFF(5);

    /** The errorCode value used in a RESPONSE type DCMessage */
    public final int value;

    ErrorCodes(int value) {
        this.value = value;
    }
}
