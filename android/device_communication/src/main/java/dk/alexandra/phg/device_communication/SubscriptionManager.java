package dk.alexandra.phg.device_communication;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pcha.btmanager.PhdInformation;

import java.util.HashSet;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import dk.alexandra.phg.device_communication.api.Callback;
import dk.alexandra.phg.device_communication.api.Device;
import dk.alexandra.phg.device_communication.api.ErrorCodes;
import dk.alexandra.phg.device_communication.api.NumberedException;
import dk.alexandra.phg.device_communication.manager.DeviceManager;

/**
 * Manage subscriptions and start the Device Communication service.
 * <p>
 * The SubscriptionManager is responsible for managing the subscriptions requested by the hosting
 * application, and for starting the background service running the CODE components. The manager
 * will map subscription/un-subscription requests to the current whitelist of devices.
 */
public class SubscriptionManager {

    private static final String TAG = SubscriptionManager.class.getName();

    private final Supplier<Context> applicationContextSupplier;
    private final Supplier<BluetoothGuard> bluetoothGuardSupplier;

    private Set<Integer> subscribedTypes = new HashSet<>();
    private Set<String> subscribedDevices = new HashSet<>();

    private Set<PhdInformation> whitelist;

    /**
     * @param applicationContextSupplier Get the current application Context object. This will be
     *                                   NonNull when the DeviceCommunicationController has been
     *                                   initialized (which is guaranteed when any of the methods
     *                                   of this object are invoked)
     * @param bluetoothGuardSupplier Get the current BluetoothGuard object. This will be NonNull
     *                               when the DeviceCommunicationController has been initialized
     *                               (which is guaranteed when any of the methods of this object
     *                               are invoked)
     */
    SubscriptionManager(@NonNull Supplier<Context> applicationContextSupplier,
                        @NonNull Supplier<BluetoothGuard> bluetoothGuardSupplier) {
        this.applicationContextSupplier = applicationContextSupplier;
        this.bluetoothGuardSupplier = bluetoothGuardSupplier;
    }

    void subscribeDeviceType(int specialization, @NonNull Callback<Void> callback) {
        updateSubscriptions(() -> subscribedTypes.add(specialization), callback);
    }

    void unsubscribeDeviceType(int specialization, @NonNull Callback<Void> callback) {
        updateSubscriptions(() -> subscribedTypes.remove(specialization), callback);
    }

    void subscribeDevice(@NonNull String persistentDeviceId, @NonNull Callback<Void> callback) {
        updateSubscriptions(() -> subscribedDevices.add(persistentDeviceId), callback);
    }

    void unsubscribeDevice(@NonNull String persistentDeviceId, @NonNull Callback<Void> callback) {
        updateSubscriptions(() -> subscribedDevices.remove(persistentDeviceId), callback);
    }

    void getAvailableDevices(@Nullable Device prototypeDevice, boolean linked, boolean connected,
                             boolean connectable, @NonNull Callback<Set<Device>> callback) {
        bluetoothGuardSupplier.get().bluetoothLEGuard(false, granted -> {
            if (granted == null || !granted) {
                Log.e(TAG, "No Bluetooth permissions when loading whitelist");
                callback.error(new NumberedException.BluetoothPermissionNotGranted(
                        "No Bluetooth permissions when loading whitelist"));
            } else if (prototypeDevice == null && linked && !connected && !connectable) {
                callback.success(getWhitelist()
                        .stream().map(phdInfo ->
                                new Device(phdInfo.getSystemId(),
                                        phdInfo.getName(),
                                        phdInfo.getManufacturer(),
                                        phdInfo.getModelNumber(),
                                        phdInfo.getSerialNumber(),
                                        phdInfo.getSpecializations().stream().mapToInt(ss ->
                                                (int) ss.getSpecialization()).toArray())
                        ).collect(Collectors.toSet()));
            } else {
                Log.e(TAG, "getAvailableDevices(): Search request is currently unsupported");
                callback.error(new NumberedException.UnsupportedOperation(
                        "Search request is currently unsupported"));
            }
        });
    }

    @NonNull
    private Set<PhdInformation> getWhitelist() {
        if (whitelist == null) {
            Context appContext = applicationContextSupplier.get();
            DeviceCommunicationService.ensurePhdInfoFileIsLoaded(appContext);
            whitelist = DeviceManager.getWhitelist(appContext);
        }
        return whitelist;
    }

    private void updateSubscriptions(@NonNull Runnable update, @NonNull Callback<Void> callback) {
        bluetoothGuardSupplier.get().bluetoothLEGuard(false, granted -> {
            if (granted != null && granted) {
                updateSubscriptionsWithBluetoothPermissions(update, callback);
            } else {
                Log.e(TAG, "No Bluetooth permissions when updating subscriptions");
                callback.error(new NumberedException.BluetoothPermissionNotGranted(
                        "No Bluetooth permissions when updating subscriptions"));
            }
        });
    }

    // Bluetooth permissions are granted at this point...
    private void updateSubscriptionsWithBluetoothPermissions(@NonNull Runnable update,
                                                             @NonNull Callback<Void> callback) {
        Set<Integer> rollbackSubscribedTypes = new HashSet<>(subscribedTypes);
        Set<String> rollbackSubscribedDevices = new HashSet<>(subscribedDevices);
        update.run();
        Set<PhdInformation> devices = new HashSet<>();
        for (int type : subscribedTypes) {
            devices.addAll(getWhitelist().stream()
                    .filter(pi -> pi.getSpecializations().stream()
                            .anyMatch(ss -> ss.getSpecialization() ==
                                    Integer.toUnsignedLong(type)))
                    .collect(Collectors.toSet()));
        }
        devices.addAll(getWhitelist().stream()
                .filter(pi -> subscribedDevices.contains(pi.getSystemId()))
                .collect(Collectors.toSet()));
        if (!devices.isEmpty()) {
            bluetoothGuardSupplier.get().bluetoothLEGuard(true, granted -> {
                if (granted != null && granted) {
                    updateSubscriptionsWithBluetoothOn(devices, rollbackSubscribedTypes,
                            rollbackSubscribedDevices, callback);
                } else {
                    subscribedTypes = rollbackSubscribedTypes;
                    subscribedDevices = rollbackSubscribedDevices;
                    Log.e(TAG, "Bluetooth could not be turned on");
                    callback.error(new NumberedException.BluetoothTurnedOff(
                            "Bluetooth could not be turned on"));
                }
            });
        } else {
            updateSubscriptionsWithBluetoothOn(devices, rollbackSubscribedTypes,
                    rollbackSubscribedDevices, callback);
        }
    }

    // Bluetooth has been turned on at this point if devices is non-empty
    private void updateSubscriptionsWithBluetoothOn(@NonNull Set<PhdInformation> devices,
                                                    @NonNull Set<Integer> rollbackSubscribedTypes,
                                                    @NonNull Set<String> rollbackSubscribedDevices,
                                                    @NonNull Callback<Void> callback) {
        try {
            DeviceCommunicationService.updateSubscriptions(applicationContextSupplier.get(),
                    devices);
            callback.success(null);
        } catch (IllegalStateException ignored) { // ServiceStartNotAllowedException
            // This should never happen in real life scenarios: The updateSubscription()
            // is invoked directly from a call originating from the running app's main
            // thread, so it cannot be suspended at this point.
            subscribedTypes = rollbackSubscribedTypes;
            subscribedDevices = rollbackSubscribedDevices;
            Log.e(TAG, "Attempt to start background service not allowed when the " +
                    "app is in the background");
            callback.error(ErrorCodes.UNKNOWN.value, "Attempt to start " +
                    "background service not allowed when the app is in the background");
        }
    }
}
