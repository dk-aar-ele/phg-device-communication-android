package dk.alexandra.phg.device_communication.api;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * Callback for an asynchronous request.
 * <p>
 * When a response is received for the request, exactly one of the success() and error() methods
 * will be invoked. There can be at most one response to a request, and a correctly functioning
 * implementation should *always* respond exactly once - otherwise memory leaks and possibly other
 * bad behaviours are likely to occur.
 */
public interface Callback<T> {
    /**
     * The request was successful and this is the response.
     * @param responseArgs The response
     */
    void success(@Nullable T responseArgs);

    /**
     * The request resulted in an exception.
     * @param errorCode A code describing the error (must be documented in the API description of
     *                  the request). 0 if the error is unknown / unexpected.
     * @param errorMessage An error message - typically just the Exception.toString() message,
     *                     although other meaningful texts may also be supplied.
     */
    void error(int errorCode, @NonNull String errorMessage);
    default void error(@NonNull NumberedException ex) {
        String message = ex.getMessage();
        error(ex.errorNumber, message != null ? message : "");
    }
}
