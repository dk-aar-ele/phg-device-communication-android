package dk.alexandra.phg.device_communication;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;

import com.pcha.codephg.inter.CompoundEntry;
import com.pcha.codephg.inter.EnumEntry;
import com.pcha.codephg.inter.MdsIntermediary;
import com.pcha.codephg.inter.MetricIntermediary;
import com.pcha.codephg.inter.NumericEntry;
import com.pcha.codephg.inter.PhgFields;
import com.pcha.btmanager.BtManager;
import com.pcha.btmanager.IntermediaryCallback;
import com.pcha.btmanager.PhdInformation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import dk.alexandra.phg.device_communication.api.DC2HostAPI;
import dk.alexandra.phg.device_communication.api.Metric;


/**
 * The Device Communication service.
 * <p>
 * The service wraps the CODE components and will launch as a background service whenever the
 * list of device subscriptions is non-empty. The service will terminate when the list is empty.
 * <p>
 * This service is intended to manage the CODE Bluetooth manager subsystem in whitelist-mode only.
 */
public class DeviceCommunicationService extends Service {
    private static final String TAG = DeviceCommunicationService.class.getName();

    private static final String BLUETOOTH_DEVICES_EXTRA = "BluetoothDevices";

    private static boolean phdInfoFileLoaded = false;

    private static final AtomicInteger measurementCounter = new AtomicInteger(1);

    @Nullable
    private BtManager btManager;
    private final Set<PhdInformation> currentSubscriptions = new HashSet<>();



    /**
     * Ensure the PhdInfo file is loaded.
     * <p>
     * The PhdInfo file is used for the device whitelist. The file is managed by the CODE BtManager
     * class and will be loaded by the constructor of this class when the first BtManager object is
     * constructed. This method will ensure that the file is loaded by creating and killing a
     * BtManager if none was ever created.
     *
     * @param context The (application) Context.
     */
    public static void ensurePhdInfoFileIsLoaded(@NonNull Context context) {
        if (phdInfoFileLoaded) return;
        startBtManager(context).close();
    }

    /**
     * Update the set of subscribed PHD devices.
     * <p>
     * This will notify the service about the current set of devices, we subscribe to. If the list
     * is non-empty, the CODE BtManager will be executed in the background service. Otherwise, the
     * BtManager will be killed, and the service will stop.
     *
     * @param context The (application) Context.
     * @param devices The devices we want CODE to whitelist and subscribe to.
     * @throws IllegalStateException When the application is suspended, starting background services
     *                               will not be permitted.
     */
    public static void updateSubscriptions(@NonNull Context context,
                                           @NonNull Set<PhdInformation> devices) {
        Intent i = new Intent(context, DeviceCommunicationService.class);
        i.putParcelableArrayListExtra(BLUETOOTH_DEVICES_EXTRA,
                new ArrayList<>(Arrays.asList(devices.stream()
                .map(PhdInformation::getBluetoothDevice)
                .toArray(BluetoothDevice[]::new))));
        context.startService(i);
    }

    /**
     * Enable/disable the individual pulse oximeter continuous streams.
     * <p>
     * A pulse oximeter can provide a continuous stream of measurements (saturation + pulse
     * observations) with a frequency of (typically) 1 Hz. Three different modalities may be
     * provided - all of which may individually be enabled or disabled at the source to save
     * processing cycles.
     * <p>
     * Setting this filter will take effect immediately. I.e. it will enable or disable any
     * active connection as well, starting at the next sample.
     *
     * @param normal Enable the normal-modality stream (the default modality applicable in most
     *               situations).
     * @param fast   Enable the fast-modality stream (typically used for sleep monitoring - apnoea.
     *               Highly sensitive to movement).
     * @param slow   Enable the slow-modality stream (typically used with heavy movements, such as
     *               monitoring during exercise).
     */
    public static void enablePulseOxModalities(boolean normal, boolean fast, boolean slow) {
        BtManager.setEnableInstantPulseOxDataFiltering(!normal);
        BtManager.setEnableFastModalityFiltering(!fast);
        BtManager.setEnableSlowModalityFiltering(!slow);
    }

    @NonNull
    private static BtManager startBtManager(@NonNull Context context) {
        BtManager.setScannerEnabled(false);
        BtManager manager = new BtManager(context, false, false, true);
        phdInfoFileLoaded = true;
        return manager;
    }



    @Override
    public void onCreate() {
        super.onCreate();
        onCreateImpl();
    }

    private void onCreateImpl() {
        BtManager.setPhgFields(createPhgFields());
        BtManager.setIntermediaryCallback(intermediaryCallback);
    }

    @Override
    public IBinder onBind(Intent intent) {
        // Mandatory dummy implementation - we do not support binding to the service
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.hasExtra(BLUETOOTH_DEVICES_EXTRA)) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU) {
                List<BluetoothDevice> devices =
                        intent.getParcelableArrayListExtra(BLUETOOTH_DEVICES_EXTRA, BluetoothDevice.class);
                if (devices != null) receivedSubscriptions(devices);
            } else {
                @SuppressWarnings({"deprecation", "RedundantSuppression"})
                List<BluetoothDevice> devices =
                        intent.getParcelableArrayListExtra(BLUETOOTH_DEVICES_EXTRA);
                if (devices != null) receivedSubscriptions(devices);
            }
        }
        if (currentSubscriptions.isEmpty()) {
            stopSelf(startId);
            return START_NOT_STICKY;
        } else {
            return START_REDELIVER_INTENT;
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        onDestroyImpl();
    }
    
    private void onDestroyImpl() {
        receivedSubscriptions(new LinkedList<>());
        BtManager.setIntermediaryCallback(null);
        BtManager.setPhgFields(null);
    }


    private void receivedSubscriptions(@NonNull List<BluetoothDevice> devices) {
        if (devices.isEmpty() && currentSubscriptions.isEmpty()) return; // Skip all the trouble
        if (btManager == null) {
            btManager = startBtManager(this);
        }
        Set<PhdInformation> newSubscriptions = BtManager.getKnownDevices().stream()
                    .filter(pi -> devices.contains(pi.getBluetoothDevice()))
                    .collect(Collectors.toSet());
        BtManager.setWhiteList(newSubscriptions.stream()
                .map((pi -> pi.getBluetoothDevice().getAddress()))
                .collect(Collectors.toList()));
        for (PhdInformation pi : newSubscriptions) {
            if (!currentSubscriptions.contains(pi)) {
                currentSubscriptions.add(pi);
                btManager.connectToDeviceFromDiscovery(pi.getBluetoothDevice(), pi);
            }
        }
        for (Iterator<PhdInformation> pii = currentSubscriptions.iterator(); pii.hasNext();) {
            PhdInformation pi = pii.next();
            if (!newSubscriptions.contains(pi)) {
                pii.remove();
                BtManager.removeDeviceFromSession(pi.getBluetoothDevice());
            }
        }
        if (currentSubscriptions.isEmpty()) {
            btManager.close();
            btManager = null;
        }
    }

    @NonNull
    private PhgFields createPhgFields() {
        // Dummy implementation
        return new PhgFields(0);
    }

    private final IntermediaryCallback intermediaryCallback = new IntermediaryCallback() {
        @Override
        public void onReceiveMdsIntermediary(MdsIntermediary mds) {
            // Not used at this point
        }

        @Override
        public void onReceiveMetricIntermediaries(List<MetricIntermediary> metricList,
                                                  MdsIntermediary mds) {
            // Copy metricList! It is re-used by the sender!!!
            List<MetricIntermediary> lmi = new ArrayList<>(metricList);
            taskQueue.post(() -> handleMetricIntermediaries(lmi, mds));
        }

        @Override
        public void onDisconnect(MdsIntermediary mds) {
            Log.i(TAG, "Disconnected " +
                    (mds.getModelNumber() != null ? mds.getModelNumber() : "unknown"));
        }

        private final @NonNull Handler taskQueue = new Handler(Looper.getMainLooper());

        private void handleMetricIntermediaries(List<MetricIntermediary> intermediaryMetrics,
                                                MdsIntermediary mds) {
            List<Metric> apiMetrics = new LinkedList<>();
            for (MetricIntermediary intermediaryMetric : intermediaryMetrics) {
                String metricId = String.valueOf(measurementCounter.getAndIncrement());
                int typeCode = (int)intermediaryMetric.getMeasurementTypeCode();
                int[] supplementalTypes = intermediaryMetric
                        .getSupplementalTypesNomenclatures().stream()
                        .mapToInt(Long::intValue)
                        .toArray();
                if (supplementalTypes.length == 0) supplementalTypes = null;
                int measurementStatus = intermediaryMetric.isHasMeasurementStatus() ?
                        intermediaryMetric.getMeasurementStatus() :
                        Metric.MEASUREMENT_STATUS_NOT_USED;
                String agentTimestamp = null;
                if (intermediaryMetric.getAgentTimeStamp() != null) {
                    agentTimestamp = intermediaryMetric.getAgentTimeStamp().getFhirString();
                }
                String measurementReceptionTimestamp = intermediaryMetric.getTimeOfMeasurementReception().getFhirString();

                if (intermediaryMetric.isNumeric()) {
                    NumericEntry numericEntry = intermediaryMetric.getNumericEntry();
                    apiMetrics.add(new Metric.Numeric(
                            metricId,
                            typeCode,
                            supplementalTypes,
                            agentTimestamp,
                            measurementReceptionTimestamp,
                            measurementStatus,
                            numericEntry.getValue().toPcd01String(),
                            (int)numericEntry.getUnitCode32()
                    ));
                } else if (intermediaryMetric.isCompoundNumeric()) {
                    List<CompoundEntry> compoundEntries = intermediaryMetric.getCompoundEntries();
                    apiMetrics.add(new Metric.CompoundNumeric(
                            metricId,
                            typeCode,
                            supplementalTypes,
                            agentTimestamp,
                            measurementReceptionTimestamp,
                            measurementStatus,
                            compoundEntries.stream().map(ce -> new Metric.Component(
                                    (int)ce.getType(),
                                    ce.getValue().toPcd01String(),
                                    (int)ce.getUnitCode32())).collect(Collectors.toList()
                            )
                    ));
                } else if (intermediaryMetric.isEnumeration() && (intermediaryMetric.getEnumEntry().isBits16Enum() ||
                        intermediaryMetric.getEnumEntry().isBits32Enum())) {
                    EnumEntry enumEntry = intermediaryMetric.getEnumEntry();
                    apiMetrics.add(new Metric.EnumBits(
                            metricId,
                            typeCode,
                            supplementalTypes,
                            agentTimestamp,
                            measurementReceptionTimestamp,
                            measurementStatus,
                            enumEntry.getBitsEnum(),
                            enumEntry.isBits32Enum()
                    ));
                }
            }

            if (apiMetrics.isEmpty()) return;

            Log.i(TAG, "Forwarding " + apiMetrics.size() + " metrics to the host.");
            dc2hostAPI.get().metricsReceived(mds.getSystemId(), apiMetrics);
        }
    };

    @SuppressWarnings("FieldMayBeFinal") // DeviceCommunicationServiceTest uses reflection to manipulate this field
    private Supplier<DC2HostAPI> dc2hostAPI = () -> DeviceCommunicationController.shared.dc2hostAPI;
}
