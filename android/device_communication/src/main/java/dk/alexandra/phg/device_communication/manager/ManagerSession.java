package dk.alexandra.phg.device_communication.manager;

import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.pcha.btmanager.BtManager;
import com.pcha.btmanager.BtStatics;
import com.pcha.btmanager.GattUuids;
import com.pcha.btmanager.StatusEventCallback;
import com.pcha.codephg.inter.Mdc;
import com.pcha.codephg.inter.PhgFields;

import java.util.HashMap;
import java.util.UUID;

/**
 * State of a device management session.
 * <p>
 * This object holds the state of the entire device management session going across one or more
 * sequentially existing DeviceManager objects.
 */
class ManagerSession {

    final @NonNull Context context;
    final @NonNull BtManager btManager;
    final @NonNull HashMap<Integer, UUID> mdcToServiceUuid;
    final @NonNull WhitelistListenerHandler whitelistListenerHandler;
    final @NonNull WhitelistManager whitelistManager;

    final @NonNull Handler taskQueue = new Handler(Looper.getMainLooper());


    /**
     * Initialize the state.
     * @param context The application context
     */
    ManagerSession(@NonNull Context context) {

        this.context = context;

        // We need to construct the reverse mapping Specialization -> Service UUID.
        // CODE only provides the Service UUID -> Specialization mapping...
        mdcToServiceUuid = new HashMap<>();
        for (GattUuids.GattType gt : GattUuids.GattType.values()) {
            long mdc = BtStatics.getMdcCodeFromUuid(gt.uuid);
            if (mdc != -1) {
                mdc = Mdc.get32BitCode(Mdc.MDC_PART_INFRA, (int) mdc);
                mdcToServiceUuid.put((int) mdc, gt.uuid);
            }
        }

        whitelistListenerHandler = new WhitelistListenerHandler();

        BtManager.setEnableInstantPulseOxDataFiltering(true);
        BtManager.setEnableFastModalityFiltering(true);
        BtManager.setEnableSlowModalityFiltering(true);
        BtManager.setScannerEnabled(false);
        BtManager.setPhgFields(new PhgFields(0));
        btManager = new BtManager(context, false, false, true);

        whitelistManager = new WhitelistManager(whitelistListenerHandler,
                context.getSystemService(BluetoothManager.class).getAdapter(),this);

        IntentFilter filter = new IntentFilter();
        filter.addAction("android.bluetooth.device.action.ACL_DISCONNECTED");
        filter.addAction("android.bluetooth.device.action.BOND_STATE_CHANGED");
        context.registerReceiver(whitelistManager, filter);

        BtManager.setStatusEventCallback((statusEvent, phdInformation, arbitraryText) -> {
            if (statusEvent == StatusEventCallback.StatusEvent.PAIRED) {
                taskQueue.post(()->whitelistManager.onDeviceBonded(phdInformation));
            } else if (statusEvent == StatusEventCallback.StatusEvent.CONNECTED) {
                taskQueue.post(()->whitelistManager.connectionStateUpdate(phdInformation, true));
            } else if (statusEvent == StatusEventCallback.StatusEvent.DISCONNECTED) {
                taskQueue.post(()->whitelistManager.connectionStateUpdate(phdInformation, false));
            } else if (statusEvent == StatusEventCallback.StatusEvent.MDS_DONE) {
                taskQueue.post(()->whitelistManager.mdsUpdated(phdInformation));
            }
        });
    }

    /**
     * End the session.
     * <p>
     * This will terminate the session and render the object unusable. This method must be called
     * as the last thing before the session is ended.
     */
    void cleanup() {
        BtManager.setStatusEventCallback(null);
        context.unregisterReceiver(whitelistManager);
        whitelistManager.cleanup();
        // Closing the btManager will save the list of known devices to disk.
        btManager.close();
    }
}
