package dk.alexandra.phg.device_communication.manager;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.ScanResult;
import android.companion.AssociationInfo;
import android.companion.CompanionDeviceManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Pair;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.pcha.btmanager.BtManager;
import com.pcha.btmanager.PhdInformation;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


// The whitelist is defined as the intersection of the set of devices registered by the
// Android Companion subsystem for use with this app and the set of devices known by the
// CODE BtManager.

public class WhitelistManager extends BroadcastReceiver {
    // Timeout to use when attempting to connect / bond with a new device. If unsuccessful
    // within this time, we will give up.
    private static final int BONDING_TIMEOUT_SECONDS = 30;
    // Timeout to use when reading the information from a device after a successful bond.
    private static final int READ_TIMEOUT_SECONDS = 10;
    // Backoff time to use when an attempt to connect to a device fails with an error.
    private static final int BACKOFF_MILLISECONDS = 1000;


    /**
     * Generate the current whitelist of personal health devices available to the app.
     * <p>
     * The list will be empty if a BtManager object has never been constructed. Note that
     * a BtManager can only be constructed if the BLUETOOTH_CONNECT permission is granted,
     * so this permission is required to get a non-empty whitelist.
     *
     * @param context The Android application context
     * @return A Set of PhdInformation objects holding details of each PHD
     */
    static Set<PhdInformation> getWhitelist(@NonNull Context context) {
        return buildWhitelist(context).map(p -> p.first).collect(Collectors.toSet());
    }

    private static Stream<Pair<PhdInformation,AssociationInfo>> buildWhitelist(@NonNull Context context) {
        // See the definition of the whitelist in a comment at the top of this file.
        CompanionDeviceManager companionDeviceManager
                = (CompanionDeviceManager) context.getSystemService(Context.COMPANION_DEVICE_SERVICE);
        List<PhdInformation> knownDevices;
        try {
            knownDevices = BtManager.getKnownDevices();
        } catch (NullPointerException e) {
            return Stream.empty();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
            List<AssociationInfo> companionAssociationInfos = companionDeviceManager.getMyAssociations();
            // Find the intersection of the knownDevices (PhdInformation) devices and Companion's (AssociationInfo) devices
            // and return a stream of Pair<PhdInformation,AssociationInfo> with the matches.
            return knownDevices.stream()
                    .flatMap(pi -> companionAssociationInfos.stream()
                            .map(ai -> Pair.create(pi,ai))
                            .filter(p -> p.second.getDeviceMacAddress() != null &&
                                    p.second.getDeviceMacAddress().toString().equalsIgnoreCase(p.first.getAddress()))
                            .limit(1)); // There should be only one, but if more than one, they will not differ in any important way, so we can pick the first.
        } else {
            // Find the intersection of the knownDevices (PhdInformation) devices and Companion's devices
            // and return the result as a stream of PhdInformation (wrapped in Pair<PhdInformation,ALWAYS-NULL>).
            @SuppressWarnings({"deprecation", "RedundantSuppression"})
            List<String> companionAssociations = companionDeviceManager.getAssociations();
            return knownDevices.stream()
                    .filter(pi -> companionAssociations.contains(pi.getAddress()))
                    .map(a -> Pair.create(a,null));
        }
    }





    private final @NonNull WhitelistListenerHandler whitelistListenerHandler;
    private final @NonNull ManagerSession parentSession;


    // The state a PHD will go through from the user requests it to be added to the whitelist and
    // until it is on the whitelist
    private enum DeviceState {
        BONDING,
        READING_DEVICE_INFO,
        // We've given up bonding with this device, but Android still has an open/dangling
        // connection and we must wait for it to close before we can "forget" the device
        TIMED_OUT,
        ON_WHITELIST,
    }
    private static class DeviceContext {
        @NonNull DeviceState state = DeviceState.ON_WHITELIST;
        int limboCount = 0;
        @Nullable Runnable timeoutTask = null;
        @Nullable Runnable backoffTask = null;
        @Nullable ScanResult scanResult = null;
        @Nullable BluetoothGatt bluetoothGatt = null;
        boolean connected = false;
        boolean connecting = false;
        @Nullable PhdInformation phdInformation = null;
        int id;
        
        /** Create a DeviceContext of an new device which the user wish to add to the whitelist */
        DeviceContext(@NonNull DeviceState state, int limboCount,
                      @Nullable Runnable timeoutTask, @Nullable ScanResult scanResult, int id) {
            this.state = state;
            this.limboCount = limboCount;
            this.timeoutTask = timeoutTask;
            this.scanResult = scanResult;
            this.id = id;
        }
        
        /** Create a DeviceContext of a device which is already on the whitelist */
        DeviceContext(@NonNull PhdInformation phdInformation, int id) {
            this.phdInformation = phdInformation;
            this.id = id;
        }
    }
    private final @NonNull HashMap<BluetoothDevice, DeviceContext> deviceContexts;

    // Association continuation for an ongoing addDevice(). Only one can be active at any time and
    // if none are active, this value is null.
    private @Nullable AssociationHandler.AssociationResult associationContinuation = null;

    // The whitelist manager is shutting down (close() has been called)
    private boolean shuttingDown = false;


    WhitelistManager(@NonNull WhitelistListenerHandler whitelistListenerHandler,
                     @NonNull BluetoothAdapter adapter,
                     @NonNull ManagerSession parentSession) {

        this.whitelistListenerHandler = whitelistListenerHandler;
        this.parentSession = parentSession;

        // We initialize the CODE whitelist as non-empty but with no real devices (only the empty
        // string). This will put CODE in "whitelist-only" mode but no device will ever match.
        LinkedList<String> phonyList = new LinkedList<>();
        phonyList.add("");
        BtManager.setWhiteList(phonyList);

        deviceContexts = new HashMap<>();
        buildWhitelist(parentSession.context).forEach(p -> {
            PhdInformation phdInfo = p.first;
            BluetoothDevice device = adapter.getRemoteDevice(phdInfo.getAddress());
            int id = Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE ? p.second.getId() : 0;
            DeviceContext dc = new DeviceContext(phdInfo, id);
            deviceContexts.put(device, dc);
            setupStickyConnection(device, phdInfo, dc);
        });
    }

    @NonNull
    Map<PhdInformation, Boolean> getWhitelistWithConnectionState() {
        Map<PhdInformation, Boolean> wl = new HashMap<>();
        deviceContexts.values().stream()
                .filter(dc -> DeviceState.ON_WHITELIST.equals(dc.state))
                .forEach(dc -> wl.put(dc.phdInformation, dc.connected));
        return wl;
    }

    @SuppressWarnings({"deprecation", "RedundantSuppression"})
    void removeDevice(@NonNull PhdInformation phdInformation,
                      @NonNull CompanionDeviceManager companionDeviceManager) {

        BluetoothDevice device = phdInformation.getBluetoothDevice();
        if (device != null) {
            DeviceContext dc = deviceContexts.get(device);
            if (dc != null && DeviceState.ON_WHITELIST.equals(dc.state)) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE) {
                    companionDeviceManager.disassociate(dc.id);
                } else {
                    companionDeviceManager.disassociate(device.getAddress());
                }
                BtManager.removeDeviceFromWhiteList(device.getAddress());
            }
            deviceContexts.remove(device);
        }
        BtManager.removeDevice(phdInformation);
        whitelistListenerHandler.signalPhdDeleted(phdInformation);
    }

    // Will only be invoked after successful BLUETOOTH_CONNECT permissions check
    // The associationInfo argument is mandatory in UPSIDE_DOWN_CAKE(34) and later. Earlier Android versions set it to null
    @SuppressLint("MissingPermission")
    void addDevice(@NonNull ScanResult scanResult, @Nullable AssociationInfo associationInfo,
                   @NonNull AssociationHandler.AssociationResult continuation) {
        BluetoothDevice device = scanResult.getDevice();

        if (associationContinuation != null) {
            // Another addDevice() request is still ongoing, and we will only process one at a time
            continuation.error(DeviceDiscoveryErrorCallback.ErrorCode.ONGOING_ADDITION,
                    device.getName());
            return;
        }

        DeviceContext dc = deviceContexts.get(device);

        if (dc == null || DeviceState.TIMED_OUT.equals(dc.state)) { // New device
            // Save the continuation so that we can resume later...
            associationContinuation = continuation;
            // Set timeout to bail out of this
            Runnable timeout = () -> timeoutHandler(device);
            parentSession.taskQueue.postDelayed(timeout, BONDING_TIMEOUT_SECONDS * 1000L);
            int id = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.UPSIDE_DOWN_CAKE &&
                    associationInfo != null) ? associationInfo.getId() : 0;
            dc = new DeviceContext(DeviceState.BONDING, 2, timeout, scanResult, id);
            deviceContexts.put(device, dc);
            if (device.getBondState() == BluetoothDevice.BOND_NONE) {
                if (!device.createBond()) {
                    // Error starting bonding. Bail out...
                    finishAssociation(device, dc);
                    deviceContexts.remove(device);
                    associationContinuation = null;
                    continuation.error(DeviceDiscoveryErrorCallback.ErrorCode.BONDING_FAILED,
                            device.getName());
                }
            } else if (device.getBondState() == BluetoothDevice.BOND_BONDED) {
                readPhdInfo(device, dc); // Will change state to DeviceState.READ_INFO
            } // else BOND_BONDING -> do nothing

        } else if (DeviceState.ON_WHITELIST.equals(dc.state)) { // Device already on whitelist
            // Ignore selection and restart the scanning
            continuation.resume();
        } // else: Device in the process of being added to the whitelist. Ignore / do not interfere.
    }

    // Check if the session ended while waiting...
    private boolean parentSessionEnded() { return DeviceManager.getSession() != parentSession; }

    void onDeviceBonded(@NonNull PhdInformation phdInformation) {
        if (parentSessionEnded()) return;

        // Note that this method will be called whenever ANY TYPE of Bluetooth device is bonded
        // i.e. also PCs, headsets etc. We don't care, though, because they are not registered
        // in deviceContexts, so the dc variable below will become null.

        // CODE will add the device to the lists too early, which result in the PhdInformation
        // struct not being filled. Therefore we delete it so that it will be added (correctly)
        // again. We need to clear the paired flag, otherwise CODE will try to un-bond the device.
        phdInformation.setIsPaired(false);
        BtManager.removeDevice(phdInformation);
        BluetoothDevice device = phdInformation.getBluetoothDevice();
        DeviceContext dc = deviceContexts.get(device);
        if (dc != null && dc.timeoutTask != null && DeviceState.BONDING.equals(dc.state)) {
            parentSession.taskQueue.removeCallbacks(dc.timeoutTask);
            parentSession.taskQueue.postDelayed(dc.timeoutTask, READ_TIMEOUT_SECONDS * 1000L);
            readPhdInfo(device, dc); // Will change state to DeviceState.READ_INFO
        } else if (dc != null && DeviceState.TIMED_OUT.equals(dc.state)) {
            // Bonded too late. Clear out the connection, so the user may retry.
            openLimboConnection(device);
        }
    }

    void onDeviceUnBonded(@NonNull BluetoothDevice device) {
        if (parentSessionEnded()) return;

        DeviceContext dc = deviceContexts.get(device);
        if (dc == null) return;

        if (!DeviceState.BONDING.equals(dc.state)) {
            // We create the CompanionDeviceManager from the application Context rather than an
            // Android Activity. This SHOULD be sufficient for the CDM to remove a device as
            // it will not need to show any UI elements to do this.
            CompanionDeviceManager companionDeviceManager
                    = (CompanionDeviceManager) parentSession.context
                    .getSystemService(Context.COMPANION_DEVICE_SERVICE);
            if (dc.phdInformation != null) removeDevice(dc.phdInformation, companionDeviceManager);
        } else {
            // We are attempting to bond. The Android BLE stack is weird. Apparently, the first
            // attempt to bond will ALWAYS fail. Probably due to some internal race condition.
            // However, it seems to work after we open and then immediately close a GATT connection.
            // We use the limbo counter to make this attempt a couple of times (usually, it works
            // the first time). If it does not work, it is probably because either the user refused
            // the pairing pop-up dialogue, or because the device refused to bond.
            if (dc.limboCount == 0) {
                // This is probably because the user or the device refuses to bond. If the device
                // refuses, Android SHOULD have notified the user, so we won't make a notification.
                bailOut(device, dc);
            } else {
                dc.limboCount--;
                openLimboConnection(device);
            }
        }
    }

    // The device can only be in BONDING state after a successful BLUETOOTH_CONNECT permissions check
    @SuppressLint("MissingPermission")
    private void openLimboConnection(@NonNull BluetoothDevice device) {
        device.connectGatt(parentSession.context, false,
                new BluetoothGattCallback() {
                    @Override
                    public void onConnectionStateChange(@NonNull BluetoothGatt gatt, int status, int newState) {
                        super.onConnectionStateChange(gatt, status, newState);
                        if (parentSessionEnded()) return;

                        BluetoothDevice device = gatt.getDevice();
                        DeviceContext dc = deviceContexts.get(device);
                        if (dc == null || (!DeviceState.BONDING.equals(dc.state) &&
                                !DeviceState.TIMED_OUT.equals(dc.state))) {
                            return;
                        }

                        if (status == BluetoothGatt.GATT_SUCCESS &&
                                newState == BluetoothProfile.STATE_CONNECTED) {
                            // Keep the connection for 2 seconds. Otherwise the BT stack may lock up...
                            parentSession.taskQueue.postDelayed(gatt::close,2000);
                        } else if (status != BluetoothGatt.GATT_SUCCESS) {
                            parentSession.taskQueue.post(() -> {
                                if (associationContinuation != null) {
                                    associationContinuation.error(DeviceDiscoveryErrorCallback.ErrorCode.BONDING_FAILED,
                                            device.getName());
                                }
                                bailOut(device, dc);
                            });
                        }

                    }
                },
                BluetoothDevice.TRANSPORT_LE, BluetoothDevice.PHY_OPTION_NO_PREFERRED, parentSession.taskQueue);
    }

    // The device can only be in BONDING / READING_DEVICE_INFO states after a successful
    // BLUETOOTH_CONNECT permissions check
    @SuppressLint("MissingPermission")
    private void timeoutHandler(@NonNull BluetoothDevice device) {
        if (parentSessionEnded()) return;
        DeviceContext dc = deviceContexts.get(device);
        if (dc == null) return;
        dc.timeoutTask = null;
        if (DeviceState.BONDING.equals(dc.state) || DeviceState.READING_DEVICE_INFO.equals(dc.state)) {
            // We did not succeed connecting to the device. Mark it as timed out, notify the user
            // and resume scanning. Another attempt cannot be made until the device state is
            // cleaned up (i.e. then the timed out device will be removed from deviceContexts).
            dc.state = DeviceState.TIMED_OUT;

            // Stop any device interaction and remove from the CODE repeatAddresses list so that
            // we may make another attempt to connect later.
            BtManager.removeDeviceFromSession(device);

            if (associationContinuation != null) {
                associationContinuation.error(DeviceDiscoveryErrorCallback.ErrorCode.BONDING_TIMEOUT,
                        device.getName());
                resumeScanning();
            }
        }
    }

    @Override
    public void onReceive(@NonNull Context context, @NonNull Intent intent) {
        if (parentSessionEnded()) return;
        BluetoothDevice device;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.TIRAMISU) {
            device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE, BluetoothDevice.class);
        } else {
            @SuppressWarnings({"deprecation", "RedundantSuppression"})
            BluetoothDevice d = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
            device = d;
        }
        if (device == null) return;

        if (BluetoothDevice.ACTION_ACL_DISCONNECTED.equals(intent.getAction())) {
            DeviceContext dc = deviceContexts.get(device);
            if (dc != null) deviceDisconnected(device, dc);
        } else if (BluetoothDevice.ACTION_BOND_STATE_CHANGED.equals(intent.getAction())) {
            int prevBondState = intent.getIntExtra(BluetoothDevice.EXTRA_PREVIOUS_BOND_STATE, 0);
            int newBondState = intent.getIntExtra(BluetoothDevice.EXTRA_BOND_STATE, 0);
            if (newBondState == BluetoothDevice.BOND_NONE && prevBondState != BluetoothDevice.BOND_NONE) {
                onDeviceUnBonded(device);
            }
        }
    }

    // The device can only be in BONDING state after a successful BLUETOOTH_CONNECT permissions check
    @SuppressLint("MissingPermission")
    private void deviceDisconnected(@NonNull BluetoothDevice device, @NonNull DeviceContext dc) {
        // We only care about devices in BONDING or TIMED_OUT states
        if (!DeviceState.BONDING.equals(dc.state) && !DeviceState.TIMED_OUT.equals(dc.state))
            return;
        if (DeviceState.BONDING.equals(dc.state)) {
            device.createBond();
        } else { // DeviceState.TIMED_OUT
            bailOut(device, dc);
        }
    }

    // Bail out of an association in either BONDING, READING_DEVICE_INFO or TIMED_OUT state
    private void bailOut(@NonNull BluetoothDevice device, @NonNull DeviceContext dc) {
        deviceContexts.remove(device);
        finishAssociation(device, dc);
        if (!DeviceState.TIMED_OUT.equals(dc.state)) resumeScanning();
    }

    private void readPhdInfo(@NonNull BluetoothDevice device, @NonNull DeviceContext dc) {
        if (parentSessionEnded()) return;
        dc.state = DeviceState.READING_DEVICE_INFO;

        BtManager.addDeviceToWhiteList(device.getAddress());
        parentSession.btManager.getAdvertisementHandler().onScanResult(0, dc.scanResult);
    }

    private void finishAssociation(@NonNull BluetoothDevice device, @NonNull DeviceContext dc) {
        dc.scanResult = null;
        BtManager.removeDeviceFromSession(device);
        if (dc.timeoutTask != null) {
            parentSession.taskQueue.removeCallbacks(dc.timeoutTask);
            dc.timeoutTask = null;
        }
    }

    private void resumeScanning() {
        if (associationContinuation != null) {
            associationContinuation.resume();
            associationContinuation = null;
        }
    }

    void mdsUpdated(@NonNull PhdInformation phdInformation) {
        BluetoothDevice device = phdInformation.getBluetoothDevice();
        if (device == null) return;
        DeviceContext dc = deviceContexts.get(device);
        if (dc != null && DeviceState.READING_DEVICE_INFO.equals(dc.state)) {
            dc.state = DeviceState.ON_WHITELIST;
            dc.phdInformation = phdInformation;
            BtManager.removeDeviceFromWhiteList(device.getAddress());
            finishAssociation(device, dc);
            connectionStateUpdate(phdInformation, false);
            parentSession.whitelistListenerHandler.signalPhdAdded(phdInformation, dc.connected);
            resumeScanning();
        }
    }

    void connectionStateUpdate(@NonNull PhdInformation phdInformation, boolean connected) {
        connectionStateUpdate(phdInformation, connected, BluetoothGatt.GATT_SUCCESS);
    }

    private void connectionStateUpdate(@NonNull PhdInformation phdInformation, boolean connected, int status) {
        BluetoothDevice device = phdInformation.getBluetoothDevice();
        if (device == null) return;
        DeviceContext dc = deviceContexts.get(device);
        if (dc == null) return;

        if (DeviceState.ON_WHITELIST.equals(dc.state)) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                connectionStateUpdateSuccess(device, phdInformation, dc, connected);
            } else {
                connectionStateUpdateFailed(device, phdInformation, dc, status);
            }
        } else {
            dc.connected = connected;
        }
    }

    private void connectionStateUpdateSuccess(@NonNull BluetoothDevice device,
                                              @NonNull PhdInformation phdInformation,
                                              @NonNull DeviceContext deviceContext,
                                              boolean connected) {
        if (connected && deviceContext.connecting) {
            deviceContext.connected = true;
            deviceContext.connecting = false;
            parentSession.whitelistListenerHandler.signalPhdUpdated(phdInformation, true);
        } else if (!connected && deviceContext.connected) {
            deviceContext.connected = false;
            setupStickyConnection(device, phdInformation, deviceContext);
            parentSession.whitelistListenerHandler.signalPhdUpdated(phdInformation, false);
        } else if (!connected && deviceContext.connecting) {
            // Failed to connect. Wait a second and then try again
            deviceContext.connecting = false;
            delayedSetupStickyConnection(device, phdInformation, deviceContext);
        }
    }

    private void connectionStateUpdateFailed(@NonNull BluetoothDevice device,
                                             @NonNull PhdInformation phdInformation,
                                             @NonNull DeviceContext deviceContext,
                                             int status) {
        // status codes we may expect here - not documented in the Android API!
        // Device went out of range - 8 (verified)
        // Disconnected by device - 19 (verified)
        // Issue with bond - 22
        // Device not found - 133 or 62

        // This was not a loss of connection, so we wait a second before attempting a reconnect
        boolean pause = ((status != 8 && status != 19) || !deviceContext.connected);

        if (deviceContext.connected) {
            deviceContext.connected = false;
            parentSession.whitelistListenerHandler.signalPhdUpdated(phdInformation, false);
        }

        if (pause) {
            delayedSetupStickyConnection(device, phdInformation, deviceContext);
        } else {
            setupStickyConnection(device, phdInformation, deviceContext);
        }
    }
    
    private void delayedSetupStickyConnection(@NonNull BluetoothDevice device,
                                              @NonNull PhdInformation phdInformation,
                                              @NonNull DeviceContext deviceContext) {
        Runnable backoffTask = () -> {
            deviceContext.backoffTask = null;
            setupStickyConnection(device, phdInformation, deviceContext);
        };
        deviceContext.backoffTask = backoffTask;
        parentSession.taskQueue.postDelayed(backoffTask, BACKOFF_MILLISECONDS);
    }

    @SuppressLint("MissingPermission")
    private void setupStickyConnection(@NonNull BluetoothDevice device,
                                       @NonNull PhdInformation phdInformation,
                                       @NonNull DeviceContext deviceContext) {
        if (deviceContext.connected || shuttingDown) return;
        deviceContext.connecting = true;
        if (deviceContext.bluetoothGatt == null) {
            deviceContext.bluetoothGatt = device.connectGatt(parentSession.context.getApplicationContext(),
                    true, new BluetoothGattCallback() {
                @SuppressLint("MissingPermission")
                @Override
                public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
                    super.onConnectionStateChange(gatt, status, newState);
                    if (status == BluetoothGatt.GATT_SUCCESS || newState == BluetoothProfile.STATE_DISCONNECTED)
                        parentSession.taskQueue.post(() -> connectionStateUpdate(phdInformation,
                                newState == BluetoothProfile.STATE_CONNECTED, status));
                }
            });
        } else {
            deviceContext.bluetoothGatt.connect();
        }
    }

    @SuppressLint("MissingPermission")
    void cleanup() {
        shuttingDown = true;
        for (Map.Entry<BluetoothDevice, DeviceContext> entry : deviceContexts.entrySet()) {
            DeviceContext dc = entry.getValue();
            if (dc.backoffTask != null) {
                parentSession.taskQueue.removeCallbacks(dc.backoffTask);
                dc.backoffTask = null;
            }
            if (dc.bluetoothGatt != null) {
                dc.bluetoothGatt.close();
                dc.bluetoothGatt = null;
            }
        }
    }
}
