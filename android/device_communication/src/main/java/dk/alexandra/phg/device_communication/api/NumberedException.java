package dk.alexandra.phg.device_communication.api;

public class NumberedException extends Exception {
    public final int errorNumber;

    NumberedException(int errorNumber, String errorMessage) {
        super(errorMessage);
        this.errorNumber = errorNumber;
    }

    public static class InvalidArgument extends NumberedException {
        public InvalidArgument(String errorMessage) {
            super(ErrorCodes.INVALID_ARGUMENT.value, errorMessage);
        }
    }

    public static class UnsupportedOperation extends NumberedException {
        public UnsupportedOperation(String errorMessage) {
            super(ErrorCodes.UNSUPPORTED_OPERATION.value, errorMessage);
        }
    }

    public static class BluetoothPermissionNotGranted extends NumberedException {
        public BluetoothPermissionNotGranted(String errorMessage) {
            super(ErrorCodes.BLUETOOTH_PERMISSION_NOT_GRANTED.value, errorMessage);
        }
    }

    public static class BluetoothTurnedOff extends NumberedException {
        public BluetoothTurnedOff(String errorMessage) {
            super(ErrorCodes.BLUETOOTH_TURNED_OFF.value, errorMessage);
        }
    }
}
