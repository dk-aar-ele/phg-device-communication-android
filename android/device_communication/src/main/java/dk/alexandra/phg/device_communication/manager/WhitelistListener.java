package dk.alexandra.phg.device_communication.manager;

import androidx.annotation.NonNull;

import com.pcha.btmanager.PhdInformation;

/**
 * Receiver of updates regarding the devices whitelist.
 */
public interface WhitelistListener {
    /**
     * A personal health device was added to the whitelist
     *
     * @param phd                The device
     * @param currentlyConnected If the device is currently connected to the app
     */
    void phdAdded(@NonNull PhdInformation phd, boolean currentlyConnected);

    /**
     * A personal health device was removed from the whitelist
     *
     * @param phd The device
     */
    void phdDeleted(@NonNull PhdInformation phd);

    /**
     * A personal health device was updated on the whitelist (may need to be refreshed in the
     * UI)
     *
     * @param phd                The device
     * @param currentlyConnected If the device is currently connected to the app
     */
    void phdUpdated(@NonNull PhdInformation phd, boolean currentlyConnected);
}
