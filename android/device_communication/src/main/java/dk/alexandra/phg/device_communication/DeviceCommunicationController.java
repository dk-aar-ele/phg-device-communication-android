package dk.alexandra.phg.device_communication;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import androidx.activity.ComponentActivity;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Set;
import java.util.function.Consumer;

import dk.alexandra.phg.device_communication.api.Callback;
import dk.alexandra.phg.device_communication.api.DC2HostAPI;
import dk.alexandra.phg.device_communication.api.DCMessageDispatcher;
import dk.alexandra.phg.device_communication.api.Device;
import dk.alexandra.phg.device_communication.api.Host2DCAPI;
import dk.alexandra.phg.device_communication.manager.DeviceManager;

/**
 * This is the main access point to the device communication library.
 * <p>
 * In order to use the device communication library, the host application must call initialize()
 * exactly once, providing the necessary arguments. From that point on, all communication and
 * control happens through the API.
 * <p>
 * The device communication library can automatically issue requests to the Android OS to grant the
 * app permission to access Bluetooth equipment, and to turn on the Bluetooth radio. In order to
 * enable this feature, the app must register an android.app.Application.ActivityLifecycleCallback
 * provided by the getActivityLifecycleCallbacks().
 */
public class DeviceCommunicationController {

    /**
     * Singleton shared instance of this class.
     */
    // The linter will complain about a memory leak by storing arbitrary Context objects.
    // But we explicitly store the ApplicationContext only, which is safe to do in a static field.
    @SuppressLint("StaticFieldLeak")
    public static final DeviceCommunicationController shared = new DeviceCommunicationController();

    private final Dispatcher dispatcher;
    private final Consumer<String> host2dcHandler;
    private final SubscriptionManager subscriptionManager;

    // All the following fields are @NonNull **after** initialize() has been invoked at least once...

    /** The API for communication towards the application */
    DC2HostAPI dc2hostAPI;

    private Context applicationContext;
    private DCMessageDispatcher messageDispatcher;
    private BluetoothGuard bluetoothGuard;

    private DeviceCommunicationController() {
        dispatcher = new Dispatcher();
        host2dcHandler = message -> messageDispatcher.receiveFromHost(message, dispatcher);
        subscriptionManager = new SubscriptionManager(this::getApplicationContext,
                                                      this::getBluetoothGuard);
    }

    private Context getApplicationContext() { return applicationContext; }
    private BluetoothGuard getBluetoothGuard() { return bluetoothGuard; }

    /**
     * Initialize the device communication library.
     * <p>
     * A handler of the DC2HostMessages must be registered when initializing the device
     * communication layer. This initializer should be called exactly once.
     *
     * @param context        The application context used to launch the service
     * @param dc2hostHandler Consumes JSON objects of type DC2HostMessage
     * @return               A consumer of JSON objects of type Host2DCMessage
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public Consumer<String> initialize(@NonNull Context context,
                                       @NonNull Consumer<String> dc2hostHandler) {
        applicationContext = context.getApplicationContext();
        messageDispatcher = new DCMessageDispatcher(dc2hostHandler);
        dc2hostAPI = new DC2HostAPI(messageDispatcher);
        bluetoothGuard = new BluetoothGuard(applicationContext);

        return host2dcHandler;
    }


    /**
     * Get an ActivityLifecycleCallbacks object for monitoring this application's UI.
     * <p>
     * The device communication library can automatically issue requests to the Android OS to grant
     * the app permission to access Bluetooth equipment, and to turn on the Bluetooth radio.
     * In order to do this, the device communication library needs a reference to the UI Activity
     * element currently showing on the screen. So, to enable this feature, the current app must
     * register an android.app.Application.ActivityLifecycleCallbacks handler provided by this
     * method. This is an optional feature. If the application does not register this handler,
     * the library can do nothing if Bluetooth is not available.
     *
     * @return An instance of the Application.ActivityLifecycleCallbacks interface which the
     *         Application may register using its registerActivityLifecycleCallbacks() method.
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    @NonNull
    public Application.ActivityLifecycleCallbacks getActivityLifecycleCallbacks() {
        if (bluetoothGuard == null)
            throw new IllegalStateException("DeviceCommunicationController was never initialized");
        return bluetoothGuard.getActivityLifecycleCallbacks();
    }

    /**
     * Get a device manager for setting up personal health device associations with this app.
     * <p>
     * The device manager offers functionality to establish and maintain a whitelist of
     * personal health devices that can be used as sources of observations.
     * <p>
     * The first time getDeviceManager() is called, a device manager session is started, if one
     * was not explicitly started using startDeviceManagerSession(). The getDeviceManager() method
     * may be called multiple times during such a session, typically using different view
     * (ComponentActivity) objects. When the session is complete, endDeviceManagerSession() must
     * be called to save the changes to the whitelist.
     * <p>
     * The startDeviceManagerSession() SHOULD be called to start the very first session as this
     * method will also check for the appropriate permissions. This is only necessary the first
     * time a session is started, as permissions cannot be revoked without restarting the app.
     * <p>
     * getDeviceManager() must be invoked after initialize(), and use of it must be completed
     * and endDeviceManagerSession() called before any "real" interaction with personal health
     * devices can begin (because the device manager session will connect to the devices and
     * therefore block any other access).
     * <p>
     * An active androidx.activity.ComponentActivity must be available to receive notifications
     * from the OS when new devices appear. This activity must be registered here during its own
     * initialization, i.e. getDeviceManager() must always be called before the provided activity
     * is started! Hence, this method should typically be called from the ComponentActivity's
     * onCreate() method.
     *
     * @param activity A ComponentActivity which can receive notifications about new devices.
     * @return The device manager
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    @NonNull
    public DeviceManager getDeviceManager(@NonNull ComponentActivity activity) {
        return new DeviceManager(activity, bluetoothGuard);
    }

    /**
     * Start the device manager session.
     * <p>
     * This must be called after initialize() and the resultCallback must be called with the
     * value true before getDeviceManager() is invoked. This method will check that the
     * appropriate permissions are in place and then start a session. When the session has been
     * started successfully, the resultCallback will be triggered with true. If permissions are
     * not granted, the resultCallback will be triggered with false.
     * <p>
     * If a device manager session was already started, invoking this is a no-op.
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public void startDeviceManagerSession(@NonNull Consumer<Boolean> resultCallback) {
        if (bluetoothGuard == null)
            throw new IllegalStateException("DeviceCommunicationController was never initialized");
        bluetoothGuard.bluetoothLEGuard(false, guardPassed -> {
            if (guardPassed != null && guardPassed) {
                DeviceManager.beginSessionIfNotStarted(applicationContext);
                resultCallback.accept(true);
            } else {
                resultCallback.accept(false);
            }
        });
    }

    /**
     * Close and cleanup the device manager session.
     * <p>
     * This must be called when the device manager is not needed anymore and will close any
     * remaining device connections and save the whitelist changes. This method must be called
     * before any "real" interaction with personal health devices begins.
     * <p>
     * If the device manager was already clean (no un-ended session was started), invoking this
     * is a no-op.
     */
    @SuppressWarnings("unused")  // This method is only called by users of this library
    public void endDeviceManagerSession() {
        DeviceManager.endSession();
    }


    private class Dispatcher implements Host2DCAPI {

        private final @NonNull Handler taskQueue = new Handler(Looper.getMainLooper());

        @Override
        public void subscribeDeviceType(int specialization, @NonNull Callback<Void> callback) {
            taskQueue.post(() -> subscriptionManager.subscribeDeviceType(specialization, callback));
        }

        @Override
        public void unsubscribeDeviceType(int specialization, @NonNull Callback<Void> callback) {
            taskQueue.post(() -> subscriptionManager.unsubscribeDeviceType(specialization, callback));
        }

        @Override
        public void subscribeDevice(@NonNull String persistentDeviceId, @NonNull Callback<Void> callback) {
            taskQueue.post(() -> subscriptionManager.subscribeDevice(persistentDeviceId, callback));
        }

        @Override
        public void unsubscribeDevice(@NonNull String persistentDeviceId, @NonNull Callback<Void> callback) {
            taskQueue.post(() -> subscriptionManager.unsubscribeDevice(persistentDeviceId, callback));
        }

        @Override
        public void deleteMetric(@NonNull Callback<Void> callback)  {
            // Currently unimplemented
        }

        @Override
        public void enablePulseOxModalities(boolean normal, boolean fast, boolean slow,
                                            @NonNull Callback<Void> callback) {
            taskQueue.post(() -> {
                DeviceCommunicationService.enablePulseOxModalities(normal, fast, slow);
                callback.success(null);
            });
        }

        @Override
        public void generateFhirBundle(@NonNull Callback<Void> callback)  {
            // Currently unimplemented
        }

        @Override
        public void getAvailableDevices(@Nullable Device prototypeDevice, boolean linked,
                                        boolean connected, boolean connectable,
                                        @NonNull Callback<Set<Device>> callback) {
            taskQueue.post(() -> subscriptionManager.getAvailableDevices(prototypeDevice, linked,
                            connected, connectable, callback));
        }
    }
}
