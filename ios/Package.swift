// swift-tools-version: 5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

// NOTE: Be careful when editing this file. It is used as input to the buildscript!

import PackageDescription

let package = Package(
    name: "PhgDeviceCommunication",
    platforms: [
        .iOS(.v15)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "PhgDeviceCommunication",
            targets: ["PhgDeviceCommunication","codephg","SQLite"]),
    ],
    targets: [
        .target(name: "PhgDeviceCommunication", dependencies: ["codephg"]),
        .binaryTarget(name: "codephg", url: "https://api.bitbucket.org/2.0/repositories/dk-aar-ele/phg-device-communication-artifacts/downloads/codephg.xcframework-2719a6e.zip", checksum: "3c34c6e960bec8202621077a289e62c8e8890dbb04db19190b6b0012f5c4a083"),
        .binaryTarget(name: "SQLite", url: "https://api.bitbucket.org/2.0/repositories/dk-aar-ele/phg-device-communication-artifacts/downloads/SQLite.xcframework-2719a6e.zip", checksum: "45647a57f843e0083e35704fab605b56d8f0ec8c46dffba9a8b628b523612d5a"),
        .testTarget(name: "PhgDeviceCommunicationTests", dependencies: ["PhgDeviceCommunication","codephg","SQLite"]),
    ]
)
