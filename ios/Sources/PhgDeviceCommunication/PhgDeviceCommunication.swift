
import Foundation
import codephg
import SQLite

public final class DeviceCommunicationController {
    
    public static func initialize(dc2hostHandler: @escaping (String)->Void) -> ((String)->Void)  {
        
        
        // TODO: Remove - send random pulse measurements for test
        Timer.scheduledTimer(withTimeInterval: 10, repeats: true) { (_) in
            let randomPulse = Int.random(in: 80...90)
            dc2hostHandler("{\"messageType\":\"EVENT\",\"name\":\"metricsReceived\",\"args\":{\"persistentDeviceId\":\"FOOBAR\",\"metrics\":[{\"measurementTypeCode\":149530,\"value\":\"\(String(randomPulse))\",\"unitCode\":264864,\"timeOfMeasurementReception\":\"2022-05-03T11:00:52.229+02:00\"}]}}")
        }

        
        let _ = DeviceCommunicationController()
        
        
        return {(x:String) -> Void in print("Throw away: \(x)")}

    }
    
    
    
    let SECONDS: Set<Calendar.Component> = [.year, .month, .day, .hour, .minute, .second]

    let fhirUploadService: FhirUploadService
    let auditLog = DemoAuditLog()
    let patientMapper = ActivePatientMapper()
    let duplicateFilter = DuplicateFilter(uploadHistory: UploadHistory.EMPTY)
    let phgProperties: PhgProperties
    let fhirEncoder: FhirEncoder

    init() {
        print("Hello from DeviceCommunication - demoing CODE:")
        let l: Connection.Location = Connection.Location.inMemory

        do {
        let _: FhirStore = try FhirStore(location: l)
        } catch (_) {
        
        }
        
        self.fhirEncoder = FhirEncoder(patientMapper: patientMapper, duplicateFilter: duplicateFilter, auditLog: auditLog)

        let capexClient = CapabilityExchangeClient()
        let fhirStore = try! FhirStore(location: .inMemory)
        self.fhirUploadService = FhirUploadService(capabilityExchangeClient: capexClient, fhirStore: fhirStore, auditLog: auditLog, duplicateFilter: duplicateFilter)

        var phgProperties = PhgProperties(systemId: [0xFE, 0xED, 0xAB, 0xEE, 0xDE, 0xAD, 0xBE, 0xEF])
        phgProperties.certifiedHfsTypes = [.ObservationUploadFhir]
        phgProperties.manufacturerName = "Continua"
        phgProperties.modelNumber = "Demo PHG"
        phgProperties.timeSyncProtocol = Nomenclature.MDC_TIME_SYNC_NTPV4
        self.phgProperties = phgProperties
        
        
        let unit = "kg" as String

        let mds = newMdsIntermediary(specialization: Nomenclature.MDC_DEV_SPEC_PROFILE_SCALE)

        let mid = String(1)
        var metric = MetricIntermediary(measurementType: Nomenclature.MDC_MASS_BODY_ACTUAL, measurementReceivedTime: Date(), measurementId: mid)
        metric.deviceTimeStamp = Calendar.current.dateComponents(SECONDS, from: Date(timeIntervalSinceNow: -60))
        metric.numericValue = NumericValue(
            value: PreciseDecimal(significand: 500,exponent: -1),
             unit: unit == "lb" ? Nomenclature.UNIT_MDC_DIM_LB : Nomenclature.UNIT_MDC_DIM_KILO_G
        )
        sendMeasurements(metrics: [metric], mds: mds)
        print("Bye from DeviceCommunication")
    }
    
    public let text = "Hello, World!"
    
    public func test() {
        print(text)
    }

    private func newMdsIntermediary(specialization: Mdc) -> MdsIntermediary {
        var mds = MdsIntermediary(phgCoincidentTime: Date(), uuid: UUID(uuid: (4,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16)))
        mds.currentTime = Calendar.current.dateComponents(SECONDS, from: Date())
        mds.hasRealTimeClock = true
        mds.manufacturerName = "Continua"
        mds.modelNumber = "Demo PHD"
        mds.firmwareRevision = "fw1.2"
        mds.hardwareRevision = "hw1.1"
        mds.serialNumber = "0123456789ABCDEF"
        mds.systemId = [0x01, 0x23, 0x45, 0x67, 0x89, 0xAB, 0xCD, 0xEF]
        mds.specializations = [(specialization, nil)]
        return mds
    }

    private func sendMeasurements(metrics: [MetricIntermediary], mds: MdsIntermediary) {
        for upload in fhirEncoder.encodeFhir(metrics: metrics, mds: mds, phgProperties: phgProperties) {
            print(upload.encodedBundle)
            fhirUploadService.queueForUpload(
                bundle: upload.encodedBundle,
                measurementIds: upload.measurementIds,
                bundleId: upload.bundleId
            )
        }
    }
}
internal class DemoAuditLog: AuditLog {
    func log(_ event: AuditEvent) { }
}
