import XCTest
@testable import PhgDeviceCommunication

final class phg_device_communicationTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(DeviceCommunicationController().text, "Hello, World!")
    }
}
